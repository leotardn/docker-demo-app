"use strict";

const {
    HTTP400Error,
    HTTP401Error,
    HTTP500Error
} = require("./lib/errorTypes");

const ErrorHandler = require("./lib/errorHandler");
const compression = require("compression");
const config = require("config");
const cors = require("cors");

const { createConnection } = require("typeorm");

const express = require("express");

const jwt = require("./middleware/tokenValidatorTools");
const logTools = require("./lib/logTools");
const profil = require("./middleware/profil");

var errorHandler = new ErrorHandler();
var app = express();
app.use(cors());
app.use(compression());

//Filtration des requêtes par express... plus pertinent lorsqu'il y a un body
app.use(express.json());
app.use(
    express.urlencoded({
        extended: false
    })
);

//Obtenir un uuid pour les logs de la requete
app.use(logTools.requestId);

//Config Log Http - Au debut et à la fin
app.use(logTools.startLogMorgan);
app.use(logTools.endLogMorgan);

logTools.info("Starting", "Démarrage");

createConnection(config.get("typeORMConn"))
    .then(
        () => {},
        (err) => {
            logTools.critical("createConnection", err.message, err.stack);
            logTools.debug("createConnection", err.stack);
        }
    )
    .catch((err) => {
        logTools.critical("createConnection", err.message, err.stack);
        logTools.debug("createConnection", err.stack);
    });

app.get("/admin/donneesNominatives", profil.getLastUpdatedDonneesNominatives);
app.get("/admin/recherche", profil.getLastUpdatedDonneesNominatives);
app.get("/admin/etudesAnterieures", profil.getLastUpdatedEtudesAnterieures);
app.post("/fusion", profil.fusionProfils);

/*
 * Verification de la validité du token ainsi que l'ajoute de idPersonne dans le req.body
 * quand enableJWT est false on fait la validation de la presence du queryParams "idPersonne"
 */
if (config.get("security.enableJWT")) {
    if (config.get("security.enableProxy")) {
        app.use(jwt.setJWTValidationWithProxy, async (err, req, res, next) => {
            if (!errorHandler.isTrustedError(err)) {
                errorHandler.handleError(
                    new HTTP401Error(
                        `Erreur lors de la validation de l'identité: ${err.message}`,
                        err
                    ),
                    {
                        idCIAM: config.constantes.NOT_APPLICABLE,
                        body: { idPersonne: config.constantes.NOT_APPLICABLE }
                    },
                    res
                );
            } else {
                errorHandler.handleError(
                    err,
                    {
                        idCIAM: config.constantes.NOT_APPLICABLE,
                        body: { idPersonne: config.constantes.NOT_APPLICABLE }
                    },
                    res
                );
            }
        });
    } else {
        app.use(jwt.setJWTValidation, async (err, req, res, next) => {
            if (!errorHandler.isTrustedError(err)) {
                errorHandler.handleError(
                    new HTTP401Error(
                        `Erreur lors de la validation de l'identité: ${err.message}`,
                        `Erreur lors de la validation de l'identité: ${err.message}`,
                        err
                    ),
                    {
                        idCIAM: config.constantes.NOT_APPLICABLE,
                        body: { idPersonne: config.constantes.NOT_APPLICABLE }
                    },
                    res
                );
            } else {
                errorHandler.handleError(
                    err,
                    {
                        idCIAM: config.constantes.NOT_APPLICABLE,
                        body: { idPersonne: config.constantes.NOT_APPLICABLE }
                    },
                    res
                );
            }
        });
    }
}

app.use(jwt.validateUser, profil.getIdPersonne);

/*
 * Ces chemins(paths) et methodes sont generalement prefixes de https://api.umontreal.ca/[objet|projet]/v[version]
 * Exemple : https://api.umontreal.ca/registraire/v1/programmes va devenir /programmes ci-dessous
 */
app.get("/personne", profil.getPersonne); //Id to switch (get it from token)
app.delete("/personne", profil.deletePersonne);

app.get("/donneesNominatives", profil.getDonneesNominatives);
app.post("/donneesNominatives", profil.saveDonneesNominatives);

app.get("/fichiersProprietes", profil.getFichiersProprietes);
app.get("/fichiersProprietes/:id", profil.getFichierProprietes);
app.post("/fichiersProprietes/:id", profil.saveFichierProprietes); //Crée ET modifie
app.delete("/fichiersProprietes/:id", profil.deleteFichierProprietes);

app.get("/etudesAnterieures", profil.getEtudesAnterieures);
app.post("/etudesAnterieures", profil.createEtudeAnterieure);
app.get("/etudesAnterieures/:id", profil.getEtudeAnterieure);
app.post("/etudesAnterieures/:id", profil.saveEtudeAnterieure);
app.delete("/etudesAnterieures/:id", profil.deleteEtudeAnterieure);

app.get("/SIS/donneesNominatives", profil.getSISDonneesNominatives);
app.get("/SIS/etudesAnterieures", profil.getSISEtudesAnterieures);

/**
 * For any other request
 */
app.use("/", (req, res) => {
    throw new HTTP400Error();
});

/**
 *
 * Centralized Node.js Error-handling middleware
 */
app.use(async (err, req, res, next) => {
    if (!errorHandler.isTrustedError(err)) {
        errorHandler.handleError(new HTTP500Error(err), req, res);
    } else {
        errorHandler.handleError(err, req, res);
    }
});

module.exports = app;
