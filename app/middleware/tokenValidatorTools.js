"use strict";
const { HTTP401Error, HTTP500Error } = require("../lib/errorTypes");
const config = require("config");
const jwksRsa = require("jwks-rsa");
const jwt = require("express-jwt");

/**
 * Force la validation de JWT
 * Validation NON APPLICATIVE du JWT
 * Tiré de l'exemple ici : https://github.com/auth0/node-jwks-rsa/blob/master/examples/express-demo/README.md
 */
exports.setJWTValidation = jwt({
    // Lecture des clés de signature JWT (JWKS) dynamiquement
    secret: jwksRsa.expressJwtSecret({
        cache: config.get("security.jwksRsa.cache"),
        rateLimit: config.get("security.jwksRsa.rateLimit"),
        jwksRequestsPerMinute: config.get(
            "security.jwksRsa.jwksRequestsPerMinute"
        ),
        jwksUri: config.get("security.jwksRsa.jwksUri"),
        handleSigningKeyError: (err, cb) => {
            if (err instanceof jwksRsa.SigningKeyNotFoundError) {
                cb(
                    new HTTP401Error(
                        "[JWT] SigningKeyNotFoundErr, aucun match entre le key id du token and celui d'un des signing certificates. \nVerifier entre autre que le Token Keys Endpoint (security.jwksRsa.jwksUri) est toujours valide",
                        "Erreur d'authentification lors de la validation du jeton"
                    )
                );
            } else {
                cb(
                    new HTTP500Error(
                        `[JWT] Erreur lors de la validation du jetton [origine: jwksRsa.expressJwtSecret]: ${err.message}`,
                        "Erreur d'authentification lors de la validation du jeton"
                    )
                );
            }
        }
    }),

    // Validation de champs du JWT - NON APPLICATIFS
    issuer: config.get("security.jwksValidations.issuer"),
    algorithms: config.get("security.jwksValidations.algorithms")

    //Note : si il n'y à pas eu d'erreur, alors req.user contient le payload !!!
});

/**
 * Force la validation de JWT en utilisant un proxy
 * Validation NON APPLICATIVE du JWT
 * Tiré de l'exemple ici : https://github.com/auth0/node-jwks-rsa/blob/master/examples/express-demo/README.md
 */
exports.setJWTValidationWithProxy = jwt({
    // Lecture des clés de signature JWT (JWKS) dynamiquement
    secret: jwksRsa.expressJwtSecret({
        cache: config.get("security.jwksRsa.cache"),
        rateLimit: config.get("security.jwksRsa.rateLimit"),
        jwksRequestsPerMinute: config.get(
            "security.jwksRsa.jwksRequestsPerMinute"
        ),
        jwksUri: config.get("security.jwksRsa.jwksUri"),
        proxy: config.get("security.proxy"),
        handleSigningKeyError: (err, cb) => {
            if (err instanceof jwksRsa.SigningKeyNotFoundError) {
                cb(
                    new HTTP401Error(
                        "[JWT] SigningKeyNotFoundErr, aucun match entre le key id du token and celui d'un des signing certificates. \nVerifier entre autre que le Token Keys Endpoint (security.jwksRsa.jwksUri) est toujours valide",
                        "Erreur d'authentification lors de la validation du jeton"
                    )
                );
            } else {
                cb(
                    new HTTP401Error(
                        `[JWT] Erreur lors de la validation du jetton [origine: jwksRsa.expressJwtSecret]: ${err.message}`,
                        "Erreur d'authentification lors de la validation du jeton"
                    )
                );
            }
        }
    }),

    // Validation de champs du JWT - NON APPLICATIFS
    issuer: config.get("security.jwksValidations.issuer"),
    algorithms: config.get("security.jwksValidations.algorithms")

    //Note : si il n'y à pas eu d'erreur, alors req.user contient le payload !!!
});

/**
 *
 * Valide si une propriete chez l'usager est conforme à une valeur etablit au preavant dans le fichier config (ex: [email_verifier: true])
 * security.jwksValidations.propertyName - security.jwksValidations.propertyValue
 */
exports.validateUser = function (req, res, next) {
    if (config.get("security.enableJWT")) {
        if (req.user) {
            var propertyName = config.get(
                "security.jwksValidations.propertyName"
            );
            const { [propertyName]: valueFromPayload } = req.user;
            if (valueFromPayload) {
                //Successful validation
                req.idCIAM = valueFromPayload;
                if (req.idCIAM && req.idCIAM.replace(/\s/g, "") !== "") {
                    return next();
                }
                return next(
                    new HTTP401Error(
                        `validateUser - La proprieté ${propertyName}: ${valueFromPayload} fournie dans le jeton okta est nulle ou invalide.`,
                        `La proprieté ${propertyName} fournie dans le jeton okta est nulle ou invalide.`
                    )
                );
            }
            return next(
                new HTTP401Error(
                    `[JWT] La validation du payload à échoué, la proprieté ${propertyName} n'existe pas.`,
                    `La validation du payload à échoué, la proprieté ${propertyName} n'existe pas.`
                )
            );
        }
        return next(
            new HTTP401Error(
                "[JWT] La validation du jetton à échoué, req.user est undefined",
                "Erreur d'authentification, la validation du jetton à échoué"
            )
        );
    }
    if (req.query.idCIAM) {
        req.idCIAM = req.query.idCIAM;
        if (req.query.idCIAM && req.query.idCIAM.replace(/\s/g, "") !== "") {
            return next();
        }
        return next(
            new HTTP401Error(
                `validateUser - La proprieté idCIAM: ${req.query.idCIAM} fournie est nulle ou invalide (format valide: UUIDv4).`,
                `La proprieté idCIAM fournie ${req.query.idCIAM} est nulle ou invalide (format valide: UUIDv4).`
            )
        );
    }
    return next(
        new HTTP401Error(
            `validateUser - La proprieté idCIAM n'existe pas dans les query params.`,
            "La proprieté idCIAM n'existe pas dans les query params"
        )
    );
};
