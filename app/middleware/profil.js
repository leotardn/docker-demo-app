/* eslint-disable complexity */
/* eslint-disable max-depth */
/* eslint-disable for-direction */
"use strict";

const {
    BaseError,
    HTTP400Error,
    HTTP401Error,
    HTTP403Error,
    HTTP404Error,
    HTTP500Error,
    HTTP503Error
} = require("../lib/errorTypes");
const axios = require("axios");
const ciam = require("../lib/ciam.js");
const config = require("config");
const db = require("../lib/db.js");
const fusion = require("../lib/fusion.js");
const peoplesoft = require("../lib/peoplesoft.js");
const { v4: uuidv4 } = require("uuid");

/**
 * Fusion deux profils dans la bd intermediaire (chacun avec son propre idPersonne et idCIAM) en un seul. Resultat: 2 idCIAM qui pointent au même idPersonne
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 */
const fusionProfils = async function (req, res, next) {
    const { idCIAMAGarder } = req.query;
    const { idCIAMADisparaitre } = req.query;
    const { matriculeAGarder } = req.query;
    try {
        if (!idCIAMAGarder || !idCIAMADisparaitre || !matriculeAGarder) {
            return next(
                new HTTP400Error(
                    "Error fusion - Une ou plusieures valeurs entre: idCIAMAGarder, idCIAMADisparaitre ou matriculeAGarder ne sont pas presentes dans la requête.",
                    "idCIAMADisparaitre ou matriculeAGarder ne sont pas presentes dans la requête."
                )
            );
        }
        let result = await db.getCIAM(idCIAMAGarder);
        const { idPersonne: idPersAGarder } = result
            ? result
            : { idPersonne: undefined };

        result = await db.getCIAM(idCIAMADisparaitre);
        const { idPersonne: idPersADisparaitre } = result
            ? result
            : { idPersonne: undefined };

        if (idPersAGarder === idPersADisparaitre) {
            res.contentType("application/json");
            res.status(200);
            return res.json();
        }
        if (!idPersAGarder || !idPersADisparaitre) {
            return next(
                new HTTP404Error(
                    "Error fusion - idPersAGarder, idPersADisparaitre  ne sont pas presentes dans la BD.",
                    "idPersAGarder, idPersADisparaitre  ne sont pas presentes dans la BD."
                )
            );
        }
        /**
         *  Appel à admissionAPI pour faire la fusion des listesDAs de deux personnes
         */
        let endpointURL = `${config.admissionApiUrl}/fusion?idPersAGarder=${idPersAGarder}&idPersADisparaitre=${idPersADisparaitre}`;
        let axiosConfig = {
            method: "post",
            url: endpointURL,
            proxy: false,
            headers: {
                "Content-Type": "application/json"
            }
        };
        /**
         * Fusion DonneesNominatives, EtudesAnterieures, ListeDAs
         */

        await Promise.all([
            fusion.donneesNominatives(
                idPersAGarder,
                idPersADisparaitre,
                matriculeAGarder,
                res.locals.reqid
            ),
            fusion.etudesAnterieures(idPersAGarder, idPersADisparaitre),
            axios(axiosConfig),
            db.saveCIAM({
                idCIAM: idCIAMADisparaitre,
                idPersonne: idPersAGarder
            })
        ]);

        res.contentType("application/json");
        res.status(200);
        res.json();
        await db.deletePersonne(idPersADisparaitre);
    } catch (err) {
        if (err instanceof BaseError) {
            return next(err);
        }
        if (err.code === "ECONNREFUSED") {
            if (err.port === 3012) {
                return next(
                    new HTTP503Error(
                        `Error fusion profilAPI - Could not connect to admissionAPI. Verify that the application is running, ${err.message}`,
                        `ECONNREFUSED: Could not connect to admissionAPI`,
                        err
                    )
                );
            }
            return next(
                new HTTP503Error(
                    `Error fusion profilAPI - ${err.message}`,
                    `${err.message}`,
                    err
                )
            );
        }
        if (err.message && err.message.indexOf("non trouvé")) {
            return next(
                new HTTP404Error(
                    `Error fusion profilAPI - ${err.message}`,
                    err.message,
                    err
                )
            );
        }
        if (err.response && err.response.data) {
            return next(
                new HTTP500Error(
                    `Error fusion profilAPI - ${err.response.data.message}`,
                    err.response.data.message,
                    err
                )
            );
        }
        return next(
            new HTTP500Error(
                `Error fusion profilAPI - idCIAMAGarder: ${idCIAMAGarder}, idCIAMADisparaitre: ${idCIAMADisparaitre}, matriculeAGarder: ${matriculeAGarder}`,
                `idCIAMAGarder: ${idCIAMAGarder}, idCIAMADisparaitre: ${idCIAMADisparaitre}, matriculeAGarder: ${matriculeAGarder}: ${err.message}`,
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table Personne avec l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns idPersonne et les id de fichiersPropriete et etudesAnterieures liés à la personne OK
 */
const getPersonne = async function getPersonne(req, res, next) {
    let { idPersonne } = req.body;
    try {
        let personne = await db.getPersonne(idPersonne);
        if (personne) {
            res.status(200);
            res.contentType("application/json");
            return res.json(personne);
        }
        return next(
            new HTTP404Error(
                `Error getPersonne - Personne with idPersonne ${idPersonne} not found in database`,
                `Personne with idPersonne ${idPersonne} not found in database`
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getPersonne - Could not retreive (get) personne from database: ${err.message}. idPersonne Requested: ${idPersonne}`,
                "Error retreiving personne from database",
                err
            )
        );
    }
};

/**
 * Fait une requette dans la table CIAM avec l'idCIAM lié au jeton pour obternir l'idPersonne (ou au query param si enableJWT = false)
 * Cree l'idPersonne dans la table Personne s'il n'existe pas.
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns idPersonne liés à la personne
 */
const getIdPersonne = async function (req, res, next) {
    try {
        let { idCIAM } = req;
        let ciam = await db.getCIAM(idCIAM);
        if (ciam) {
            req.body.idPersonne = ciam.idPersonne;
            return next();
        }
        //Si la personne n,existe pas, on la créee ainsi que ses objets donneesNominatives et CIAM
        let idPersonne = uuidv4();
        await db.savePersonne({
            idPersonne: idPersonne,
            donneesNominatives: [{ idPersonne: idPersonne, emailAddr: idCIAM }],
            ciam: [
                {
                    idPersonne: idPersonne,
                    idCIAM: idCIAM
                }
            ]
        });
        req.body.idPersonne = idPersonne;
        next();
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getIdPersonne - Could not retreive (get) personne from database: ${err.message}. idCIAM Requested: ${req.idCIAM}`,
                "Error retreiving personne from database",
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table DonneesNominatives avec l'idPersonne lié au le jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns l'objet DonneesNominatives liés à la personne Ok
 */
const getDonneesNominatives = async function getDonneesNominatives(
    req,
    res,
    next
) {
    let { idPersonne } = req.body;
    try {
        let donneesNominatives = await db.getDonneesNominatives(idPersonne);
        if (donneesNominatives) {
            res.status(200);
            res.contentType("application/json");
            return res.json(donneesNominatives);
        }
        next(
            new HTTP404Error(
                `Error getDonneesNominatives - Données nominatives from personne with idPersonne ${idPersonne} not found in database`,
                `Données nominatives from personne with idPersonne ${idPersonne} not found in database`
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getDonneesNominatives - Could not retreive (get) donneesNominatives from database: ${err.message}. donneesNominatives.idPersonne: ${idPersonne}`,
                "Error retreiving donneesNominatives from database",
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table FichiersProprietes avec l'idPersonne lié au jeton (ou au query param si enableJWT = false) et le id (nom) correspondant au fichier voulu
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns les objets FichiersProprietes liés à l'idPersonne
 */
const getFichiersProprietes = async function getFichiersProprietes(
    req,
    res,
    next
) {
    let { idPersonne } = req.body;
    try {
        let fichiersProprietes = await db.getFichiersProprietes(idPersonne);
        if (fichiersProprietes) {
            res.status(200);
            res.contentType("application/json");
            return res.json(fichiersProprietes);
        }
        return next(
            new HTTP404Error(
                `Error getFichiersProprietes - Fichiers proprietés from personne with idPersonne ${idPersonne} not found in database`,
                `Fichiers proprietés from personne with idPersonne ${idPersonne} not found in database`
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getFichiersProprietes - Could not retreive (get) property files with idPersonne ${idPersonne} from database: ${err.message}`,
                "Error retreiving fichiersProprietes from database",
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table FichiersProprietes correspondant à l'Id fourni et l'idPersonne lié au jeton (ou au query param si enableJWT = false) et le id (nom) correspondant au fichier voulu
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns l'objet FichiersProprietes liés à la personne avec l'id (nom) demandé
 */
const getFichierProprietes = async function getFichierProprietes(
    req,
    res,
    next
) {
    let { id } = req.params;
    let { idPersonne } = req.body;
    try {
        if (!id) {
            return next(
                new HTTP400Error(
                    "getFichierProprietes - Could not identify the fichiersProprietes (missing Id - nomFichierProp)",
                    "Could not identify the fichiersProprietes (missing Id - nomFichierProp)"
                )
            );
        }
        let fichierProprietes = await db.getFichierProprietes(id, idPersonne);
        if (fichierProprietes) {
            res.status(200);
            res.contentType("application/json");
            return res.json(fichierProprietes);
        }
        return next(
            new HTTP404Error(
                `Error getFichierProprietes - Fichier proprietés with id ${id} from personne with idPersonne ${idPersonne} not found in database`,
                `Fichier proprietés with id ${id} from personne with idPersonne ${idPersonne} not found in database`
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getFichierProprietes - Could not retreive (get) property file from database: ${err.message} - propertyFile id:${id} with idPersonne ${idPersonne}`,
                "Error retreiving fichierProprietes from database",
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table EtudesAnterieures avec l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns liste les objets EtudesAnterieures liés à la personne ok
 */
const getEtudesAnterieures = async function getEtudesAnterieures(
    req,
    res,
    next
) {
    let { idPersonne } = req.body;
    try {
        let etudesAnterieures = await db.getEtudesAnterieures(idPersonne);
        if (etudesAnterieures) {
            res.status(200);
            res.contentType("application/json");
            return res.json(etudesAnterieures);
        }
        return next(
            new HTTP404Error(
                `Error getEtudesAnterieures - No etudesAnterieures associated to idPersonne ${idPersonne} found in database`,
                "No etudesAnterieures found in database"
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getEtudesAnterieures - Could not retreive (get) etudesAnterieures associated to idPersonne ${idPersonne} from database: ${err.message}`,
                "Error retreiving etudesAnterieures from database",
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table EtudesAnterieures avec idEtudesAnterieures et l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns l'objet EtudesAnterieures liés à la personne
 */
const getEtudeAnterieure = async function getEtudeAnterieure(req, res, next) {
    let { idPersonne } = req.body;
    let { id: idEtudeAnterieure } = req.params;
    try {
        let etudeAnterieure = await db.getEtudeAnterieure(idEtudeAnterieure);
        if (etudeAnterieure) {
            res.status(200);
            res.contentType("application/json");
            return res.json(etudeAnterieure);
        }
        return next(
            new HTTP404Error(
                `Error getEtudeAnterieure - No etudesAnterieures with id ${idEtudeAnterieure} found in database`,
                "No etudeAnterieure found in database"
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getEtudeAnterieure - Could not retreive (get) etudeAnterieure ${idEtudeAnterieure} associated to idPersonne ${idPersonne} from database: ${err.message}`,
                "Error retreiving etudeAnterieure from database",
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table DonneesNominatives pour chercher touts les entrées ajoutées ou modifées après la date fournie
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns liste les objets DonneesNominatives ajoutés ou modifés après la date fournie
 */
const getAdminDonneesNominatives = async function getAdminDonneesNominatives(
    req,
    res,
    next
) {
    try {
        if (req.query.mode === "sis") {
            return await getSIS(
                req,
                res,
                next,
                config.peoplesoft.adminDonneesNominatives.serviceName
            );
        }
        if (req.query.mode === "inter") {
            let dateDebutAnalyse;
            if (req.query.matricule) {
                let donneesNominatives = await db.getAdminDonneesNominatives(
                    req.query.matricule
                );

                if (donneesNominatives) {
                    res.status(200);
                    res.contentType("application/json");
                    return res.json(donneesNominatives);
                }
                return next(
                    new HTTP404Error(
                        `Error getAdminDonneesNominatives - DonneesNominatives with matricule (${req.query.matricule}) NOT FOUND in database`,
                        `DonneesNominatives with matricule (${req.query.matricule}) NOT FOUND in database`
                    )
                );
            }
            if (req.query.dateDebutAnalyse) {
                dateDebutAnalyse = new Date(req.query.dateDebutAnalyse);
                if (dateDebutAnalyse.toString() === "Invalid Date") {
                    return next(
                        new HTTP400Error(
                            `Error getAdminDonneesNominatives - Format date "${req.query.dateDebutAnalyse}" invalide. Format accepeté: 2022-10-06T12:50:46.232Z`,
                            "Format date invalide. Exemple de format accepeté: 2022-10-06T12:50:46.232Z"
                        )
                    );
                }
            } else {
                //If no date is provided then quesry is made on the whole database
                dateDebutAnalyse = config.constantes.NOT_APPLICABLE;
            }

            let lastUpdatedDonneesNominativesList;
            if (req.query.nom && req.query.prenom) {
                lastUpdatedDonneesNominativesList = await db.getLastUpdatedDonneesNominatives(
                    dateDebutAnalyse,
                    { nom: req.query.nom, prenom: req.query.prenom }
                );
            } else if (req.query.courriel) {
                if (!req.query.courriel.includes("@")) {
                    return next(
                        new HTTP400Error(
                            `Error getAdminDonneesNominatives - courriel invalid. Requested courriel: ${req.query.courriel}`,
                            `Error validating provided courriel ${req.query.courriel}. Please provide valid courriel`
                        )
                    );
                }
                lastUpdatedDonneesNominativesList = await db.getLastUpdatedDonneesNominatives(
                    dateDebutAnalyse,
                    { courriel: req.query.courriel }
                );
                /*
                 * If courriel is not found in emailAddr DN,
                 * a query is made in CIAM table to find the idPersonne and query DN a second time but with idPersonne
                 */
                if (lastUpdatedDonneesNominativesList.length === 0) {
                    let CIAM = await db.getCIAM(req.query.courriel);
                    if (CIAM) {
                        //If call was made by admissionapi just to get idPersonne. Return CIAM object
                        if (req.query.idPersonne === "true") {
                            res.status(200);
                            res.contentType("application/json");
                            return res.json([CIAM]);
                        }
                        let { idPersonne } = CIAM;
                        if (idPersonne) {
                            lastUpdatedDonneesNominativesList = await db.getLastUpdatedDonneesNominatives(
                                dateDebutAnalyse,
                                { idPersonne: [idPersonne] }
                            );
                        }
                    }
                }
            } else if (dateDebutAnalyse !== config.constantes.NOT_APPLICABLE) {
                lastUpdatedDonneesNominativesList = await db.getLastUpdatedDonneesNominatives(
                    dateDebutAnalyse
                );
            } else if (req.body.idPersonne) {
                lastUpdatedDonneesNominativesList = await db.getLastUpdatedDonneesNominatives(
                    dateDebutAnalyse,
                    { idPersonne: req.body.idPersonne }
                );
            } else {
                return next(
                    new HTTP400Error(
                        `Error getAdminDonneesNominatives - Un ou plusieurs paramètres de recherche absents dans l'appel`,
                        `Un ou plusieurs paramètre de recherche absents dans l'appel`
                    )
                );
            }

            if (lastUpdatedDonneesNominativesList.length > 0) {
                if ((req.query.nom && req.query.prenom) || req.query.courriel) {
                    if (req.query.idPersonne !== "true") {
                        //Check if idPersonne has to be sent through (that's usually the case when admissionapi makes the request)
                        let newListWithAdmApplNbr = new Array();
                        for (let lastUpdatedDonneesNominatives of lastUpdatedDonneesNominativesList) {
                            let endpointURL = `${config.admissionApiUrl}/admin/admApplNbrs?idPersonne=${lastUpdatedDonneesNominatives.idPersonne}`;
                            let axiosConfig = {
                                method: "get",
                                url: endpointURL,
                                proxy: false,
                                headers: {
                                    "Content-Type": "application/json"
                                }
                            };
                            // eslint-disable-next-line no-await-in-loop
                            let response = await axios(axiosConfig);
                            lastUpdatedDonneesNominatives.admApplNbr =
                                response.data;
                            delete lastUpdatedDonneesNominatives.idPersonne;
                            newListWithAdmApplNbr.push(
                                lastUpdatedDonneesNominatives
                            );
                        }
                        res.status(200);
                        res.contentType("application/json");
                        return res.json(newListWithAdmApplNbr);
                    }
                }
                res.status(200);
            } else {
                res.status(204);
            }
            res.contentType("application/json");
            return res.json(lastUpdatedDonneesNominativesList);
        }
        return next(
            new HTTP400Error(
                `Error getAdminDonneesNominatives - Paramètre de recherche "mode" absent dans l'appel ou incorrecte`,
                `Paramètre de recherche "mode" absent dans l'appel ou incorrecte`
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getAdminDonneesNominatives - Could not retreive donneesNominatives from database, ${err.message}`,
                "Error retreiving latest updated donneesNominatives from database",
                err
            )
        );
    }
};

/**
 * Fait une requête dans la table EtudesAnterieures pour chercher touts les entrées ajoutées ou modifées après la date fournie
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns liste les objets EtudesAnterieures ajoutés ou modifés après la date fournie Ok
 */
const getLastUpdatedEtudesAnterieures = async function getLastUpdatedEtudesAnterieures(
    req,
    res,
    next
) {
    try {
        var derniereDate = new Date(req.query.derniereDate);
        const lastUpdatedEtudesAnterieures = await db.getLastUpdatedEtudesAnterieures(
            derniereDate
        );
        if (lastUpdatedEtudesAnterieures) {
            res.status(200);
            res.contentType("application/json");
            return res.json(lastUpdatedEtudesAnterieures);
        }
        return next(
            new HTTP500Error(
                `Error getLastUpdatedEtudesAnterieures - Could not retreive latest updated (${derniereDate}) etudesAnterieures from database`,
                "Error retreiving latest updated etudesAnterieures from database"
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error getLastUpdatedEtudesAnterieures - Could not retreive latest updated (${derniereDate}) etudesAnterieures from database`,
                "Error retreiving latest updated etudesAnterieures from database",
                err
            )
        );
    }
};

/**
 * Fait une invocation du SW peoplesoft
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @param {*} serviceName le nom du service de la requête
 * @returns l'objet DonneesNominatives
 */
const getSIS = async function (req, res, next, serviceName) {
    try {
        let param;
        let sisData;
        if (
            serviceName ===
            config.peoplesoft.adminDonneesNominatives.serviceName
        ) {
            let searchBy;
            let {
                matricule,
                admApplNbr,
                nomPrenom,
                courriel,
                loginUdeM,
                inscritDepuis
            } = req.query;
            if (matricule) {
                param = `matricule=${matricule},numDA=,name=,emailAdr=,loginUdeM=,registredSince_yyyymmdd=`;
                searchBy = `matricule=${matricule}`;
            } else if (admApplNbr) {
                param = `matricule=,numDA=${admApplNbr},name=,emailAdr=,loginUdeM=,registredSince_yyyymmdd=`;
                searchBy = `numDA=${admApplNbr}`;
            } else if (nomPrenom) {
                param = `matricule=,numDA=,name=${nomPrenom},emailAdr=,loginUdeM=,registredSince_yyyymmdd=`;
                searchBy = `name=${nomPrenom}`;
            } else if (courriel) {
                param = `matricule=,numDA=,name=,emailAdr=${courriel},loginUdeM=,registredSince_yyyymmdd=`;
                searchBy = `emailAdr=${courriel}`;
            } else if (loginUdeM) {
                param = `matricule=,numDA=,name=,emailAdr=,loginUdeM=${loginUdeM},registredSince_yyyymmdd=`;
                searchBy = `loginUdeM=${loginUdeM}`;
            } else if (inscritDepuis) {
                param = `matricule=,numDA=,name=,emailAdr=,loginUdeM=,registredSince_yyyymmdd=${inscritDepuis}`;
                searchBy = `inscritDepuis=${inscritDepuis}`;
            } else {
                throw new HTTP400Error(
                    `Erreur getSIS serviceName = ${serviceName} - Aucun des parametres matricule, admApplNbr, nomPrenom, courriel, loginUdeM ou inscritDepuis n'a été fourni`,
                    "Veyez fournir un des parametres suivant: matricule, admApplNbr, nomPrenom, courriel, loginUdeM ou inscritDepuis"
                );
            }
            sisData = await peoplesoft.getSISData(
                searchBy,
                res.locals.reqid,
                serviceName,
                param
            );
        } else {
            let idCIAM = req.idCIAM;
            if (!idCIAM) {
                throw new HTTP401Error(
                    `Erreur getSIS serviceName = ${serviceName} - Aucun idCIAM a été fourni`,
                    "Aucun idCIAM a été fourni"
                );
            }
            let matricule = await ciam.getMatricule(idCIAM);
            if (!matricule) {
                throw new HTTP403Error(
                    `Erreur getSIS serviceName = ${serviceName} - L'usager n'a pas de matricule assigné dans son profil idCIAM = ${idCIAM}`,
                    "L'usager n'a pas de matricule assigné dans son profil CIAM"
                );
            }
            sisData = await peoplesoft.getSISData(
                matricule,
                res.locals.reqid,
                serviceName
            );
        }
        res.status(200);
        res.contentType("application/json");
        return res.json(sisData);
    } catch (err) {
        if (
            err instanceof HTTP400Error ||
            err instanceof HTTP401Error ||
            err instanceof HTTP403Error ||
            err instanceof HTTP404Error
        ) {
            return next(err);
        }
        let newErr;
        if (err.message.includes("non trouvé")) {
            newErr = new HTTP404Error(
                `Erreur getSIS - ${err.message}`,
                "Matricule non trouvé dans Synchro",
                err
            );
        } else if (err.message.includes("Error getSISData - Synchro Error")) {
            newErr = new HTTP503Error(
                `Erreur getSIS serviceName = ${serviceName} - ${err.message}`,
                `Erreur Synchro lors de l'appel à peoplesoft`,
                err
            );
        } else {
            newErr = new HTTP503Error(
                `Erreur getSIS serviceName = ${serviceName} - ${err.message}`,
                `Erreur api lors de l'appel à peoplesoft`,
                err
            );
        }
        return next(newErr);
    }
};

/**
 * Fait une invocation du SW peoplesoft UM_WS_JSON_SEC_DONN_NO pour chercher le données nominatives étant donné un matricule
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns l'objet DonneesNominatives
 */
const getSISDonneesNominatives = async function (req, res, next) {
    return await getSIS(
        req,
        res,
        next,
        config.peoplesoft.donneesNominatives.serviceName
    );
};

/**
 * Fait une invocation du SW peoplesoft
 * UM_WS_JSON_SEC_ETUD_ANT  pour chercher
 * les études antérieures étant donné un matricule
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 * @returns liste les objets EtudesAnterieures
 */
const getSISEtudesAnterieures = async function (req, res, next) {
    return await getSIS(
        req,
        res,
        next,
        config.peoplesoft.etudesAnterieures.serviceName
    );
};

/**
 * Mise à jour de la table LastUpdatedFieldsDN pour garder la trace des modification à la table DN
 * @param {*} donneesNominatives donneesNominatives nouvellement sauvegardé dans la bd
 * @param {*} oldDonneesNominatives ancien donneesNominatives dans la bd
 * @returns idCIAM
 */
const saveLastUpdatedFieldsDN = async function (
    donneesNominatives,
    oldDonneesNominatives
) {
    var lastUpdatedFieldsToMonitor = config.get("lastUpdatedFieldsToMonitor");
    var lastUpdatedFieldsDN = [];
    var i = 0;
    lastUpdatedFieldsToMonitor.forEach((element) => {
        let { [element]: monitoredFieldValue } = donneesNominatives;
        let { [element]: monitoredFieldOldValue } = oldDonneesNominatives;
        if (monitoredFieldValue) {
            var lastUpdatedFields = {
                idPersonne: donneesNominatives.idPersonne,
                fieldName: element,
                fieldValue: monitoredFieldValue,
                fieldOldValue: monitoredFieldOldValue
            };
            lastUpdatedFieldsDN[i] = lastUpdatedFields;
        }
        i++;
    });
    if (lastUpdatedFieldsDN.length > 0) {
        db.saveLastUpdatedFieldsDN(lastUpdatedFieldsDN);
    }
};

/**
 * Ajoute ou modifie une entrée existante dans la table DonneesNominatives avec l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next Ok
 */
const saveDonneesNominatives = async function saveDonneesNominatives(
    req,
    res,
    next
) {
    let jsonDonneesNominatives = req.body;
    try {
        delete jsonDonneesNominatives.createdAt;
        delete jsonDonneesNominatives.updatedAt;

        let oldDonneesNominatives = await db.getDonneesNominatives(
            jsonDonneesNominatives.idPersonne
        );
        let donneesNominatives = await db.saveDonneesNominatives(
            jsonDonneesNominatives
        );
        if (donneesNominatives) {
            res.status(200);
            res.contentType("application/json");
            res.json(donneesNominatives);
            if (oldDonneesNominatives) {
                await saveLastUpdatedFieldsDN(
                    donneesNominatives,
                    oldDonneesNominatives
                );
            }
            return;
        }
        return next(
            new HTTP500Error(
                `Error saveDonneesNominatives - Could not save donneesNominatives to database, empty objet was return by db.saveDonneesNominatives. DonneesNominatives to be saved: ${JSON.stringify(
                    jsonDonneesNominatives
                )}`,
                "Error saving donneesNominatives to database"
            )
        );
    } catch (err) {
        if (err.message.includes("ORA-02291")) {
            return next(
                new HTTP404Error(
                    `Error saveDonneesNominatives - Could not save DonneesNominatives in database: ${
                        err.message
                    }, donneesNominatives: ${JSON.stringify(
                        jsonDonneesNominatives
                    )}`,
                    "idPersonne not found in database",
                    err
                )
            );
        } else if (err.message.includes("ORA-00904")) {
            return next(
                new HTTP400Error(
                    `Error saveDonneesNominatives - Could not save DonneesNominatives in database: ${
                        err.message
                    }, donneesNominatives: ${JSON.stringify(
                        jsonDonneesNominatives
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON ${err.message
                        .split("ORA-00904")
                        .slice(1)}`,
                    err
                )
            );
        } else if (err.message.includes("ORA-")) {
            return next(
                new HTTP400Error(
                    `Error saveDonneesNominatives - Could not save donneesNominatives to database: ${
                        err.message
                    }, donneesNominatives: ${JSON.stringify(
                        jsonDonneesNominatives
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON:${err.message
                        .split(":")
                        .slice(1)}`,
                    err
                )
            );
        }
        return next(
            new HTTP500Error(
                `Error saveDonneesNominatives - Could not save donneesNominatives to database: ${
                    err.message
                }, donneesNominatives:${JSON.stringify(
                    jsonDonneesNominatives
                )}`,
                "Error saving donneesNominatives to database",
                err
            )
        );
    }
};

/**
 * Ajoute ou modifie une entrée existante dans la table FichiersProprietes avec l'idPersonne lié au jeton (ou au query param si enableJWT = false) ainsi que le id [nomFichierProp] fourni dans la requête http
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 */
const saveFichierProprietes = async function saveFichierProprietes(
    req,
    res,
    next
) {
    let jsonFichierProprietes;
    try {
        /*
         * Verifie si le id [nomFichierProp] est present dans la requête
         * Le id fourni dans le url de la requête est celui utilisé par défaut
         */
        if (req.params.id && req.params.id.replace(/\s/g, "") !== "") {
            req.body.id = req.params.id;
        } else {
            return next(
                new HTTP400Error(
                    "Error saveFichierProprietes - Could not identify fichiersProprietes (missing Id [nomFichierProp])",
                    "Could not identify fichiersProprietes (missing Id [nomFichierProp])"
                )
            );
        }

        jsonFichierProprietes = req.body;

        delete jsonFichierProprietes.createdAt;
        delete jsonFichierProprietes.updatedAt;

        //Verifie si le fichier est encodé en base64
        if (
            !(
                Buffer.from(jsonFichierProprietes.propFile, "base64").toString(
                    "base64"
                ) === jsonFichierProprietes.propFile
            )
        ) {
            return next(
                new HTTP400Error(
                    `Error saveFichierProprietes - Property file ${jsonFichierProprietes.id} provided is not BASE64 encoded`,
                    `Property file ${jsonFichierProprietes.id} provided is not BASE64 encoded`
                )
            );
        }

        let fichierProprietes = await db.saveFichiersProprietes(
            jsonFichierProprietes
        );
        if (fichierProprietes) {
            res.status(200);
            res.contentType("application/json");
            return res.json(fichierProprietes);
        }
        //Return
        next(
            new HTTP500Error(
                `Error saveFichierProprietes - Could not save property file to database, empty objet was return by db.saveFichiersProprietes. FichierProprietes to be saved: ${JSON.stringify(
                    jsonFichierProprietes
                )}`,
                "Error saving fichiersProprietes to database"
            )
        );
    } catch (err) {
        if (err.message.includes("ORA-02291")) {
            return next(
                new HTTP404Error(
                    `Error saveFichierProprietes - Could not save FichiersProprietes in database: ${
                        err.message
                    }, FichierProprietes: ${JSON.stringify(
                        jsonFichierProprietes
                    )}`,
                    "idPersonne or id of fichierProp not found in database",
                    err
                )
            );
        } else if (err.message.includes("ORA-00904")) {
            return next(
                new HTTP400Error(
                    `Error saveFichierProprietes - Could not save FichiersProprietes in database: ${
                        err.message
                    }, FichierProprietes: ${JSON.stringify(
                        jsonFichierProprietes
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON ${err.message
                        .split("ORA-00904")
                        .slice(1)}`,
                    err
                )
            );
        } else if (err.message.includes("ORA-")) {
            return next(
                new HTTP400Error(
                    `Error saveFichierProprietes - Could not save FichiersProprietes to database: ${
                        err.message
                    }, FichierProprietes: ${JSON.stringify(
                        jsonFichierProprietes
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON:${err.message
                        .split(":")
                        .slice(1)}`,
                    err
                )
            );
        }
        return next(
            new HTTP500Error(
                `Error saveFichierProprietes - Could not save property file to database: ${
                    err.message
                }, FichierProprietes: ${JSON.stringify(jsonFichierProprietes)}`,
                "Error saving fichiersProprietes to database",
                err
            )
        );
    }
};

/**
 * Modifie une entrée existante dans la table EtudesAnterieures avec l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next Ok
 */
const saveEtudeAnterieure = async function saveEtudeAnterieure(req, res, next) {
    let jsonEtudesAnterieures = req.body;
    try {
        //Le id fourni dans le url de la requête est celui utilisé par défaut
        if (
            !req.params.id ||
            req.params.id.replace(/\s/g, "") === "" ||
            isNaN(req.params.id)
        ) {
            return next(
                new HTTP400Error(
                    "Error saveEtudeAnterieure - Could not identify etudesAnterieures (missing Id)",
                    "Could not identify etudesAnterieures (missing Id)"
                )
            );
        }

        delete jsonEtudesAnterieures.createdAt;
        delete jsonEtudesAnterieures.updatedAt;

        jsonEtudesAnterieures.id = parseInt(req.params.id);
        let etudesAnterieures = await db.saveEtudesAnterieures(
            jsonEtudesAnterieures
        );
        if (etudesAnterieures) {
            res.status(200);
            res.contentType("application/json");
            return res.json(etudesAnterieures);
        }
        return next(
            new HTTP500Error(
                `Error saveEtudeAnterieure - Could not save etudesAnterieures to database, empty objet was return by db.saveEtudesAnterieures. EtudesAnterieures to be saved: ${JSON.stringify(
                    jsonEtudesAnterieures
                )}`,
                "Error saving etudesAnterieures to database"
            )
        );
    } catch (err) {
        if (err.message.includes("ORA-02291")) {
            return next(
                new HTTP404Error(
                    `Error saveEtudeAnterieure - Could not save EtudesAnterieures in database: ${
                        err.message
                    }, EtudesAnterieures: ${JSON.stringify(
                        jsonEtudesAnterieures
                    )}`,
                    "idPersonne not found in database",
                    err
                )
            );
        } else if (err.message.includes("ORA-00904")) {
            return next(
                new HTTP400Error(
                    `Error saveEtudeAnterieure - Could not save EtudesAnterieures in database: ${
                        err.message
                    }, EtudesAnterieures: ${JSON.stringify(
                        jsonEtudesAnterieures
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON ${err.message
                        .split("ORA-00904")
                        .slice(1)}`,
                    err
                )
            );
        } else if (err.message.includes("ORA-")) {
            return next(
                new HTTP400Error(
                    `Error saveEtudeAnterieure - Could not save EtudesAnterieures to database: ${
                        err.message
                    }, EtudesAnterieures: ${JSON.stringify(
                        jsonEtudesAnterieures
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON:${err.message
                        .split(":")
                        .slice(1)}`,
                    err
                )
            );
        }
        return next(
            new HTTP500Error(
                `Error saveEtudeAnterieure - Could not save etudesAnterieures to database: ${
                    err.message
                }, EtudesAnterieures: ${JSON.stringify(jsonEtudesAnterieures)}`,
                "Error saving etudesAnterieures to database",
                err
            )
        );
    }
};

/**
 * Ajoute une nouvelle entrée dans la table EtudesAnterieures avec l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 */
const createEtudeAnterieure = async function createEtudeAnterieure(
    req,
    res,
    next
) {
    let jsonEtudeAnterieure = req.body;

    delete jsonEtudeAnterieure.id;
    delete jsonEtudeAnterieure.createdAt;
    delete jsonEtudeAnterieure.updatedAt;

    try {
        let etudesAnterieures = await db.saveEtudesAnterieures(
            jsonEtudeAnterieure
        );

        if (etudesAnterieures) {
            res.status(200);
            res.contentType("application/json");
            return res.json(etudesAnterieures);
        }
        return next(
            new HTTP500Error(
                `Error createEtudeAnterieure - Could not save etudesAnterieures to database, empty objet was return by db.saveEtudesAnterieures. EtudesAnterieures to be created: ${JSON.stringify(
                    jsonEtudeAnterieure
                )}`,
                "Error saving etudesAnterieures to database"
            )
        );
    } catch (err) {
        if (err.message.includes("ORA-02291")) {
            return next(
                new HTTP404Error(
                    `Error createEtudeAnterieure - Could not create EtudesAnterieures in database: ${
                        err.message
                    }, EtudesAnterieures: ${JSON.stringify(
                        jsonEtudeAnterieure
                    )}`,
                    "idPersonne not found in database",
                    err
                )
            );
        } else if (err.message.includes("ORA-00904")) {
            return next(
                new HTTP400Error(
                    `Error createEtudeAnterieure - Could not create EtudesAnterieures in database: ${
                        err.message
                    }, EtudesAnterieures: ${JSON.stringify(
                        jsonEtudeAnterieure
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON ${err.message
                        .split("ORA-00904")
                        .slice(1)}`,
                    err
                )
            );
        } else if (err.message.includes("ORA-")) {
            return next(
                new HTTP400Error(
                    `Error createEtudeAnterieure - Could not create EtudesAnterieures to database: ${
                        err.message
                    }, EtudesAnterieures: ${JSON.stringify(
                        jsonEtudeAnterieure
                    )}`,
                    `Un mauvais champs a été envoyé. Verifier avec le contrat JSON:${err.message
                        .split(":")
                        .slice(1)}`,
                    err
                )
            );
        }
        return next(
            new HTTP500Error(
                `Error createEtudeAnterieure - Could not save etudesAnterieures to database: ${
                    err.message
                }, EtudesAnterieures: ${JSON.stringify(jsonEtudeAnterieure)}`,
                "Error saving etudesAnterieures to database",
                err
            )
        );
    }
};

/**
 * Efface toutes les entrées liées à la personne dans toutes les tables profilapi de la BD intermediaire avec l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 */
const deletePersonne = async function deletePersonne(req, res, next) {
    let { idPersonne } = req.body;
    try {
        let personneDelete = await db.deletePersonne(idPersonne);
        if (personneDelete) {
            res.status(204);
            res.contentType("application/json");
            return res.json();
        }
        return next(
            new HTTP404Error(
                `Error deletePersonne - Could not delete personne with idPersonne ${idPersonne} from database`,
                "Error deleting personne from database"
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error deletePersonne - Could not delete personne with idPersonne ${idPersonne} from database: ${err.message}`,
                "Error deleting personne from database",
                err
            )
        );
    }
};

/**
 * Efface les entrées dans la table EtudesAnterieures liées l'idPersonne lié au jeton (ou au query param si enableJWT = false)
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 */
const deleteEtudeAnterieure = async function deleteEtudeAnterieure(
    req,
    res,
    next
) {
    let { idPersonne } = req.body;
    let idEtudeAnterieure;
    try {
        //Id EtudeAnt
        if (req.params.id) {
            idEtudeAnterieure = parseInt(req.params.id);
        } else {
            return next(
                new HTTP400Error(
                    "Error deleteEtudeAnterieure - Could not identify etudesAnterieures (missing Id)",
                    "Could not identify etudesAnterieures (missing Id)"
                )
            );
        }
        let etudeAnterieureDeleted = await db.deleteEtudeAnterieure(
            idEtudeAnterieure,
            idPersonne
        );
        if (etudeAnterieureDeleted) {
            res.status(204);
            res.contentType("application/json");
            return res.json();
        }
        return next(
            new HTTP404Error(
                `Error deleteEtudeAnterieure - Could not delete etudesAnterieures [id: ${idEtudeAnterieure}] from database`,
                "Error deleting etudesAnterieures from database"
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error deleteEtudeAnterieure - Could not delete etudesAnterieures [id: ${idEtudeAnterieure}] from database: ${err.message}`,
                "Error deleting etudesAnterieures from database",
                err
            )
        );
    }
};

/**
 * Efface l'entrée dans la table FichiersProprietes liées l'idPersonne lié au jeton (ou au query param si enableJWT = false) ainsi que le id [nomFichierProp] fourni dans la requête http
 * @param {*} req express http request
 * @param {*} res express http response
 * @param {*} next express next
 */
const deleteFichierProprietes = async function deleteFichierProprietes(
    req,
    res,
    next
) {
    let idFichierProprietes;
    let { idPersonne } = req.body;
    try {
        if (req.params.id) {
            idFichierProprietes = req.params.id;
        } else {
            return next(
                new HTTP400Error(
                    "Error deleteFichierProprietes - Could not identify the fichiersProprietes (missing Id [nomFichierProp])",
                    "Could not identify the fichiersProprietes (missing Id [nomFichierProp])"
                )
            );
        }

        let fichierProprietesDeleted = await db.deleteFichiersProprietes(
            idFichierProprietes,
            idPersonne
        );
        if (fichierProprietesDeleted) {
            res.status(204);
            res.contentType("application/json");
            return res.json();
        }
        next(
            new HTTP404Error(
                `Error deleteFichierProprietes - Could not delete fichiersProprietes of idPersonne ${idPersonne} from database`,
                "Error deleting fichiersProprietes from database"
            )
        );
    } catch (err) {
        return next(
            new HTTP500Error(
                `Error deleteFichierProprietes - Could not delete fichiersProprietes of idPersonne ${idPersonne} from database: ${err.message}`,
                "Error deleting fichiersProprietes from database",
                err
            )
        );
    }
};

module.exports = {
    fusionProfils,
    getPersonne,
    getIdPersonne,
    getDonneesNominatives,
    getEtudesAnterieures,
    getEtudeAnterieure,
    getFichiersProprietes,
    getFichierProprietes,
    getLastUpdatedDonneesNominatives: getAdminDonneesNominatives,
    getLastUpdatedEtudesAnterieures,
    getSIS,
    getSISDonneesNominatives,
    getSISEtudesAnterieures,
    saveDonneesNominatives,
    saveEtudeAnterieure,
    saveFichierProprietes,
    createEtudeAnterieure,
    deletePersonne,
    deleteEtudeAnterieure,
    deleteFichierProprietes
};
