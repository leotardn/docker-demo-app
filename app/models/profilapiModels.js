"use strict";

class Personne {
    constructor(idPersonne, createdAt) {
        this.idPersonne = idPersonne;
        this.createdAt = createdAt;
    }
}

class LastUpdatedFieldsDN {
    constructor(idPersonne, fieldName, fieldValue, fieldOldValue, updatedAt) {
        this.idPersonne = idPersonne;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.fieldOldValue = fieldOldValue;
        this.updatedAt = updatedAt;
    }
}

class CIAM {
    constructor(idCIAM, idPersonne, createdAt, updatedAt) {
        this.idCIAM = idCIAM;
        this.idPersonne = idPersonne;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}

class DonneesNominatives {
    constructor(
        idPersonne,
        frenchTestExempt,
        allowCommunications,
        levelOfStudy,
        fieldOfStudy,
        latestDiploma,
        UMWappArmeesCdn,
        UMLastName,
        UMFirstName,
        UMLegalName,
        UMPreferredName,
        sex,
        birthday,
        birthcountry,
        birthplace,
        languageCd,
        langCd,
        parentRole1,
        parentRole2,
        UMLastName1,
        UMFirstName1,
        UMLastName2,
        UMFirstName2,
        country,
        address1,
        address2,
        city,
        state,
        postal,
        emailAddr,
        phoneNumber1,
        phoneType1,
        UMExtension1,
        UMPhoneCountry1,
        phoneNumber2,
        phoneType2,
        UMExtension2,
        UMPhoneCountry2,
        phoneNumber3,
        phoneType3,
        UMExtension3,
        UMPhoneCountry3,
        UMPrefPhone,
        acceptPhoneText,
        UMStatAuCan,
        firstNationsMetisInuit,
        country2,
        UMFirstProv,
        UMSuppDoc1,
        profileType,
        emplId,
        campusId,
        profLangCD,
        UMIntrDescr,
        createdAt,
        updatedAt
    ) {
        this.idPersonne = idPersonne;
        this.frenchTestExempt = frenchTestExempt;
        this.allowCommunications = allowCommunications;
        this.levelOfStudy = levelOfStudy;
        this.fieldOfStudy = fieldOfStudy;
        this.latestDiploma = latestDiploma;
        this.UMWappArmeesCdn = UMWappArmeesCdn;
        this.UMLastName = UMLastName;
        this.UMFirstName = UMFirstName;
        this.UMLegalName = UMLegalName;
        this.UMPreferredName = UMPreferredName;
        this.sex = sex;
        this.birthday = birthday;
        this.birthcountry = birthcountry;
        this.birthplace = birthplace;
        this.languageCd = languageCd;
        this.langCd = langCd;
        this.parentRole1 = parentRole1;
        this.parentRole2 = parentRole2;
        this.UMLastName1 = UMLastName1;
        this.UMFirstName1 = UMFirstName1;
        this.UMLastName2 = UMLastName2;
        this.UMFirstName2 = UMFirstName2;
        this.country = country;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.postal = postal;
        this.emailAddr = emailAddr;
        this.phoneNumber1 = phoneNumber1;
        this.phoneType1 = phoneType1;
        this.UMExtension1 = UMExtension1;
        this.UMPhoneCountry1 = UMPhoneCountry1;
        this.phoneNumber2 = phoneNumber2;
        this.phoneType2 = phoneType2;
        this.UMExtension2 = UMExtension2;
        this.UMPhoneCountry2 = UMPhoneCountry2;
        this.phoneNumber3 = phoneNumber3;
        this.phoneType3 = phoneType3;
        this.UMExtension3 = UMExtension3;
        this.UMPhoneCountry3 = UMPhoneCountry3;
        this.UMPrefPhone = UMPrefPhone;
        this.acceptPhoneText = acceptPhoneText;
        this.UMStatAuCan = UMStatAuCan;
        this.firstNationsMetisInuit = firstNationsMetisInuit;
        this.country2 = country2;
        this.UMFirstProv = UMFirstProv;
        this.UMSuppDoc1 = UMSuppDoc1;
        this.profileType = profileType;
        this.emplId = emplId;
        this.campusId = campusId;
        this.profLangCD = profLangCD;
        this.UMIntrDescr = UMIntrDescr;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}

class EtudesAnterieures {
    constructor(
        id,
        yearFrom,
        UMMonthFrom,
        yearTo,
        UMMonthTo,
        ACADPlan,
        ACADSubPlanModalite,
        ACADSubPlanInstrument,
        ACADSubPlanBaccap,
        ACADSubPlanOption,
        ACADSubPlanHonor,
        extOrgId,
        UMOthrInstFlag,
        country,
        state,
        UMOthrInstDescr,
        UMOthrInstFlng,
        descr,
        codeOfDegreeID,
        degree,
        typeDiploma,
        specialisationPremiere1,
        specialisationPremiere2,
        specialisationPremiere3,
        specialisationTerminale1,
        specialisationTerminale2,
        optionMath,
        BICoursesCompleted,
        descr100,
        degreeStatus,
        progStatus,
        UMAnnee,
        UMMois,
        idPersonne,
        createdAt,
        updatedAt
    ) {
        this.id = id;
        this.yearFrom = yearFrom;
        this.UMMonthFrom = UMMonthFrom;
        this.yearTo = yearTo;
        this.UMMonthTo = UMMonthTo;
        this.ACADPlan = ACADPlan;
        this.ACADSubPlanModalite = ACADSubPlanModalite;
        this.ACADSubPlanInstrument = ACADSubPlanInstrument;
        this.ACADSubPlanBaccap = ACADSubPlanBaccap;
        this.ACADSubPlanOption = ACADSubPlanOption;
        this.ACADSubPlanHonor = ACADSubPlanHonor;
        this.extOrgId = extOrgId;
        this.UMOthrInstFlag = UMOthrInstFlag;
        this.country = country;
        this.state = state;
        this.UMOthrInstDescr = UMOthrInstDescr;
        this.UMOthrInstFlng = UMOthrInstFlng;
        this.descr = descr;
        this.codeOfDegreeID = codeOfDegreeID;
        this.degree = degree;
        this.typeDiploma = typeDiploma;
        this.specialisationPremiere1 = specialisationPremiere1;
        this.specialisationPremiere2 = specialisationPremiere2;
        this.specialisationPremiere3 = specialisationPremiere3;
        this.specialisationTerminale1 = specialisationTerminale1;
        this.specialisationTerminale2 = specialisationTerminale2;
        this.optionMath = optionMath;
        this.BICoursesCompleted = BICoursesCompleted;
        this.descr100 = descr100;
        this.degreeStatus = degreeStatus;
        this.progStatus = progStatus;
        this.UMAnnee = UMAnnee;
        this.UMMois = UMMois;
        this.idPersonne = idPersonne;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}

class FichiersProprietes {
    constructor(id, propFile, idPersonne, createdAt, updatedAt) {
        this.id = id;
        this.propFile = propFile;
        this.idPersonne = idPersonne;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}

module.exports = {
    DonneesNominatives: DonneesNominatives,
    EtudesAnterieures: EtudesAnterieures,
    FichiersProprietes: FichiersProprietes,
    CIAM: CIAM,
    LastUpdatedFieldsDN: LastUpdatedFieldsDN,
    Personne: Personne
};
