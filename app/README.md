# profilapi

## Table des matières

<mark>Cet automatisme ne fonctionne pas encore dans Gitlab</mark>
[TOC]

## Description

L'API profile s'inscrit dans la ligné d'interfaces créées au sein du projet RECA (Refonte etc..). Le but étant de manipuler et sauvegarder dans une BD intermédiaire les profiles des candidats du site d'admission. Le code profilapi reçois des appels en écriture/lecture et interagit avec la BD à l'aide de l'ORM typeORM.

## Architecture

### Composants

Voici la structure de l'profile node.js en entière.
![Composantes NodeJs profilapi](./docs/DiagrammeProfileAPI.png)

### Problématique

Nous avons besoin de ce patron pour créer une couche d'intégration permettant de mieux manipuler les informations provenant du site d'admission tout en ayant une haute disponibilé et vitesse

### Solution choisie

Nous avons choisi d'écrire ce code en Node.js pour pouvoir tirer profit de la structure Express.js et ses atouts dans la construction d'un API web. NodeJs apporte une grande variété de librairies dont typeORM qui est particulièrement utilisée dans le code.

### Patrons d'intégration

Le patron d'intégration utilisé pour ce code est Exposition API

#### Patron Exposition API

![Exposition API](./docs/PatronExpositionAPI.png)

### Patron de sécurité

#### Jeton OAuth2/JWT

Ce patron est plus complexe, mais il a un avantage majeur : la validation d'autorisation est associée au propriétaire de l'information. Ce code vérifie si le jeton est valide et vérifié si un champ en particulier existe avec la valeur voulue. Un de ces champs peut être l'email.

_Voici une représentation graphique (du "happy path"):_
<mark>Utiliser mermaid pour représenter les algorithmes dans votre solution</mark>

<!--Guide pour ces graphiques : https://mermaidjs.github.io/#/sequenceDiagram-->

```mermaid
sequenceDiagram
    Participant Site Admission
    Participant Utilisateur
    Participant F5 AZ
    Participant F5 API
    Participant API
    Site Admission->>Site Admission : Le site veut faire un appel d'API
    Site Admission->>Utilisateur : Diriger vers l'authentification
    Utilisateur->>F5 AZ : Authentification
    F5 AZ->>Site Admission : Retour du code d'autorisation
    Site Admission ->> F5 AZ : Requête de Jeton
    F5 AZ ->> Site Admission : Jeton retourné
    Note right of F5 AZ: Jeton valide 5 min
    Site Admission->>F5 API: Appel API + Jeton
    F5 API ->>F5 API : Validation Signature
    F5 API ->>F5 API : Validation Expiration
    F5 API ->>F5 API : Validation Claims
    F5 API->> profilapi: Appel API
    profilapi ->>profilapi : Validation Signature
    profilapi ->>profilapi : Validation Claims
    profilapi->>F5 API : Réponse
    F5 API->>Site Admission: Réponse
```

## Références

### Langage et plate-forme

-   Npm et [Node.js](https://nodejs.org/en/) - à installer avant de rouler !
-   C'est un projet [Express](https://expressjs.com/), qui est une plate-forme Node populaire pour exposer des APIs HTTP REST
-   Le projet initial a été créé avec [Express Generator](https://expressjs.com/en/starter/generator.html)
-   Concepts importants : [Middlewares](http://expressjs.com/en/guide/using-middleware.html) et [Router](http://expressjs.com/en/4x/api.html#router)

-   Livre utile Evan M. Hahn chez Manning : [Express in Action](https://www.manning.com/books/express-in-action) (Dispo au bureau de Ghislain)
-   J'utilise [nodemon](https://nodemon.io/) pour mes tests "live". C'est optionnel. Nodemon détecte les changements dans le code, et redémarre le serveur automatiquement (on ne veut pas ça en DEV ou PROD ;-))
-   Je configure les tests unitaires avec [SuperTest](https://www.npmjs.com/package/supertest)

### Modules Node utilisés

-   [axios](https://www.npmjs.com/package/axios) : Client Http qui permet des appels avec Promise
-   [jwks-rsa](https://www.npmjs.com/package/jwks-rsa) : Pour la validation de la sécurité "JWT"
-   [typeorm](https://www.npmjs.com/package/typeorm) : Pour gérer et manipuler les données dans la BD Oracle
-   [config](https://www.npmjs.com/package/config) : Pour gérer les fichiers de configuration json facilement
-   [morgan](https://www.npmjs.com/package/morgan) : Pour créer une entrée de journal en mode "middleware/Express"
-   [uuid](https://www.npmjs.com/package/uuid) : Pour créer des Id de requête uniques (pour les journaux)
-   [winston](https://www.npmjs.com/package/winston) Pour envoyer des entrées de journal dans différents dispositifs.

## Sections du code

### Racine

-   _README.md_: Le (présent) fichier de documentation
-   _package.json_: La configuration npm (package.json) et le code d'exécution
-   _app.js_: l'profile Express. Pas très important, parce que la majorité de la logique est dans le répertoire _middleware_

### bin

généré par Express Generator

### config

-   _default.json_: Propritétés qui s'appliquent à tous les déploiements
-   _NODE_ENV.json_: (pas sauvegardé dans Git) : configuration spécifique au déploiement dans _NODE_ENV_ (variable d'environnement). Selon la valeur de NODE_ENV, il faut créer un fichier NODE_ENV.json, basé sur les fichiers de templates...

### docs

Fichiers utilisés dans le présent document

### entities

Schéma de la donnée que tu veux manipuler avec typeORM (composition et relation entre chaque élément)

### lib

Modules maison utilisés dans le code pour la gestion d'erreurs et la journalisation. De plus, la logique principale a été extraite du middleware et mise dans les modules dans ce répértoire.

### middleware

Logique pour l'accès et mise à jour des données de profil.

### models

Le model est une simple représentation de la table dans la base des données

### test

Tests unitaires

npm run testunit

Tests d'intégration

npm run test --testenv={environnement}

## Tests et travail sur ordinateur local

### Démarrer

Voici comment tester le code en mode local

-   Ajuster la variable d'environnement "NODE_ENV" à la même valeur que le prefixe du fichier précédent (ici : NODE_ENV=dev)
-   Installer [Node.js](https://nodejs.org/en/)
-   Installer [mocha](https://mochajs.org/)
-   (optionnel) Installer [nodemon](https://nodemon.io/)
-   Cloner ce projet
-   Transformer un fichier de "template" config en vrai fichier de config (ex: config/dev.json)
-   dans le dossier de projet, taper

```javascript
npm install
```

et

```javascript
npm run mon
```

pour rouler avec [nodemon](https://nodemon.io/)
ou

```javascript
npm start
```

pour rouler le code en mode serveur

```javascript
npm --testenv=dev run test
```

pour rouler la série de tests automatisés sur le serveur Dev, pour Prod testenv=prod (les noms des serveurs peuvent être modifiés dans le fichier _config/default.json_).

Pour exécuter les tests, lizez les instructions ci-dessous.

Attention: l'ordre d'exécution des tests est important:
des données sont créées et ensuite effacées de la base de données.
Besoins pour les tests d'intégration:
une connexion avec la base de données
l'api profil doit être exécutée
l'api ciam doit être exécutée
l'api admission doit être exécutée (pour la fusion)

```bash
curl -v -X GET -H "Accept: profile/json" -H "Content-Type: profile/json" "http://localhost:3011/personne"
```

pour faire un appel à l'API directement.

## Qualité du code

Pour garantir la qualité du code, nous utilisons le module node pre-commit pour forcer l'exécution réussie de prettier, de eslint, des tests unitaires avant un commit et du seuil établi pour la couverture des tests.

[Documentation sur le module pre-commit](https://www.npmjs.com/package/pre-commit)

Le package.json continent les informations sur les commandes qui seront exécutées avant chaque commit.

Pour utiliser le module pre-commit de node, nous avons ajouter dans le répertoire [.githooks](.githooks/pre-commit), le hook pre-commit. De plus, il faut exécuter la commande suivante de manière à ce que git checher dans ce répertoire le hook:

`git config --local core.hooksPath .githooks/`

On peut toujours sauter le pre-commit, en utilisant la commande commit de git avec l'option:

` --no-verify`

### linter

eslintrc.json contient la configuration de eslint pour le projet. Pour exécuter eslint:

`npm run lint:check`

### prettier

prettierrc.json contient la configuration de prettier pour le projet. Pour exécuter eslint:

`npm run format:write`

### nyc

Pour fournir un rapport sur la coverture des tests, nous utilisons nyc. Pour obtenir le rapport sur la couverture des tests, il faut exécuter la commande:

`npm run coverage`

Le fichier [coverage/index.html](coverage/index.html) montre le rapport sur la couverture des tests.coverage/index.html

## Déploiement et Outils de maintenance

Le code a été créé avec l'intention de déployer dans un système de conteneurs.
Comme un tel système n'est pas disponible actuellement, nous utilisons des VM Linux, et le code est configuré par les administrateurs comme un service "System D" (en utilisant [une méthode sembable à celle-ci](https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6)).

### Étapes de déploiement

-   Le serveur Linux doit être bien configuré avec:
-   Node et npm installés
-   Le client Oracle installé et correctement configuré
-   Une configuration de service SystemD, permettant les commandes ci-bas (section _Commandes de maintenance_)
-   Le code doit être copié au bon endroit (éventuellement par un processus CI/CD)
-   Le fichier de configuration doit être correctement constitué dans le répertoire _config_ (voir section suivante)

### Configuration

Le fichier "_config/default.json_" doit être utilisé tel quel.
On doit utiliser les fichiers "_config/x.template.json_" pour créer un fichier "_config/x.json_", ou x est la même valeur que la variable d'environnement _NODE_ENV_.
Voici le contenu des deux fichiers de configuration:

-   _default.json_ : Constantes importantes, Configuration de journalisation, messages d'erreurs
-   _x.json_ : Configuration de connexion de base de données Oracle, Configuration de validation de Jeton JWT (associé au serveur OAuth2)

### Migration BD

Documentation sur typeorm: https://typeorm.io/

Pour faire la migration BD avec avec TypeORM deux commandes doivent être roulés:

Première commande: génère le fichier avec les modifications à faire et aussi les commandes pour pour un retour en arrière. Notez que nous pouvons exécuter par commande en fournissant le fichier de configuration qui indique l'environnement:

```javascript
npm run typeorm -- migration:generate -n refactoring --outputJs --config ./config/ormconfigLocal.json
npm run typeorm -- migration:generate -n refactoring --outputJs --config ./config/ormconfigDev.json
npm run typeorm -- migration:generate -n refactoring --outputJs --config ./config/ormconfigTest.json
npm run typeorm -- migration:generate -n refactoring --outputJs --config ./config/ ormconfigPreProd.json
npm run typeorm -- migration:generate -n refactoring --outputJs --config ./config/ormconfigProd.json
```

Cette commande va crée un dossier "migration" à la racine de l'application ainsi qu'un script .js à l'interieur. Ce script va contenir plusieurs commandes SQL qui peuvent être modifié, extraite et roulé individuellement si c'est le comportement voulu ou donné à DBA.
Le fichier .js est généré en comparant tes entities nouvellement modifiées contre la bd configurée dans _config/ormconfig.json_.

Une fois le script obtenu, enlevez la variable "name" avant la prochaine étape. Et rajouter `ALTER SESSION SET NLS_LENGTH_SEMANTICS='CHAR'` en tant que query pour pour avoir des varchar de type "CHAR" à la place de Byte.
Comme dans la session, par défaut la valeur de la variable NLS_LENGTH_SEMANTICS est du type "BYTE", toutes les commandes de création de la bd seront générés. Il faut donc les supprimer et laisser seulement les commandes qui effectuent le changement désiré.

Deuxième commande: exécution direct du script de migration avec la commande suivante:

```javascript
npm run typeorm migration:run -- --config ./config/ormconfigLocal.json
npm run typeorm migration:run -- --config ./config/ormconfigDev.json
npm run typeorm migration:run -- --config ./config/ormconfigTest.json
npm run typeorm migration:run -- --config ./config/ormconfigPreProd.json
npm run typeorm migration:run -- --config ./config/ormconfigProd.json
```

### Commandes de maintenance

**Scripts Linux en DEV et PROD pour le déploiement et le support**
Sur les serveurs de DEV et PROD, ce service Node a été configuré comme un service "systemd". Il faut donc utiliser [la commande systemctl](https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units) pour contrôler ce service.
On ne doit pas démarrer manuellement le serveur avec les commandes ci-haut, mais utiliser les commandes suivantes:

```bash
#Pour les administrateurs en dev
sudo npm
sudo systemctl enable node-profilapi@npmadm
sudo systemctl disable node-profilapi@npmadm
sudo systemctl edit node-profilapi@npmadm
#Pour les administrateurs en test
systemctl --user --full edit node-profilapi.service
systemctl status node-profilapi.service --user
systemctl --user enable --now node-profilapi.service
#Pour les opérateurs en dev
sudo systemctl stop node-profilapi@npmadm
sudo systemctl start node-profilapi@npmadm
#Pour les opérateurs en test
sudo systemctl --user stop node-profilapi
sudo systemctl --user start node-profilapi
#Lecture des journaux en dev
sudo journalctl -u node-profilapi@npmadm -f
#Lecture des journaux en test
journalctl --user --user-unit node-profilapi.service -f
```
