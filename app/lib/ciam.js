"use strict";

const axios = require("axios");
const config = require("config");

/**
 * Fait appel au service ciamapi pour crée un idPersonne et
 * mettre à jour le profil okta
 * @param {*} idCIAM
 * @returns matricule
 */
const getMatricule = async function (idCIAM) {
    try {
        let endpointURL = `${config.serviceCiamUrl}/compte/${idCIAM}`;
        let axiosConfig = {
            method: "get",
            url: endpointURL,
            proxy: false,
            headers: {
                "Content-Type": "application/json"
            }
        };

        let response = await axios(axiosConfig);
        let { udem_matricule: matricule } = response.data;
        //Verifie si l'usager a un matricule dans son profil CIAM. Cas échéant, si le matricule est absent on retourne une erreur  401,
        if (matricule && matricule.replace(/\s/g, "") !== "") {
            return matricule;
        }
        return undefined;
    } catch (err) {
        if (err.response && err.response.status) {
            // eslint-disable-next-line eqeqeq
            if (err.response.status == 404) {
                return undefined;
            }
        }
        throw new Error(`Error getMatricule - ${err.message}`, err);
    }
};

module.exports = {
    getMatricule: getMatricule
};
