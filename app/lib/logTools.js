/* eslint-disable capitalized-comments */
/* eslint-disable multiline-comment-style */
//Tools for log outputs
"use strict";
/*
 * Requiring `winston-mail` will expose
 * `winston.transports.Mail`
 */
//require("winston-mail");
const Mail = require("winston-mail").Mail;
const config = require("config");
const morgan = require("morgan");
var os = require("os");
const { v4: uuidv4 } = require("uuid");
const winston = require("winston");
const winstonLogStash = require("winston3-logstash-transport");

var reqid = config.constantes.NOT_APPLICABLE;
/**
 * Ajoute un message de type "error" au journal
 * @param {uuid} id id de la requête http
 * @param {*} message message à ajouter dans le journal
 * @param {*} idCIAM idCIAM de l'usager
 * @param {*} idPersonne idPersonne de l'usager
 */
const critical = function critical(
    id = reqid,
    message,
    idCIAM,
    idPersonne,
    errStack
) {
    if (id === "currentId") {
        id = reqid;
    }

    if (!idCIAM) {
        idCIAM = config.constantes.NOT_APPLICABLE;
    }
    if (!idPersonne) {
        idPersonne = config.constantes.NOT_APPLICABLE;
    }

    logger.critical(message, {
        reqid: id,
        idCIAM: idCIAM,
        idPersonne: idPersonne,
        errStack
    });
};

/**
 * Ajoute un message de type "error" au journal
 * @param {uuid} id id de la requête http
 * @param {*} message message à ajouter dans le journal
 * @param {*} idCIAM idCIAM de l'usager
 * @param {*} idPersonne idPersonne de l'usager
 */
const error = function error(id = reqid, message, idCIAM, idPersonne) {
    if (id === "currentId") {
        id = reqid;
    }

    if (!idCIAM) {
        idCIAM = config.constantes.NOT_APPLICABLE;
    }
    if (!idPersonne) {
        idPersonne = config.constantes.NOT_APPLICABLE;
    }
    logger.error(message, {
        reqid: id,
        idCIAM: idCIAM,
        idPersonne: idPersonne
    });
};

/**
 * Ajoute un message de type "warn" au journal
 * @param {uuid} id id de la requête http
 * @param {*} message message à ajouter dans le journal
 * @param {*} idCIAM idCIAM de l'usager
 * @param {*} idPersonne idPersonne de l'usager
 */
const warn = function warn(id = reqid, message, idCIAM, idPersonne) {
    if (id === "currentId") {
        id = reqid;
    }

    if (!idCIAM) {
        idCIAM = config.constantes.NOT_APPLICABLE;
    }
    if (!idPersonne) {
        idPersonne = config.constantes.NOT_APPLICABLE;
    }

    logger.warn(message, { reqid: id, idCIAM: idCIAM, idPersonne: idPersonne });
};

/**
 * Ajoute un message de type "info" au journal
 * @param {uuid} id id de la requête http
 * @param {*} message message à ajouter dans le journal
 * @param {*} idCIAM idCIAM de l'usager
 * @param {*} idPersonne idPersonne de l'usager
 */
const info = function info(id = reqid, message, idCIAM, idPersonne) {
    if (id === "currentId") {
        id = reqid;
    }

    if (!idCIAM) {
        idCIAM = config.constantes.NOT_APPLICABLE;
    }
    if (!idPersonne) {
        idPersonne = config.constantes.NOT_APPLICABLE;
    }

    logger.info(message, { reqid: id, idCIAM: idCIAM, idPersonne: idPersonne });
};

/**
 * Ajoute un message de type "http" au journal
 * @param {uuid} id id de la requête http
 * @param {*} message message à ajouter dans le journal
 * @param {*} idCIAM idCIAM de l'usager
 * @param {*} idPersonne idPersonne de l'usager
 * @param {*} reponseTime temps écoulé pour servir la requête http
 */
const http = function http(
    id = reqid,
    message,
    idCIAM,
    idPersonne,
    reponseTime
) {
    //if responseTime is defined then idCIAM and idPersonne will be defined (HTTP RESPONSE endLogMorgan function)
    if (reponseTime) {
        logger.http(message, {
            reqid: id,
            idCIAM: idCIAM,
            idPersonne: idPersonne,
            reponseTime: reponseTime
        });
    } else {
        logger.http(message, { reqid: id });
    }
};

/**
 * Ajoute un message de type "debug" au journal
 * @param {uuid} id id de la requête http
 * @param {*} message message à ajouter dans le journal
 * @param {*} idCIAM idCIAM de l'usager
 * @param {*} idPersonne idPersonne de l'usager
 */
const debug = function debug(id = reqid, message, idCIAM, idPersonne) {
    if (id === "currentId") {
        id = reqid;
    }

    if (!idCIAM) {
        idCIAM = config.constantes.NOT_APPLICABLE;
    }
    if (!idPersonne) {
        idPersonne = config.constantes.NOT_APPLICABLE;
    }

    logger.debug(message, {
        reqid: id,
        idCIAM: idCIAM,
        idPersonne: idPersonne
    });
};

/*
 * Define your severity levels.
 * With them, You can create log files,
 * see or hide levels based on the running ENV.
 */
const levels = {
    critical: 0,
    error: 1,
    warn: 2,
    info: 3,
    http: 4,
    debug: 5
};

/*
 * This method set the current severity based on
 * the current NODE_ENV: show all the log levels
 * if the server was run in development mode; otherwise,
 * if it was run in production, show only warn and error messages.
 */
const level = () => {
    const logslevel = process.env.npm_config_logslevel;
    if (
        logslevel === "critical" ||
        logslevel === "error" ||
        logslevel === "warn" ||
        logslevel === "info" ||
        logslevel === "http" ||
        logslevel === "debug"
    ) {
        return logslevel;
    }
    return config.get("logLevel");
};

/*
 * Define different colors for each level.
 * Colors make the log message more visible,
 * adding the ability to focus or ignore messages.
 */
const colors = {
    critical: "bold red",
    error: "red",
    warn: "yellow",
    info: "green",
    http: "magenta",
    debug: "white"
};

/*
 * Tell winston that you want to link the colors
 * defined above to the severity levels.
 */
winston.addColors(colors);

let logFormat = winston.format.combine(
    // Add the message timestamp with the preferred format
    winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }),
    //Define the label to be sent to logstash
    winston.format.label({ label: config.get("configServer.appName") }),
    winston.format.printf(
        (info) =>
            `${info.timestamp} ${info.level} ${info.label} ${
                process.env.NODE_ENV || "dev"
            }: ${info.reqid} - ${info.message}`
    ),
    // Tell Winston that the logs must be colored
    winston.format.colorize({ all: true })
);

var emailErrTransportOptions = {
    to: config.smtp.to,
    from: config.smtp.from,
    subject: config.smtp.subjectErr,
    host: config.smtp.host,
    port: config.smtp.port,
    html: true,
    username: config.smtp.username,
    password: config.smtp.password,
    level: config.smtp.level,
    handleExceptions: false,
    handleRejections: false,
    silent: config.smtp.silent,
    tls: config.smtp.tls,
    formatter: ({ message, reqid, idCIAM, errStack }) => {
        let body;
        let logMsg =
            new Date()
                .toISOString()
                .replace(/T/, " ") // replace T with a space
                .replace(/Z/, " ") // replace Z with a space
                .replace(/\./, ":") + // replace "." for millisec with a ":"
            " " +
            "reqId: " +
            reqid +
            " " +
            "loginOkta: " +
            idCIAM +
            " \n" +
            message;

        body = `<h2>Erreur du service ${config.configServer.appName}</h2>
            <h3>Error message</h3>
            </body>
                <pre>${logMsg}</pre>
            </body>
            <h3>Error Stack</h3>
            </body>
                <pre>${errStack}</pre>
            </body>
            <h3>Additional Info</h3>
            </body>
                <pre>Server Name: ${os.hostname()}
API Version: ${process.env.npm_package_version}
                </pre>
            </body>`;
        return body;
    }
};

var emailExceptionTransportOptions = {
    to: config.smtp.to,
    from: config.smtp.from,
    subject: config.smtp.subjectException,
    host: config.smtp.host,
    port: config.smtp.port,
    username: config.smtp.username,
    password: config.smtp.password,
    html: true,
    level: config.smtp.level,
    silent: config.smtp.silent,
    tls: config.smtp.tls,
    formatter: ({ message, reqid = "unhandledException" }) => {
        let body;
        let logMsg =
            new Date()
                .toISOString()
                .replace(/T/, " ") // replace T with a space
                .replace(/Z/, " ") // replace Z with a space
                .replace(/\./, ":") + // replace "." for millisec with a ":"
            " " +
            "reqId: " +
            reqid +
            " " +
            " \n" +
            message;

        body = `<h2>Exception service ${config.configServer.appName}</h2>
            <h3>Exception message</h3>
            </body>
                <pre>${logMsg}</pre>
            </body>
            <h3>Additional Info</h3>
            </body>
                <pre>Server Name: ${os.hostname()}
API Version: ${process.env.npm_package_version}
                </pre>
            </body>`;
        return body;
    }
};

/*
 * Define which transports the logger must use to print out messages.
 * In this example, we are using two different transports
 */
const transports = [
    // Allow the use the console to print the messages
    new winston.transports.Console({
        handleExceptions: true,
        handleRejections: true,
        // Define the format of the message showing the timestamp, the level and the message
        format: logFormat
    }),

    // Allow logs to be sent to logstash and ElasticSearch
    new winstonLogStash({
        mode: config.get("logstash.mode"),
        host: config.get("logstash.host"),
        port: config.get("logstash.port"),
        level: config.get("logstash.level"),
        silent: config.get("logstash.silent"),
        maxConnectRetries: config.get("logstash.maxConnectRetries"),
        timeoutConnectRetries: config.get("logstash.timeoutConnectRetries"),
        trailingLineFeed: true,
        sslEnable: config.get("logstash.sslEnable"),
        sslKey: process.env.LOGSTASH_SERVER_KEY,
        sslCert: process.env.LOGSTASH_SERVER_CERT,
        sslCA: process.env.LOGSTASH_SERVER_CA,
        rejectUnauthorized: false,
        formatted: false,
        format: winston.format.combine(
            winston.format.json(),
            //Define the label to be sent to logstash
            winston.format.label({ label: config.get("configServer.appName") })
        )
    })
];

let logger = winston.createLogger({
    exitOnError: true,
    level: level(),
    levels: levels,
    //format: logFormat,
    transports: transports,
    handleRejections: true,
    handleExceptions: true,

    exceptionHandlers: [new Mail(emailExceptionTransportOptions)]
});
logger.add(new Mail(emailErrTransportOptions));

/*
 *Const skip = () => {
 *    const env = process.env.NODE_ENV || "dev";
 *    return env !== "dev";
 *};
 */

const requestId = function requestId(req, res, next) {
    reqid = uuidv4();
    res.locals.reqid = reqid;
    // Création d'un "token morgan" réutilisable - la valeur sera toujours disponible PAR REQUETE, à l'aide de res.locals.reqid
    morgan.token("id", function (req, res) {
        return res.locals.reqid;
    });
    next();
};

const endLogMorgan = morgan(
    function (tokens, req, res) {
        var message = [
            "RESPONSE",
            tokens["remote-addr"](req, res),
            `"${tokens.method(req, res)}`,
            `${tokens.url(req, res)}"`,
            tokens.status(req, res),
            tokens.res(req, res, "content-length"),
            "-",
            `"${tokens["response-time"](req, res)}`,
            'ms"',
            tokens["user-agent"](req, res)
        ].join(" ");
        var responseTime = tokens["response-time"](req, res);
        if (!responseTime || isNaN(responseTime)) {
            return http(
                res.locals.reqid,
                message.trim(),
                req.idCIAM,
                req.body.idPersonne,
                -1
            );
        }
        return http(
            res.locals.reqid,
            message.trim(),
            req.idCIAM,
            req.body.idPersonne,
            parseFloat(responseTime)
        );
    },
    {
        immediate: false
    }
);

const startLogMorgan = morgan(
    function (tokens, req, res) {
        var message = [
            "REQUEST",
            tokens["remote-addr"](req, res),
            `"${tokens.method(req, res)}`,
            `${tokens.url(req, res)}"`,
            tokens["user-agent"](req, res)
        ].join(" ");
        return http(res.locals.reqid, message.trim());
    },
    {
        immediate: true
    }
);

module.exports = {
    critical: critical,
    error: error,
    warn: warn,
    info: info,
    http: http,
    debug: debug,
    startLogMorgan: startLogMorgan,
    endLogMorgan: endLogMorgan,
    requestId: requestId
};
