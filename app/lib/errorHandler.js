"use strict";

const { BaseError } = require("./errorTypes");
const logTools = require("./logTools");

/**
 * Cette classe permet une gestion d'erreur personnalisé pour les erreurs de type BaseError.
 * Il peut être utilisé comme middleware dans Express
 */
class ErrorHandler {
    /**
     *
     * @param {BaseError} err Erreur a traiter de type BaseError
     * @param {*} res la réponse qui sera retourné suite la l'appel
     */
    handleError(err, req, res) {
        try {
            if (err.getErrCode() === 500 || err.getErrCode() === 503) {
                logTools.critical(
                    res.locals.reqid,
                    `${err.getName()} httpstatus=${err.getErrCode()} - ${
                        err.message
                    }`,
                    req.idCIAM,
                    req.body.idPersonne,
                    err.stack
                );
            } else {
                logTools.error(
                    res.locals.reqid,
                    `${err.getName()} httpstatus=${err.getErrCode()} - ${
                        err.message
                    }`,
                    req.idCIAM,
                    req.body.idPersonne
                );
            }
            logTools.debug(
                res.locals.reqid,
                `${err.stack}`,
                req.idCIAM,
                req.body.idPersonne
            );
            var errResponse = {
                error: err.getName(),
                requestId: res.locals.reqid,
                status: err.getErrCode(),
                timestamp: new Date().toLocaleString().replace(",", ""),
                message: err.getHttpErrMsg()
            };
            if (res.locals.reqid !== "NO_REQ_ERR" && res.locals.reqid !== "0") {
                res.contentType("application/json");
                res.status(err.getErrCode());
                res.json(errResponse);
            }
            /*
             * Exemple d'action qui pourraient être lancé selon un code ou description spécifique dans l'erreur reçu.
             *
             * await sendMailToAdminIfCritical();
             * await sendEventsToSentry();
             *
             */
        } catch (err) {
            logTools.critical(
                res.locals.reqid,
                `ErrorHandler - ${err.message}`,
                req.idCIAM,
                req.body.idPersonne,
                err.stack
            );
            logTools.debug(
                res.locals.reqid,
                `${err.stack}`,
                req.idCIAM,
                req.body.idPersonne
            );
        }
    }

    /**
     *
     * Valide si l'argument err est de type BaseError
     * @param {BaseError} err
     */
    isTrustedError(err) {
        if (err instanceof BaseError) {
            return true;
        }
        return false;
    }
}
module.exports = ErrorHandler;
