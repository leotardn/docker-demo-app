const {
    CIAM,
    DonneesNominatives,
    EtudesAnterieures,
    FichiersProprietes,
    LastUpdatedFieldsDN,
    Personne
} = require("../models/profilapiModels");

const { In, MoreThanOrEqual, getRepository } = require("typeorm");
const config = require("config");

/**
 * Obtention des données d'une personne (données nominatives, études antérieures et fichiers de propriétés)
 * @param {*}  idPersonne l'id de la personne
 * @return {*} l'objet personne
 */
const getPersonne = async function (idPersonne) {
    try {
        const personneRepository = getRepository(Personne);
        let result = await personneRepository.find({
            where: {
                idPersonne: idPersonne
            },
            relations: [
                "donneesNominatives",
                "etudesAnterieures",
                "fichiersProprietes"
            ]
        });
        var personne = {};
        if (result.length > 0) {
            var etudesAnterieures = [];
            var i = 0;
            result[0].etudesAnterieures.forEach((element) => {
                etudesAnterieures[i++] = element.id;
            });

            var fichiersProprietes = [];
            i = 0;
            result[0].fichiersProprietes.forEach((element) => {
                fichiersProprietes[i++] = element.id;
            });

            personne = {
                idPersonne: result[0].idPersonne,
                etudesAnterieures: etudesAnterieures,
                fichiersProprietes: fichiersProprietes
            };
            return personne;
        }
        return undefined;
    } catch (err) {
        throw new Error(`Error getPersonne - ${err.message}`, err);
    }
};

/**
 * Obtention l'objet CIAM d'une personne
 * @param {*}  idCIAM l'id CIAM de la personne
 * @return {*} l'objet CIAM
 */
const getCIAM = async function (idCIAM) {
    try {
        const ciamRepository = getRepository(CIAM);
        let result = await ciamRepository.findOne({
            where: {
                idCIAM: idCIAM
            }
        });

        return result;
    } catch (err) {
        throw new Error(`Error getCIAM idCIAM = ${idCIAM} - ${err.message}`);
    }
};

/**
 * Obtention des données nominatives d'une personne
 * @param {*}  idPersonne l'id de la personne
 * @return {*} l'objet personne
 */
const getDonneesNominatives = async function (idPersonne) {
    try {
        const donneesNominativesRepository = getRepository(DonneesNominatives);
        let result = await donneesNominativesRepository.findOne({
            where: {
                idPersonne: idPersonne
            }
        });

        return result;
    } catch (err) {
        throw new Error(`Error getDonneesNominatives - ${err.message}`, err);
    }
};

/**
 * Obtention des données nominatives d'une personne avec la matricule fourni
 * @param {*}  matricule la matricule de la personne
 * @return {*} l'objet personne
 */
const getAdminDonneesNominatives = async function (matricule) {
    try {
        const donneesNominativesRepository = getRepository(DonneesNominatives);
        let result = await donneesNominativesRepository.findOne({
            where: {
                emplId: matricule
            }
        });

        return result;
    } catch (err) {
        throw new Error(
            `Error getAdminDonneesNominatives - ${err.message}`,
            err
        );
    }
};

/**
 * Obtention des études antérieures d'une personne
 * @param {*}  idPersonne l'id de la personne
 * @return {*} tableau avec tous les études antérieures trouvées
 */
const getEtudesAnterieures = async function (idPersonne) {
    try {
        const etudesAnterieuresRepo = getRepository(EtudesAnterieures);
        let result = await etudesAnterieuresRepo.find({
            where: {
                idPersonne: idPersonne
            }
        });

        return result;
    } catch (err) {
        throw new Error(`Error getEtudesAnterieures - ${err.message}`, err);
    }
};

/**
 * Obtention des études antérieures d'une personne
 * @param {*}  idEtudeAnterieure l'id de l'EtudeAnterieure
 * @return {*} tableau avec tous les études antérieures trouvées
 */
const getEtudeAnterieure = async function (idEtudeAnterieure) {
    try {
        const etudesAnterieuresRepo = getRepository(EtudesAnterieures);
        let result = await etudesAnterieuresRepo.findOne({
            where: {
                id: idEtudeAnterieure
            }
        });
        return result;
    } catch (err) {
        throw new Error(`Error getEtudeAnterieure - ${err.message}`, err);
    }
};

/**
 * Obtention du fichier de propriété d'une personne correspondant à l'id fourni
 * @param {*}  id l'id du fichiers de propriété
 * @param {*}  idPersonne l'id de la personne
 * @return {*} l'objet fichier de propriétés
 */
const getFichierProprietes = async function (id, idPersonne) {
    try {
        const fichiersProprietesRepository = getRepository(FichiersProprietes);
        let result = await fichiersProprietesRepository.findOne({
            where: {
                id: id,
                idPersonne: idPersonne
            }
        });

        return result;
    } catch (err) {
        throw new Error(`Error getFichierProprietes - ${err.message}`, err);
    }
};

/**
 * Obtention des fichiers de propriété d'une personne
 * @param {*}  idPersonne l'id de la personne
 * @return {*} les objets fichiers de propriétés
 */
const getFichiersProprietes = async function (idPersonne) {
    try {
        const fichiersProprietesRepository = getRepository(FichiersProprietes);
        let result = await fichiersProprietesRepository.find({
            where: {
                idPersonne: idPersonne
            }
        });
        return result;
    } catch (err) {
        throw new Error(`Error getFichiersProprietes - ${err.message}`, err);
    }
};

/**
 * Obtention des études antérieures à partir d'une date fournie
 * @param {*}  derniereDate la date limite
 * @return {*} tableau avec les études antérieures trouvées
 */
const getLastUpdatedEtudesAnterieures = async function (derniereDate) {
    try {
        const etudesAnterieuresRepository = getRepository(EtudesAnterieures);
        let result = await etudesAnterieuresRepository.find({
            where: {
                createdAt: MoreThanOrEqual(derniereDate)
            }
        });
        return result;
    } catch (err) {
        throw new Error(
            `Error getLastUpdatedEtudesAnterieures - ${err.message}`,
            err
        );
    }
};

/**
 * Obtention des données nominatives à partir d'une date fournie
 * @param {*}  derniereDate la date limite de type Date. Si derniereDate = "NOT_APPLICABLE", aucune condition liée à la date sera considérée (recherche totale)
 * @param {*}  searchParams json avec les parametres de recherche: "nom" ET "prenom" ou "courriel"
 * @return {*} tableau avec les données nominatives trouvées
 */
const getLastUpdatedDonneesNominatives = async function (
    derniereDate,
    searchParams
) {
    try {
        const donneesNominativesRepository = getRepository(DonneesNominatives);
        let result;
        if (!searchParams) {
            result = await donneesNominativesRepository.find({
                where: { updatedAt: MoreThanOrEqual(derniereDate) },
                select: [
                    "idPersonne",
                    "UMFirstName",
                    "UMLastName",
                    "emplId",
                    "emailAddr",
                    "levelOfStudy",
                    "fieldOfStudy"
                ],
                relations: ["lastUpdatedFieldsDN"]
            });
        } else if (searchParams) {
            if (searchParams.nom && searchParams.prenom) {
                if (derniereDate === config.constantes.NOT_APPLICABLE) {
                    result = await donneesNominativesRepository.find({
                        where: {
                            UMLastName: searchParams.nom,
                            UMFirstName: searchParams.prenom
                        },
                        select: [
                            "idPersonne",
                            "UMFirstName",
                            "UMLastName",
                            "emplId",
                            "emailAddr"
                        ]
                    });
                } else {
                    result = await donneesNominativesRepository.find({
                        where: {
                            updatedAt: MoreThanOrEqual(derniereDate),
                            UMLastName: searchParams.nom,
                            UMFirstName: searchParams.prenom
                        },
                        select: [
                            "idPersonne",
                            "UMFirstName",
                            "UMLastName",
                            "emplId",
                            "emailAddr"
                        ]
                    });
                }
            } else if (searchParams.courriel) {
                if (derniereDate === config.constantes.NOT_APPLICABLE) {
                    result = await donneesNominativesRepository.find({
                        where: {
                            emailAddr: searchParams.courriel
                        },
                        select: [
                            "idPersonne",
                            "UMFirstName",
                            "UMLastName",
                            "emplId",
                            "emailAddr"
                        ]
                    });
                } else {
                    result = await donneesNominativesRepository.find({
                        where: {
                            updatedAt: MoreThanOrEqual(derniereDate),
                            emailAddr: searchParams.courriel
                        },
                        select: [
                            "idPersonne",
                            "UMFirstName",
                            "UMLastName",
                            "emplId",
                            "emailAddr"
                        ]
                    });
                }
            } else if (searchParams.idPersonne) {
                if (derniereDate === config.constantes.NOT_APPLICABLE) {
                    result = await donneesNominativesRepository.find({
                        where: { idPersonne: In(searchParams.idPersonne) },
                        select: [
                            "idPersonne",
                            "UMFirstName",
                            "UMLastName",
                            "emplId",
                            "emailAddr"
                        ]
                    });
                } else {
                    result = await donneesNominativesRepository.find({
                        where: {
                            updatedAt: MoreThanOrEqual(derniereDate),
                            idPersonne: In(searchParams.idPersonne)
                        },
                        select: [
                            "idPersonne",
                            "UMFirstName",
                            "UMLastName",
                            "emplId",
                            "emailAddr"
                        ]
                    });
                }
            }
        }
        return result;
    } catch (err) {
        throw new Error(
            `Error getLastUpdatedDonneesNominatives - ${err.message}`,
            err
        );
    }
};

/**
 * Ajoute ou modifie une entrée existante dans la table Personne
 * @param {*}  jsonPersonne les données de la personne
 * @return {*} les données de la personne sauvegardées
 */
const savePersonne = async function (jsonPersonne) {
    try {
        const personneRepository = getRepository(Personne);
        var personne = personneRepository.create(jsonPersonne);
        let result = await personneRepository.save(personne);
        return result;
    } catch (err) {
        throw new Error(`Error savePersonne - ${err.message}`, err);
    }
};

/**
 * Ajoute ou modifie une entrée existante dans la table CIAM
 * @param {*}  idCIAM l'id CIAM de la personne
 * @return {*} l'objet CIAM
 */
const saveCIAM = async function (jsonCIAM) {
    try {
        const ciamRepository = getRepository(CIAM);
        var ciam = ciamRepository.create(jsonCIAM);
        let result = await ciamRepository.save(ciam);
        return result;
    } catch (err) {
        throw new Error(
            `Error saveCIAM idCIAM = ${JSON.stringify(jsonCIAM)} - ${
                err.message
            }`,
            err
        );
    }
};

/**
 * Ajoute ou modifie une entrée existante dans la table DonneesNominatives
 * @param {*}  jsonDonneesNominatives les données nominatives
 * @return {*} les données nominatives sauvegardées
 */
const saveDonneesNominatives = async function (jsonDonneesNominatives) {
    try {
        const donneesNominativesRepository = getRepository(DonneesNominatives);
        var donneesNominatives = donneesNominativesRepository.create(
            jsonDonneesNominatives
        );
        let result = await donneesNominativesRepository.save(
            donneesNominatives
        );
        return result;
    } catch (err) {
        throw new Error(`Error saveDonneesNominatives - ${err.message}`, err);
    }
};

/**
 * Keep track of last updated fields in DonneesNominatives table and their updated values
 * @param {*}  jsonLastUpdatedFieldsDN last updated fields in DonneesNominatives table and the new value to which the updated to
 * @return {*} saved LastUpdatedFieldsDN
 */
const saveLastUpdatedFieldsDN = async function (jsonLastUpdatedFieldsDN) {
    try {
        const lastUpdatedFieldsRepository = getRepository(LastUpdatedFieldsDN);
        var lastUpdatedFields = lastUpdatedFieldsRepository.create(
            jsonLastUpdatedFieldsDN
        );
        let result = await lastUpdatedFieldsRepository.save(lastUpdatedFields);
        return result;
    } catch (err) {
        throw new Error(`Error saveLastUpdatedFieldsDN - ${err.message}`, err);
    }
};

/**
 * Ajoute ou modifie une entrée existante dans la table FichiersProprietes
 * @param {*}  jsonFichiersProprietes le fichier des propriétés
 * @return {*} le fichier des propriétés sauvegardés
 */
const saveFichiersProprietes = async function (jsonFichiersProprietes) {
    try {
        const fichiersProprietesRepository = getRepository(FichiersProprietes);
        var fichiersProprietes = fichiersProprietesRepository.create(
            jsonFichiersProprietes
        );
        let result = await fichiersProprietesRepository.save(
            fichiersProprietes
        );
        return result;
    } catch (err) {
        throw new Error(`Error saveFichiersProprietes - ${err.message}`, err);
    }
};

/**
 * Modifie une entrée existante dans la table EtudesAnterieures
 * @param {*}  jsonEtudesAnterieures les études antérieures
 * @return {*} les études antérieures sauvegardées
 */
const saveEtudesAnterieures = async function (jsonEtudesAnterieures) {
    try {
        const etudesAnterieuresRepo = getRepository(EtudesAnterieures);
        var etudesAnterieures = etudesAnterieuresRepo.create(
            jsonEtudesAnterieures
        );

        let result = await etudesAnterieuresRepo.save(etudesAnterieures);
        return result;
    } catch (err) {
        throw new Error(`Error saveEtudesAnterieures - ${err.message}`, err);
    }
};

/**
 * Efface toutes les entrées liées à la personne dans toutes les tables profilapi de la BD
 * @param {*}  idPersonne l'id de la personne
 * @return {*} le nombre de rows affectés
 */
const deletePersonne = async function (idPersonne) {
    try {
        const personneRepository = getRepository(Personne);
        let result = await personneRepository.delete({
            idPersonne: idPersonne
        });
        // eslint-disable-next-line eqeqeq
        if (result && result.affected && result.affected != 0) {
            return result;
        }
        return undefined;
    } catch (err) {
        throw new Error(`deletePersonne - ${err.message}`, err);
    }
};

/**
 * Efface l'étude antérieure idéntifié liées à la personne
 * @param {*}  idEtudeAnterieure l'id de l'étude antérieure
 * @param {*}  idPersonne l'id de la personne
 * @return {*} le nombre de rows affectés
 */
const deleteEtudeAnterieure = async function (idEtudeAnterieure, idPersonne) {
    try {
        const etudesAnterieuresRepo = getRepository(EtudesAnterieures);

        let result = await etudesAnterieuresRepo.delete({
            id: idEtudeAnterieure,
            idPersonne: idPersonne
        });
        // eslint-disable-next-line eqeqeq
        if (result && result.affected && result.affected != 0) {
            return result;
        }
        return undefined;
    } catch (err) {
        throw new Error(`Error deleteEtudeAnterieure - ${err.message}`, err);
    }
};

/**
 * Efface les études antérieures liées à la personne
 * @param {*}  idPersonne l'id de la personne
 * @return {*} le nombre de rows affectés
 */
const deleteEtudesAnterieures = async function (idPersonne) {
    try {
        const etudesAnterieuresRepo = getRepository(EtudesAnterieures);

        let result = await etudesAnterieuresRepo.delete({
            idPersonne: idPersonne
        });
        // eslint-disable-next-line eqeqeq
        if (result && result.affected && result.affected != 0) {
            return result;
        }
        return undefined;
    } catch (err) {
        throw new Error(`Error deleteEtudesAnterieures - ${err.message}`, err);
    }
};

/**
 * Efface l'entrée dans la table FichiersProprietes liées l'idPersonne
 * @param {*}  idFichiersProprietes l'id du fichier des propriétés
 * @param {*}  idPersonne l'id de la personne
 * @return {*} le nombre de rows affectés
 */
/*   */
const deleteFichiersProprietes = async function (
    idFichiersProprietes,
    idPersonne
) {
    try {
        let fichiersProprietesRepository = getRepository(FichiersProprietes);
        let result = await fichiersProprietesRepository.delete({
            id: idFichiersProprietes,
            idPersonne: idPersonne
        });
        // eslint-disable-next-line eqeqeq
        if (result && result.affected && result.affected != 0) {
            return result;
        }
        return undefined;
    } catch (err) {
        throw new Error(`Error deleteFichiersProprietes - ${err.message}`, err);
    }
};

module.exports = {
    getPersonne: getPersonne,
    getCIAM: getCIAM,
    getDonneesNominatives: getDonneesNominatives,
    getAdminDonneesNominatives: getAdminDonneesNominatives,
    getEtudesAnterieures: getEtudesAnterieures,
    getEtudeAnterieure: getEtudeAnterieure,
    getFichiersProprietes: getFichiersProprietes,
    getFichierProprietes: getFichierProprietes,
    getLastUpdatedEtudesAnterieures: getLastUpdatedEtudesAnterieures,
    getLastUpdatedDonneesNominatives: getLastUpdatedDonneesNominatives,
    savePersonne: savePersonne,
    saveCIAM: saveCIAM,
    saveDonneesNominatives: saveDonneesNominatives,
    saveFichiersProprietes: saveFichiersProprietes,
    saveEtudesAnterieures: saveEtudesAnterieures,
    saveLastUpdatedFieldsDN: saveLastUpdatedFieldsDN,
    deletePersonne: deletePersonne,
    deleteEtudesAnterieures: deleteEtudesAnterieures,
    deleteEtudeAnterieure: deleteEtudeAnterieure,
    deleteFichiersProprietes: deleteFichiersProprietes
};
