/**
 * Constructor personalisé pour decrire les erreurs les plus communs
 */
class BaseError extends Error {
    /**
     *
     * @param {string} name nom de l'erreur
     * @param {*} errCode code decrivant l'erreur (example: code 500 du status Http 'Internal Server Error')
     * @param {string} description description claire de l'erreur
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(name, errCode, description, cause) {
        if (!cause) {
            super(description);
        } else {
            super(description, { cause });
        }
        Object.setPrototypeOf(this, new.target.prototype);

        this.name = name;
        this.errCode = errCode;
        this.description = description;
    }

    /**
     * Retourne le nom de l'erreur
     */
    getName() {
        return this.name;
    }
    /**
     * Retourne le code decrivant l'erreur (example: code 500 du status Http 'Internal Server Error')
     */
    getErrCode() {
        return this.errCode;
    }
    /**
     * Retourne la description claire de l'erreur
     */
    getDescription() {
        return this.description;
    }
    /**
     * Modifie la description claire de l'erreur
     */
    setDescription(description) {
        this.description = description;
    }
}

/**
 * Http Status codes ENUM
 */
const HttpStatusCode = Object.freeze({
    OK: 200,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    UNPROCESSABLE_ENTITY: 422,
    INTERNAL_SERVER: 500,
    SERVICE_UNAVAILABLE: 503
});

/**
 * Erreur http standard 400 'bad request'.
 * Cette réponse indique que le serveur n'a pas pu comprendre la requête à cause d'une syntaxe invalide.
 */
class HTTP400Error extends BaseError {
    /**
     *
     * @param {string} errDescription Description détaillé de l'erreur pour la journalisation.
     * @param {string} httpErrMsg Message a envoyer avec l'erreur http400 'Bad Request' - le serveur n'a pas pu comprendre la requête à cause d'une syntaxe invalide.
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(
        errDescription = "Le serveur n'a pas pu comprendre la requête à cause d'une syntaxe invalide",
        httpErrMsg = "Le serveur n'a pas pu comprendre la requête à cause d'une syntaxe invalide",
        cause
    ) {
        super("BAD REQUEST", HttpStatusCode.BAD_REQUEST, errDescription, cause);
        this.httpErrMsg = httpErrMsg;
    }

    getHttpErrMsg() {
        return this.httpErrMsg;
    }
}

/**
 * Erreur http standard 401 'Unauthorized'.
 * Cette réponse indique que une identification est nécessaire pour obtenir la réponse demandée.
 *
 * Detail en anglais: Although the HTTP standard specifies "unauthorized", semantically this response means "unauthenticated".
 */
class HTTP401Error extends BaseError {
    /**
     *
     * @param {string} errDescription Description détaillé de l'erreur pour la journalisation.
     * @param {string} httpErrMsg Message a envoyer avec l'erreur http401 'Unauthorized' - Identification du client necessaire.
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(
        errDescription = "Identification du client necessaire",
        httpErrMsg = "Identification du client necessaire",
        cause
    ) {
        super(
            "UNAUTHORIZED",
            HttpStatusCode.UNAUTHORIZED,
            errDescription,
            cause
        );
        this.httpErrMsg = httpErrMsg;
    }

    getHttpErrMsg() {
        return this.httpErrMsg;
    }
}

/**
 * Erreur http standard 403 'Forbidden'.
 * Cette réponse indique que le client n'a pas les droits d'accès au contenu.
 * Le serveur refuse de donner acces a la resource demandé.
 * En contraste avec l'erreur 401, ici l'identité du client est connue par le server.
 */
class HTTP403Error extends BaseError {
    /**
     *
     * @param {string} errDescription Description détaillé de l'erreur pour la journalisation.
     * @param {string} httpErrMsg Message a envoyer avec l'erreur http403 'Forbidden' - Droits d'accès refusés.
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(
        errDescription = "Droits d'accès refusés",
        httpErrMsg = "Droits d'accès refusés",
        cause
    ) {
        super("FORBIDDEN", HttpStatusCode.FORBIDDEN, errDescription, cause);
        this.httpErrMsg = httpErrMsg;
    }

    getHttpErrMsg() {
        return this.httpErrMsg;
    }
}

/**
 * Erreur http standard 404 'Not Found'.
 * Cette réponse indique que le serveur n'a pas trouvé la resource demandée.
 * Dans le browser, cela signifie que l'URL n'a pas pu etre reconnu.
 * Dans le context API, cela signifie que le endpoint est valide mais la resource elle meme n'existe pas.
 */
class HTTP404Error extends BaseError {
    /**
     *
     * @param {string} errDescription Description détaillé de l'erreur pour la journalisation.
     * @param {string} httpErrMsg Message a envoyer avec l'erreur http404 'Not Found' - le serveur n'a pas trouvé la resource demandée.
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(
        errDescription = "Le serveur n'a pas trouvé la resource demandée",
        httpErrMsg = "Le serveur n'a pas trouvé la resource demandée",
        cause
    ) {
        super("NOT FOUND", HttpStatusCode.NOT_FOUND, errDescription, cause);
        this.httpErrMsg = httpErrMsg;
    }

    getHttpErrMsg() {
        return this.httpErrMsg;
    }
}

/**
 * Erreur http standard 422 'Unprocessable Entity'.
 * Ce code de statut indique que le serveur a compris le type de contenu de la requête et que la syntaxe de la requête est correcte
 * mais que le serveur n'a pas été en mesure de réaliser les instructions demandées.
 */
class HTTP422Error extends BaseError {
    /**
     *
     * @param {string} errDescription Description détaillé de l'erreur pour la journalisation.
     * @param {string} httpErrMsg Message a envoyer avec l'erreur http422 'Unprocessable Entity' - Le serveur n'a pas été en mesure de réaliser les instructions demandées.
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(
        errDescription = "Le serveur n'a pas été en mesure de réaliser les instructions demandées",
        httpErrMsg = "Le serveur n'a pas été en mesure de réaliser les instructions demandées",
        cause
    ) {
        super(
            "UNPROCESSABLE ENTITY",
            HttpStatusCode.UNPROCESSABLE_ENTITY,
            errDescription,
            cause
        );
        this.httpErrMsg = httpErrMsg;
    }

    getHttpErrMsg() {
        return this.httpErrMsg;
    }
}

/**
 * Erreur http standard 500 'Internal Server'.
 * Cette réponse indique que le serveur a rencontré une situation qu'il ne sait pas traiter.
 */
class HTTP500Error extends BaseError {
    /**
     *
     * @param {string} errDescription Description détaillé de l'erreur pour la journalisation.
     * @param {string} httpErrMsg Message a envoyer avec l'erreur http500 'Internal Server' - Erreur interne du serveur.
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(
        errDescription = "Erreur interne du serveur",
        httpErrMsg = "Erreur interne du serveur",
        cause
    ) {
        super(
            "INTERNAL SERVER ERROR",
            HttpStatusCode.INTERNAL_SERVER,
            errDescription,
            cause
        );
        this.httpErrMsg = httpErrMsg;
    }

    getHttpErrMsg() {
        return this.httpErrMsg;
    }
}

/**
 * Erreur http standard 503 'Service Unavailable'.
 * Cette réponse indique que le serveur n'est pas prêt pour traiter la requête.
 * Les causes les plus communes sont que le serveur est indisponible pour maintenance ou qu'il est surchargé.
 */
class HTTP503Error extends BaseError {
    /**
     *
     * @param {string} errDescription Description détaillé de l'erreur pour la journalisation.
     * @param {string} httpErrMsg Message a envoyer avec l'erreur http503 'Service Unavailable' - Le serveur n'est pas prêt pour traiter la requête.
     * @param {string} cause l'erreur à l'origne de l'exception si present
     */
    constructor(
        errDescription = "Le serveur n'est pas prêt pour traiter la requête",
        httpErrMsg = "Le serveur n'est pas prêt pour traiter la requête",
        cause
    ) {
        super(
            "SERVICE UNAVAILABLE",
            HttpStatusCode.SERVICE_UNAVAILABLE,
            errDescription,
            cause
        );
        this.httpErrMsg = httpErrMsg;
    }

    getHttpErrMsg() {
        return this.httpErrMsg;
    }
}

module.exports = {
    BaseError,
    HTTP400Error,
    HTTP401Error,
    HTTP403Error,
    HTTP404Error,
    HTTP422Error,
    HTTP500Error,
    HTTP503Error
};
