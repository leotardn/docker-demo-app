"use strict";

const axios = require("axios");
const config = require("config");
const https = require("https");

/**
 * Fait appel au service SIS de Peoplesoft
 * @param {String} searchParam parametre pour réperer les données dans SIS
 * @param {String} uuid pour identifier la requête
 * @param {String} serviceCalled identifie le service à être appelé (données nominatives ou études antérieures)
 * @param {String} PSParam parametre à passer à PS pour réperer les données dans SIS (uniquement pour adminDonneesNominatives)
 * @returns Données extraits de Synchro
 */
const getSISData = async function getSISData(
    searchParam,
    uuid,
    serviceCalled,
    PSParam
) {
    const majDonneesNominatives = function (sisData) {
        sisData.UMStatAuCan = config.constantes.statAuCanCitoyenn;
        sisData.firstNationsMetisInuit = config.constantes.Oui;
        return sisData;
    };

    try {
        let endPoint;
        let url;
        if (
            serviceCalled ===
            config.peoplesoft.adminDonneesNominatives.serviceName
        ) {
            endPoint = config.peoplesoft.adminDonneesNominatives.endPoint;
            url = `${config.peoplesoft.url}/${endPoint}/${PSParam}`;
        } else {
            if (
                serviceCalled ===
                config.peoplesoft.donneesNominatives.serviceName
            ) {
                endPoint = config.peoplesoft.donneesNominatives.endPoint;
            }
            if (
                serviceCalled ===
                config.peoplesoft.etudesAnterieures.serviceName
            ) {
                endPoint = config.peoplesoft.etudesAnterieures.endPoint;
            }
            url = `${config.peoplesoft.url}/${endPoint}/${searchParam}?uuid=${uuid}`;
        }
        let axiosConfig = {};

        const agent = new https.Agent({
            rejectUnauthorized: false
        });

        axiosConfig = {
            method: "get",
            url: url,
            httpsAgent: agent,
            auth: {
                username: config.peoplesoft.username,
                password: config.peoplesoft.password
            }
        };
        const response = await axios(axiosConfig);
        if (
            serviceCalled === config.peoplesoft.donneesNominatives.serviceName
        ) {
            let data = response.data[0];
            if (
                data.UMStatAuCan ===
                config.constantes.ancienStatAuCAnFirstNations
            ) {
                data = majDonneesNominatives(data);
            }
            return data;
        }

        return response.data;
    } catch (err) {
        if (err.response && err.response.data) {
            let erreurCode;
            let erreurMessage;
            //Quand synchro tombe en panne durant l'appel et retourne une erreur 500 sans data[]
            // eslint-disable-next-line eqeqeq
            if (typeof err.response.data == "string") {
                throw new Error(
                    `Error getSISData - Synchro Error ${err.response.status}: ${err.response.data}`,
                    err
                );
            }
            // Erreur pour la requête des données nominatives
            if (
                serviceCalled ===
                config.peoplesoft.donneesNominatives.serviceName
            ) {
                erreurCode =
                    err.response.data[
                        config.peoplesoft.donneesNominatives.respErr
                    ].erreurs[0].code;
                erreurMessage =
                    err.response.data[
                        config.peoplesoft.donneesNominatives.respErr
                    ].erreurs[0].message;
            }
            // Erreur pour la requête admin des données nominatives
            if (
                serviceCalled ===
                config.peoplesoft.adminDonneesNominatives.serviceName
            ) {
                erreurCode =
                    err.response.data[
                        config.peoplesoft.adminDonneesNominatives.respErr
                    ].erreurs[0].code;
                erreurMessage =
                    err.response.data[
                        config.peoplesoft.adminDonneesNominatives.respErr
                    ].erreurs[0].message;
            }
            // Erreur pour la requête des études antérieures
            if (
                serviceCalled ===
                config.peoplesoft.etudesAnterieures.serviceName
            ) {
                erreurCode =
                    err.response.data[
                        config.peoplesoft.etudesAnterieures.respErr
                    ].erreurs[0].code;
                erreurMessage =
                    err.response.data[
                        config.peoplesoft.etudesAnterieures.respErr
                    ].erreurs[0].message;
            }
            let error = {
                code: erreurCode,
                message: erreurMessage
            };
            if (
                serviceCalled !==
                config.peoplesoft.adminDonneesNominatives.serviceName
            ) {
                searchParam = `matricule ${searchParam}`;
            }
            // eslint-disable-next-line eqeqeq
            if (error.code == 400) {
                /*
                 * Si aucune matricule n'est fourni dans l'appel à synchro on reçois un http400 mais on retourne à l'usager un 500
                 * car on a deja fait la validation du jeton et de la matricule plus haut et donc ca serait un bug de notre code
                 */
                throw new Error(
                    `Error getSISData - ${searchParam} httpErrStatus recu de PS: 400 (${serviceCalled}) - ${error.message}`,
                    err
                );
            }
            //Si la matricule fourni n'existe pas dans synchro
            // eslint-disable-next-line eqeqeq
            if (error.code == 404) {
                throw new Error(
                    `Error getSISData - ${searchParam} non trouvé (${serviceCalled})- ${error.message}`,
                    err
                );
            }
            throw new Error(
                `Error getSISData - Synchro Error code ${error.code}: ${error.message}`,
                err
            );
        }
        throw new Error(`Error getSISData - ${err.message}`, err);
    }
};

module.exports = {
    getSISData
};
