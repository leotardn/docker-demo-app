"use strict";

const config = require("config");
const db = require("./db.js");
const peoplesoft = require("./peoplesoft.js");
const { v4: uuidv4 } = require("uuid");

/**
 * Fusionne les données nominatives provenant de Synchro avec DonneesNominatives la plus récente presente dans la bd intermediaire (priorité synchro)
 * @param {*} idPersAGarder idPersonne de la personne à garder dans la bd intermediaire
 * @param {*} idPersADisparaitre idPersonne de la personne à disparaître dans la bd intermediaire
 * @param {*} matriculeAGarder matricule dans synchro de la personne à garder dans la bd intermediaire
 * @returns le nouveau object DonneesNominatives qui sera associé à idPersAGarder
 */
const donneesNominatives = async function (
    idPersAGarder,
    idPersADisparaitre,
    matriculeAGarder,
    reqId = uuidv4()
) {
    try {
        var persAGarderDN = await db.getDonneesNominatives(idPersAGarder);
        var persADisparaitreDN = await db.getDonneesNominatives(
            idPersADisparaitre
        );

        if (!persADisparaitreDN) {
            throw new Error(
                `Error fusion_donneesNominatives - idPersonne à disparaître ${idPersADisparaitre} non trouvé dans la BD`
            );
        } else if (!persAGarderDN) {
            throw new Error(
                `Error fusion_donneesNominatives - idPersonne à garder ${idPersAGarder} non trouvé dans la BD`
            );
        }
        var lastUpdatedAGarderDN = new Date(persAGarderDN.updatedAt);
        var lastUpdatedADisparaitreDN = new Date(persADisparaitreDN.updatedAt);
        //Verifie les données nominatives les plus recentes
        if (lastUpdatedAGarderDN < lastUpdatedADisparaitreDN) {
            //Sauvegarde les plus récentes DonneesNominatives dans profil avec l'idPersAGarder
            persADisparaitreDN.idPersonne = idPersAGarder;
            persADisparaitreDN.emplId = matriculeAGarder;
            db.saveDonneesNominatives(persADisparaitreDN);
        }

        var donneesNominatives = await updateDNFromSIS(
            idPersAGarder,
            matriculeAGarder,
            reqId
        );

        return donneesNominatives;
    } catch (err) {
        throw new Error(
            `Error fusion_donneesNominatives - ${err.message}`,
            err
        );
    }
};

/**
 * Fusionne les données nominatives provenant de Synchro avec ceux present dans la bd intermediaire (priorité synchro)
 * @param {*} idPersonne idPersonne de la personne dont les DonneesNominatives vont être mises à jours
 * @param {*} matricule matricule dans Synchro de la personne dont les DonneesNominatives vont être mises à jours
 * @param {*} uuidReq identifiant de la requête pour le journalisation
 * @returns le nouveau object DonneesNominatives
 */
const updateDNFromSIS = async function (idPersonne, matricule, uuidReq) {
    try {
        if (!idPersonne) {
            throw new Error("Error updateDNFromSIS - idPersonne absent");
        }
        if (!matricule) {
            throw new Error("Error updateDNFromSIS - matricule absent");
        }
        if (!uuidReq) {
            throw new Error("Error updateDNFromSIS - uuidReq absent");
        }
        const SISDonneesNominatives = await peoplesoft.getSISData(
            matricule,
            uuidReq,
            config.peoplesoft.donneesNominatives.serviceName
        );
        SISDonneesNominatives.idPersonne = idPersonne;
        SISDonneesNominatives.matricule = matricule;
        /**
         * Removes null value from objects in the array
         * @param {*} obj Array of objects from which null values will be removed
         * @returns Array of objects with removed null values
         */
        const nonNullValues = (obj) => {
            return Object.fromEntries(
                Object.entries(obj).filter(
                    // eslint-disable-next-line no-unused-vars
                    ([key, value]) => value !== null && value !== ""
                )
            );
        };
        const result = [SISDonneesNominatives].map(nonNullValues);
        db.saveDonneesNominatives(result[0]);
        return SISDonneesNominatives;
    } catch (err) {
        throw new Error(`Error updateDNFromSIS - ${err.message}`, err);
    }
};

/**
 * Fusionne les études antérieures de idPersAGarder et idPersADisparaitre, sauvegarde dans l'idPersAGarder
 * @param {*} idPersAGarder idPersonne de la personne à garder dans la bd intermediaire
 * @param {*} idPersADisparaitre idPersonne de la personne à disparaître dans la bd intermediaire
 * @returns liste des études antérieures retenu apres la fusion.
 */
const etudesAnterieures = async function (idPersAGarder, idPersADisparaitre) {
    try {
        var persAGarderEtudesAnt = await db.getEtudesAnterieures(idPersAGarder);
        var persADisparaitreEtudesAnt = await db.getEtudesAnterieures(
            idPersADisparaitre
        );
        /**
         * Regroupe les objets par proprieté
         * @param {*} arr array d'objet a regrouper selon la proprieté demandé
         * @param {*} property valeur utiliser pour regrouper les objets autour
         * @param {*} idPersAGarder idPersonne de la personne à garder dans la bd intermediaire
         * @returns un array avec plusieurs array chacun regroupé par leur valeur de property
         */
        const groupBy = (arr, property, idPersAGarder) => {
            return arr.reduce(function (accumulation, currentValue) {
                if (!accumulation[currentValue[property]]) {
                    accumulation[currentValue[property]] = [];
                }
                var listeEtudesAntDansUNEOrg =
                    accumulation[currentValue[property]];
                var existingEtudeAnt = false;
                for (let i = 0; i < listeEtudesAntDansUNEOrg.length; i++) {
                    /*
                     * Si le nom de l’institution fréquenté n'est pas dans la liste et
                     *  il est different, on ne compare pas l'etudesAnt
                     */
                    if (
                        currentValue.UMOthrInstFlag === config.constantes.Oui &&
                        currentValue.UMOthrInstDescr.toLowerCase() !==
                            listeEtudesAntDansUNEOrg[
                                i
                            ].UMOthrInstDescr.toLowerCase()
                    ) {
                        continue;
                    }
                    /*
                     * On essaie de vérifier si c'est le même par rapport au degree et
                     * à l'année du début de l'étude
                     */
                    if (
                        currentValue.degree &&
                        currentValue.degree ===
                            listeEtudesAntDansUNEOrg[i].degree &&
                        currentValue.yearFrom ===
                            listeEtudesAntDansUNEOrg[i].yearFrom
                    ) {
                        existingEtudeAnt = true;
                        break;
                    }
                    /*
                     * On essaie encore de vérifier si c'est le même pour un diplôme en cours.
                     */
                    if (
                        currentValue.degreeStatus ===
                        config.constantes.degreeStatusEnCours
                    ) {
                        if (
                            currentValue.yearFrom ===
                                listeEtudesAntDansUNEOrg[i].yearFrom &&
                            currentValue.UMAnnee ===
                                listeEtudesAntDansUNEOrg[i].UMAnnee &&
                            currentValue.UMMonthFrom ===
                                listeEtudesAntDansUNEOrg[i].UMMonthFrom &&
                            currentValue.UMMois ===
                                listeEtudesAntDansUNEOrg[i].UMMois
                        ) {
                            existingEtudeAnt = true;
                            break;
                        }
                    } else if (
                        // Dernière tentative d'établir que c'est la même étude.
                        currentValue.yearFrom ===
                            listeEtudesAntDansUNEOrg[i].yearFrom &&
                        currentValue.yearTo ===
                            listeEtudesAntDansUNEOrg[i].yearTo &&
                        currentValue.UMMonthFrom ===
                            listeEtudesAntDansUNEOrg[i].UMMonthFrom &&
                        currentValue.UMMonthTo ===
                            listeEtudesAntDansUNEOrg[i].UMMonthTo
                    ) {
                        existingEtudeAnt = true;
                        break;
                    }
                }

                if (existingEtudeAnt === false) {
                    currentValue.idPersonne = idPersAGarder;
                    accumulation[currentValue[property]].push(currentValue);
                }
                return accumulation;
            }, []);
        };

        let valeurs = Object.values(
            groupBy(
                persAGarderEtudesAnt.concat(persADisparaitreEtudesAnt),
                "extOrgId",
                idPersAGarder
            )
        );

        let etudesAntAGarder = Array();
        for (let i of valeurs) {
            var newArr = etudesAntAGarder.concat(i);
            etudesAntAGarder = newArr;
        }

        await db.deleteEtudesAnterieures(idPersAGarder);
        await db.saveEtudesAnterieures(etudesAntAGarder);
        return etudesAntAGarder;
    } catch (err) {
        throw new Error(`Error fusion_etudesAnterieures - ${err.message}`, err);
    }
};

module.exports = {
    updateDNFromSIS: updateDNFromSIS,
    donneesNominatives: donneesNominatives,
    etudesAnterieures: etudesAnterieures
};
