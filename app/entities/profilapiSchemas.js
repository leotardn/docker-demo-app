"use strict";

const {
    CIAM,
    DonneesNominatives,
    EtudesAnterieures,
    FichiersProprietes,
    LastUpdatedFieldsDN,
    Personne
} = require("../models/profilapiModels");
var EntitySchema = require("typeorm").EntitySchema;

exports.personne = new EntitySchema({
    name: "Personne",
    target: Personne,
    columns: {
        idPersonne: {
            primary: true,
            type: "uuid"
        },
        createdAt: {
            type: "timestamp",
            createDate: true,
            update: false
        }
    },
    relations: {
        ciam: {
            target: "CIAM",
            type: "one-to-many",
            joinTable: true,
            inverseSide: "personne",
            cascade: true
        },
        donneesNominatives: {
            target: "DonneesNominatives",
            type: "one-to-one",
            joinTable: true,
            inverseSide: "personne",
            cascade: true
        },
        etudesAnterieures: {
            target: "EtudesAnterieures",
            type: "one-to-many",
            joinTable: true,
            inverseSide: "personne",
            cascade: true
        },
        fichiersProprietes: {
            target: "FichiersProprietes",
            type: "one-to-many",
            joinTable: true,
            inverseSide: "personne",
            cascade: true
        }
    }
});

exports.ciam = new EntitySchema({
    name: "CIAM",
    target: CIAM,
    columns: {
        idCIAM: {
            primary: true,
            type: "varchar"
        },
        idPersonne: {
            type: "uuid",
            nullable: true
        },
        createdAt: {
            type: "timestamp",
            createDate: true,
            update: false
        },
        updatedAt: {
            type: "timestamp",
            updateDate: true
        }
    },
    relations: {
        personne: {
            target: "Personne",
            type: "many-to-one",
            joinColumn: {
                name: "idPersonne",
                referencedColumnName: "idPersonne"
            },
            joinTable: true,
            onDelete: "CASCADE"
        }
    }
});

exports.lastUpdatedFieldsDN = new EntitySchema({
    name: "LastUpdatedFieldsDN",
    target: LastUpdatedFieldsDN,
    columns: {
        idPersonne: {
            primary: true,
            type: "uuid"
        },
        fieldName: {
            primary: true,
            type: "varchar"
        },
        fieldValue: {
            nullable: true,
            type: "varchar"
        },
        fieldOldValue: {
            nullable: true,
            type: "varchar"
        },
        updatedAt: {
            type: "timestamp",
            updateDate: true
        }
    },
    relations: {
        donneesNominatives: {
            target: "DonneesNominatives",
            type: "many-to-one",
            joinColumn: {
                name: "idPersonne",
                referencedColumnName: "idPersonne"
            },
            joinTable: true,
            onDelete: "CASCADE"
        }
    }
});

exports.donneesNominatives = new EntitySchema({
    name: "DonneesNominatives",
    target: DonneesNominatives,
    columns: {
        idPersonne: {
            primary: true,
            type: "uuid"
        },
        frenchTestExempt: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        allowCommunications: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        levelOfStudy: {
            type: "varchar",
            length: "10",
            nullable: true
        },
        fieldOfStudy: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        latestDiploma: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        UMWappArmeesCdn: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        UMLastName: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        UMFirstName: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        UMLegalName: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        UMPreferredName: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        sex: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        birthday: {
            type: "varchar",
            length: "10",
            nullable: true
        },
        birthcountry: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        birthstate: {
            type: "varchar",
            length: "6",
            nullable: true
        },
        birthplace: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        languageCd: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        langCd: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        parentRole1: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        parentRole2: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        UMLastName1: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        UMFirstName1: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        UMLastName2: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        UMFirstName2: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        country: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        address1: {
            type: "varchar",
            length: "55",
            nullable: true
        },
        address2: {
            type: "varchar",
            length: "55",
            nullable: true
        },
        city: {
            type: "varchar",
            length: "30",
            nullable: true
        },
        state: {
            type: "varchar",
            length: "6",
            nullable: true
        },
        postal: {
            type: "varchar",
            length: "12",
            nullable: true
        },
        emailAddr: {
            type: "varchar",
            length: "70",
            nullable: true
        },
        phoneNumber1: {
            type: "varchar",
            length: "24",
            nullable: true
        },
        phoneType1: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        UMExtension1: {
            type: "varchar",
            length: "6",
            nullable: true
        },
        UMPhoneCountry1: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        phoneNumber2: {
            type: "varchar",
            length: "24",
            nullable: true
        },
        phoneType2: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        UMExtension2: {
            type: "varchar",
            length: "6",
            nullable: true
        },
        UMPhoneCountry2: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        phoneNumber3: {
            type: "varchar",
            length: "24",
            nullable: true
        },
        phoneType3: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        UMExtension3: {
            type: "varchar",
            length: "6",
            nullable: true
        },
        UMPhoneCountry3: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        UMPrefPhone: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        acceptPhoneText: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        UMStatAuCan: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        firstNationsMetisInuit: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        country2: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        UMFirstProv: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        UMSuppDoc1: {
            type: "varchar",
            length: "6",
            nullable: true
        },
        profileType: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        emplId: {
            type: "varchar",
            length: "16",
            nullable: true
        },
        campusId: {
            type: "varchar",
            length: "16",
            nullable: true
        },
        profLangCD: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        UMEduIntrptFlag: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        UMIntrDescr: {
            type: "varchar",
            length: "1000",
            nullable: true
        },
        createdAt: {
            type: "timestamp",
            createDate: true,
            update: false
        },
        updatedAt: {
            type: "timestamp",
            updateDate: true
        }
    },
    relations: {
        personne: {
            target: "Personne",
            type: "one-to-one",
            joinColumn: {
                name: "idPersonne",
                referencedColumnName: "idPersonne"
            },
            joinTable: true,
            onDelete: "CASCADE"
        },
        lastUpdatedFieldsDN: {
            target: "LastUpdatedFieldsDN",
            type: "one-to-many",
            joinTable: true,
            inverseSide: "donneesNominatives",
            cascade: true
        }
    }
});

exports.etudesAnterieures = new EntitySchema({
    name: "EtudesAnterieures",
    target: EtudesAnterieures,
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        yearFrom: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        UMMonthFrom: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        yearTo: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        UMMonthTo: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        ACADPlan: {
            type: "varchar",
            length: "10",
            nullable: true
        },
        ACADSubPlanModalite: {
            type: "varchar",
            length: "10",
            nullable: true
        },
        ACADSubPlanInstrument: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        ACADSubPlanBaccap: {
            type: "varchar",
            length: "10",
            nullable: true
        },
        ACADSubPlanOption: {
            type: "varchar",
            length: "10",
            nullable: true
        },
        ACADSubPlanHonor: {
            type: "varchar",
            length: "10",
            nullable: true
        },
        extOrgId: {
            type: "varchar",
            length: "11",
            nullable: true
        },
        UMOthrInstFlag: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        country: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        state: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        UMOthrInstDescr: {
            type: "varchar",
            nullable: true
        },
        UMOthrInstFlng: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        descr: {
            type: "varchar",
            length: "100",
            nullable: true
        },
        codeOfDegreeID: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        degree: {
            type: "varchar",
            length: "8",
            nullable: true
        },
        typeDiploma: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        specialisationPremiere1: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        specialisationPremiere2: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        specialisationPremiere3: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        specialisationTerminale1: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        specialisationTerminale2: {
            type: "varchar",
            length: "3",
            nullable: true
        },
        optionMath: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        BICoursesCompleted: {
            type: "varchar",
            length: "80",
            nullable: true
        },
        descr100: {
            type: "varchar",
            length: "100",
            nullable: true
        },
        degreeStatus: {
            type: "varchar",
            length: "1",
            nullable: true
        },
        progStatus: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        UMAnnee: {
            type: "varchar",
            length: "4",
            nullable: true
        },
        UMMois: {
            type: "varchar",
            length: "2",
            nullable: true
        },
        idPersonne: {
            type: "uuid",
            nullable: false
        },
        createdAt: {
            type: "timestamp",
            createDate: true,
            update: false
        },
        updatedAt: {
            type: "timestamp",
            updateDate: true
        }
    },
    relations: {
        personne: {
            target: "Personne",
            type: "many-to-one",
            joinColumn: {
                name: "idPersonne",
                referencedColumnName: "idPersonne"
            },
            joinTable: true,
            onDelete: "CASCADE"
        }
    }
});

exports.fichiersProprietes = new EntitySchema({
    name: "FichiersProprietes",
    target: FichiersProprietes,
    columns: {
        id: {
            primary: true,
            type: "varchar"
        },
        propFile: {
            type: "long",
            nullable: true
        },
        idPersonne: {
            primary: true,
            type: "uuid"
        },
        createdAt: {
            type: "timestamp",
            createDate: true,
            update: false
        },
        updatedAt: {
            type: "timestamp",
            updateDate: true
        }
    },
    relations: {
        personne: {
            target: "Personne",
            type: "many-to-one",
            joinColumn: {
                name: "idPersonne",
                referencedColumnName: "idPersonne"
            },
            joinTable: true,
            onDelete: "CASCADE"
        }
    }
});
