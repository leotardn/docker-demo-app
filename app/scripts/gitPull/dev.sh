ssh -A -o StrictHostKeyChecking=no -o LogLevel=Debug -l npmadm vantage-te-arch.dit.umontreal.ca << EOF
sudo systemctl stop node-profilapi@npmadm
cd profilapi
git checkout .
git pull https://$JenkinsCreds@gitlab.ti.umontreal.ca/apisynchro/profilapi.git develop
rm -r node_modules -f
npm install ignore-errors