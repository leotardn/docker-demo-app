ssh -A -o StrictHostKeyChecking=no -o LogLevel=Debug -l npmadm agnac-te-adm.dit.umontreal.ca << EOF
systemctl --user stop node-profilapi
cd profilapi
git checkout .
git pull https://$JenkinsCreds@gitlab.ti.umontreal.ca/apisynchro/profilapi.git test
rm -r node_modules -f
npm install ignore-errors