ssh -A -o StrictHostKeyChecking=no -o LogLevel=Debug -l npmadm agnac-te-adm.dit.umontreal.ca << EOF
    echo '{' > profilapi/config/test.json
    echo '  "typeORMConn": {' >> profilapi/config/test.json
    echo '      "type": "oracle",' >> profilapi/config/test.json
    echo '      "username": "$CredPimaDB_USR",' >> profilapi/config/test.json
    echo '      "password": "$CredPimaDB_PSW",' >> profilapi/config/test.json
    echo '      "connectString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=PDTIOR01-scan.dit.umontreal.ca)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=SRVESPIMA.UMONTREAL.CA)))",' >> profilapi/config/test.json						
    echo '      "synchronize": false,' >> profilapi/config/test.json
    echo '      "entities": [' >> profilapi/config/test.json
    echo '          "./entities/profilapiSchemas.js"' >> profilapi/config/test.json
    echo '      ],' >> profilapi/config/test.json
    echo '      "migrationsTableName": "migration_profilapi",' >> profilapi/config/test.json
    echo '      "migrations": ["./migration/*.js"],' >> profilapi/config/test.json
    echo '      "cli": {' >> profilapi/config/test.json
    echo '          "migrationsDir": "./migration"' >> profilapi/config/test.json
    echo '      }' >> profilapi/config/test.json
    echo '  },' >> profilapi/config/test.json
    echo '  "security": {' >> profilapi/config/test.json
    echo '      "jwksRsa": {' >> profilapi/config/test.json
    echo '          "cache": true,' >> profilapi/config/test.json
    echo '          "rateLimit": true,' >> profilapi/config/test.json
    echo '          "jwksRequestsPerMinute": 500,' >> profilapi/config/test.json
    echo '          "jwksUri": "https://communaute.authentification.devumontreal.ca/oauth2/default/v1/keys"' >> profilapi/config/test.json
    echo '      },' >> profilapi/config/test.json
    echo '      "jwksValidations": {' >> profilapi/config/test.json
    echo '          "issuer": "https://communaute.authentification.devumontreal.ca/oauth2/default",' >> profilapi/config/test.json
    echo '          "algorithms": ["RS256"],' >> profilapi/config/test.json
    echo '          "propertyName": "sub"' >> profilapi/config/test.json
    echo '      },' >> profilapi/config/test.json
    echo '      "proxy": "http://mandataire.ti.umontreal.ca:80",' >> profilapi/config/test.json
    echo '      "enableProxy": true,' >> profilapi/config/test.json
    echo '      "enableJWT": true' >> profilapi/config/test.json
    echo '  },' >> profilapi/config/test.json
    echo '  "smtp": {' >> profilapi/config/test.json
    echo '      "to": "servicesintegration_bdintermedaire@listes.umontreal.ca",' >> profilapi/config/test.json
    echo '      "from": "servicesintegration_bdintermedaire@listes.umontreal.ca",' >> profilapi/config/test.json
    echo '      "subjectErr": "Erreur ProfilAPI Test",' >> profilapi/config/test.json
    echo '      "subjectException": "Exception lancée dans ProfilAPI Test",' >> profilapi/config/test.json
    echo '      "host": "apps1.edge.umontreal.ca",' >> profilapi/config/test.json
    echo '      "port": 587,' >> profilapi/config/test.json
    echo '      "username": "$SMTP_USR",' >> profilapi/config/test.json
    echo '      "password": "$SMTP_PSW",' >> profilapi/config/test.json
    echo '      "level": "critical",' >> profilapi/config/test.json
    echo '      "silent": false,' >> profilapi/config/test.json
    echo '      "tls": true' >> profilapi/config/test.json
    echo '  },' >> profilapi/config/test.json
    echo '  "logstash": {' >> profilapi/config/test.json
    echo '      "mode": "tcp",' >> profilapi/config/test.json
    echo '      "host": "mazis-te-poc.dit.umontreal.ca",' >> profilapi/config/test.json
    echo '      "port": 5043,' >> profilapi/config/test.json
    echo '      "level": "http",' >> profilapi/config/test.json
    echo '      "silent": true,' >> profilapi/config/test.json
    echo '      "sslEnable": true,' >> profilapi/config/test.json
    echo '      "maxConnectRetries": 4,' >> profilapi/config/test.json
    echo '      "timeoutConnectRetries": 100' >> profilapi/config/test.json
    echo '  },' >> profilapi/config/test.json
    echo '  "logLevel": "debug",' >> profilapi/config/test.json
    echo '  "serviceCiamUrl": "http://localhost:3014",' >> profilapi/config/test.json
    echo '  "admissionApiUrl": "http://localhost:3012",' >> profilapi/config/test.json
    echo '  "peoplesoft": {' >> profilapi/config/test.json
    echo '      "url":"https://$ServeurSynchro.synchro.umontreal.ca",' >> profilapi/config/test.json
    echo '      "username":"$CredPS_USR",' >> profilapi/config/test.json
    echo '      "password":"$CredPS_PSW"' >> profilapi/config/test.json
    echo '  }' >> profilapi/config/test.json
    echo '}' >> profilapi/config/test.json
    systemctl --user start node-profilapi