ssh -A -o StrictHostKeyChecking=no -o LogLevel=Debug -l npmadm vantage-te-arch.dit.umontreal.ca << EOF
    echo '{' > profilapi/config/dev.json
    echo '  "typeORMConn": {' >> profilapi/config/dev.json
    echo '      "type": "oracle",' >> profilapi/config/dev.json
    echo '      "username": "$CredPimaDB_USR",' >> profilapi/config/dev.json
    echo '      "password": "$CredPimaDB_PSW",' >> profilapi/config/dev.json
    echo '      "connectString": "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=PDTIOR01-scan.dit.umontreal.ca)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=SRVDVPIMA.UMONTREAL.CA)))",' >> profilapi/config/dev.json
    echo '      "synchronize": false,' >> profilapi/config/dev.json
    echo '      "entities": [' >> profilapi/config/dev.json
    echo '          "./entities/profilapiSchemas.js"' >> profilapi/config/dev.json
    echo '      ],' >> profilapi/config/dev.json
    echo '      "migrationsTableName": "migration_profilapi",' >> profilapi/config/dev.json
    echo '      "migrations": ["./migration/*.js"],' >> profilapi/config/dev.json
    echo '      "cli": {' >> profilapi/config/dev.json
    echo '          "migrationsDir": "./migration"' >> profilapi/config/dev.json
    echo '      }' >> profilapi/config/dev.json
    echo '  },' >> profilapi/config/dev.json
    echo '  "security": {' >> profilapi/config/dev.json
    echo '      "jwksRsa": {' >> profilapi/config/dev.json
    echo '          "cache": true,' >> profilapi/config/dev.json
    echo '          "rateLimit": true,' >> profilapi/config/dev.json
    echo '          "jwksRequestsPerMinute": 500,' >> profilapi/config/dev.json
    echo '          "jwksUri": "https://communaute.authentification.devumontreal.ca/oauth2/default/v1/keys"' >> profilapi/config/dev.json
    echo '      },' >> profilapi/config/dev.json
    echo '      "jwksValidations": {' >> profilapi/config/dev.json
    echo '          "issuer": "https://communaute.authentification.devumontreal.ca/oauth2/default",' >> profilapi/config/dev.json
    echo '          "algorithms": ["RS256"],' >> profilapi/config/dev.json
    echo '          "propertyName": "sub"' >> profilapi/config/dev.json
    echo '      },' >> profilapi/config/dev.json
    echo '      "proxy": "http://mandataire.ti.umontreal.ca:80",' >> profilapi/config/dev.json
    echo '      "enableProxy":true,' >> profilapi/config/dev.json
    echo '      "enableJWT":true' >> profilapi/config/dev.json 
    echo '  },' >> profilapi/config/dev.json
    echo '  "smtp": {' >> profilapi/config/dev.json
    echo '      "to": ["leotard.niyonkuru@umontreal.ca","eliana.coelho@umontreal.ca","mohamed.mezzoug@umontreal.ca"],' >> profilapi/config/dev.json
    echo '      "from": "leotard.niyonkuru@umontreal.ca",' >> profilapi/config/dev.json
    echo '      "subjectErr": "Erreur ProfilAPI Dev",' >> profilapi/config/dev.json
    echo '      "subjectException": "Exception lancée dans ProfilAPI Dev",' >> profilapi/config/dev.json
    echo '      "host": "apps1.edge.umontreal.ca",' >> profilapi/config/dev.json
    echo '      "port": 587,' >> profilapi/config/dev.json
    echo '      "username": "$SMTP_USR",' >> profilapi/config/dev.json
    echo '      "password": "$SMTP_PSW",' >> profilapi/config/dev.json
    echo '      "level": "critical",' >> profilapi/config/dev.json
    echo '      "silent": true,' >> profilapi/config/dev.json
    echo '      "tls": true' >> profilapi/config/dev.json
    echo '  },' >> profilapi/config/dev.json
    echo '  "logstash": {' >> profilapi/config/dev.json
    echo '      "mode": "tcp",' >> profilapi/config/dev.json
    echo '      "host": "mazis-te-poc.dit.umontreal.ca",' >> profilapi/config/dev.json
    echo '      "port": 5043,' >> profilapi/config/dev.json
    echo '      "level": "http",' >> profilapi/config/dev.json
    echo '      "silent": true,' >> profilapi/config/dev.json
    echo '      "sslEnable": true,' >> profilapi/config/dev.json
    echo '      "maxConnectRetries": 4,' >> profilapi/config/dev.json
    echo '      "timeoutConnectRetries": 100' >> profilapi/config/dev.json
    echo '  },' >> profilapi/config/dev.json
    echo '  "logLevel": "debug",' >> profilapi/config/dev.json
    echo '  "serviceCiamUrl": "http://localhost:3014",' >> profilapi/config/dev.json
    echo '  "admissionApiUrl": "http://localhost:3012",' >> profilapi/config/dev.json
    echo '  "peoplesoft": {' >> profilapi/config/dev.json
    echo '      "url":"https://$ServeurSynchro.synchro.umontreal.ca",' >> profilapi/config/dev.json
    echo '      "username":"$CredPS_USR",' >> profilapi/config/dev.json
    echo '      "password":"$CredPS_PSW"' >> profilapi/config/dev.json
    echo '  }' >> profilapi/config/dev.json
    echo '}' >> profilapi/config/dev.json
    sudo systemctl start node-profilapi@npmadm 
