"use strict";

const Chance = require("chance");

const { assert } = require("chai");
const assertNode = require("assert");
const config = require("config");
const proxyquire = require("proxyquire");
const sinon = require("sinon");

const chance = new Chance();

describe("Tests unitaires de peoplesoft ", function () {
    describe("getSISData ", function () {
        let matricule;
        let uuidReq;

        describe("Données nominatives ", function () {
            before(function () {
                matricule = config.test.matriculeExistant;
                uuidReq = chance.guid();
            });
            afterEach(function () {
                sinon.restore();
            });

            it("Donnees nominatives trouvées", async function () {
                // Arrange
                let dnExpected = {
                    emplId: "00405",
                    campusId: "",
                    frenchTestExempt: "",
                    allowCommunications: "",
                    levelOfStudy: "",
                    fieldOfStudy: "",
                    latestDiploma: "",
                    UMWappArmeesCdn: "",
                    UMLastName: "Levasseur",
                    UMFirstName: "Denis",
                    UMLegalName: "",
                    UMPreferredName: "",
                    sex: "M",
                    birthday: "1956-02-24",
                    birthcountry: "",
                    birthplace: "",
                    languageCd: "FR",
                    langCd: "",
                    parentRole1: "FA",
                    parentRole2: "M",
                    UMLastName1: "Inconnu",
                    UMFirstName1: "Inconnu",
                    UMLastName2: "Inconnu",
                    UMFirstName2: "Inconnu",
                    country: "CAN",
                    address1: "91-64E AVENUE",
                    address2: "",
                    city: "ST-EUSTACHE",
                    state: "QC",
                    postal: "J7P3P4",
                    emailAddr: "",
                    phoneNumber1: "",
                    phoneType1: "",
                    UMExtension1: "",
                    UMPhoneCountry1: "",
                    phoneNumber2: "",
                    phoneType2: "",
                    UMExtension2: "",
                    UMPhoneCountry2: "",
                    phoneNumber3: "450-4736473",
                    phoneType3: "PRIM",
                    UMExtension3: "",
                    UMPhoneCountry3: "",
                    UMPrefPhone: "PRIM",
                    acceptPhoneText: "",
                    UMStatAuCan: "CD",
                    firstNationsMetisInuit: "N",
                    country2: "CAN",
                    UMFirstProv: "",
                    UMSuppDoc1: "",
                    profileType: "",
                    profLangCD: "",
                    UMEduIntrptFlag: "",
                    UMIntrDescr: ""
                };
                const axiosStub = sinon.stub().resolves({ data: [dnExpected] });
                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });
                // Act
                const sISDonneesNominatives = await peoplesoft.getSISData(
                    matricule,
                    uuidReq,
                    config.peoplesoft.donneesNominatives.serviceName
                );

                //Assert
                assert.deepEqual(
                    sISDonneesNominatives,
                    dnExpected,
                    "Réponse incorrect de SIS"
                );
            });

            it("Donnees nominatives trouvées avec UMStatAuCan = IN", async function () {
                // Arrange
                let dnFromSynchro = {
                    emplId: "00405",
                    campusId: "",
                    frenchTestExempt: "",
                    allowCommunications: "",
                    levelOfStudy: "",
                    fieldOfStudy: "",
                    latestDiploma: "",
                    UMWappArmeesCdn: "",
                    UMLastName: "Levasseur",
                    UMFirstName: "Denis",
                    UMLegalName: "",
                    UMPreferredName: "",
                    sex: "M",
                    birthday: "1956-02-24",
                    birthcountry: "",
                    birthplace: "",
                    languageCd: "FR",
                    langCd: "",
                    parentRole1: "FA",
                    parentRole2: "M",
                    UMLastName1: "Inconnu",
                    UMFirstName1: "Inconnu",
                    UMLastName2: "Inconnu",
                    UMFirstName2: "Inconnu",
                    country: "CAN",
                    address1: "91-64E AVENUE",
                    address2: "",
                    city: "ST-EUSTACHE",
                    state: "QC",
                    postal: "J7P3P4",
                    emailAddr: "",
                    phoneNumber1: "",
                    phoneType1: "",
                    UMExtension1: "",
                    UMPhoneCountry1: "",
                    phoneNumber2: "",
                    phoneType2: "",
                    UMExtension2: "",
                    UMPhoneCountry2: "",
                    phoneNumber3: "450-4736473",
                    phoneType3: "PRIM",
                    UMExtension3: "",
                    UMPhoneCountry3: "",
                    UMPrefPhone: "PRIM",
                    acceptPhoneText: "",
                    UMStatAuCan: "IN",
                    firstNationsMetisInuit: "N",
                    country2: "CAN",
                    UMFirstProv: "",
                    UMSuppDoc1: "",
                    profileType: "",
                    profLangCD: "",
                    UMEduIntrptFlag: "",
                    UMIntrDescr: ""
                };
                let dnExpected = {
                    emplId: "00405",
                    campusId: "",
                    frenchTestExempt: "",
                    allowCommunications: "",
                    levelOfStudy: "",
                    fieldOfStudy: "",
                    latestDiploma: "",
                    UMWappArmeesCdn: "",
                    UMLastName: "Levasseur",
                    UMFirstName: "Denis",
                    UMLegalName: "",
                    UMPreferredName: "",
                    sex: "M",
                    birthday: "1956-02-24",
                    birthcountry: "",
                    birthplace: "",
                    languageCd: "FR",
                    langCd: "",
                    parentRole1: "FA",
                    parentRole2: "M",
                    UMLastName1: "Inconnu",
                    UMFirstName1: "Inconnu",
                    UMLastName2: "Inconnu",
                    UMFirstName2: "Inconnu",
                    country: "CAN",
                    address1: "91-64E AVENUE",
                    address2: "",
                    city: "ST-EUSTACHE",
                    state: "QC",
                    postal: "J7P3P4",
                    emailAddr: "",
                    phoneNumber1: "",
                    phoneType1: "",
                    UMExtension1: "",
                    UMPhoneCountry1: "",
                    phoneNumber2: "",
                    phoneType2: "",
                    UMExtension2: "",
                    UMPhoneCountry2: "",
                    phoneNumber3: "450-4736473",
                    phoneType3: "PRIM",
                    UMExtension3: "",
                    UMPhoneCountry3: "",
                    UMPrefPhone: "PRIM",
                    acceptPhoneText: "",
                    UMStatAuCan: "CD",
                    firstNationsMetisInuit: "O",
                    country2: "CAN",
                    UMFirstProv: "",
                    UMSuppDoc1: "",
                    profileType: "",
                    profLangCD: "",
                    UMEduIntrptFlag: "",
                    UMIntrDescr: ""
                };
                const axiosStub = sinon
                    .stub()
                    .resolves({ data: [dnFromSynchro] });
                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });
                // Act
                const sISDonneesNominatives = await peoplesoft.getSISData(
                    matricule,
                    uuidReq,
                    config.peoplesoft.donneesNominatives.serviceName
                );

                //Assert
                assert.deepEqual(
                    sISDonneesNominatives,
                    dnExpected,
                    "Réponse incorrect de SIS"
                );
            });

            it("Error getting donnees nominatives - pas de response", async function () {
                const axiosStub = sinon
                    .stub()
                    .throws({ code: "ECONRESET", message: "Error ECONRESET" });

                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });

                const testFunction = async () => {
                    // Act
                    await peoplesoft.getSISData(
                        matricule,
                        uuidReq,
                        config.peoplesoft.donneesNominatives.serviceName
                    );
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(error.message, /^Error getSISData /);
                    return true;
                });
            });

            it("Error getting donnees nominatives - reponse avec erreur 404", async function () {
                let responseErreur = {
                    response: {
                        status: 404,
                        data: {
                            UM_WS_JSON_SEC_DONN_NOMIN: {
                                erreurs: [
                                    {
                                        code: 404,
                                        message:
                                            "Le matricule  0040 n'existe pas dans Synchro"
                                    }
                                ],
                                uuid: "bbe1fc8a-3756-5a87-8d0d-191f646006ef"
                            }
                        }
                    }
                };
                const axiosStub = sinon.stub().throws(responseErreur);

                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });

                const testFunction = async () => {
                    // Act
                    await peoplesoft.getSISData(
                        matricule,
                        uuidReq,
                        config.peoplesoft.donneesNominatives.serviceName
                    );
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(error.message, /Error getSISData/);
                    assert.match(error.message, /non trouvé/);
                    return true;
                });
            });

            it("Error getting donnees nominatives - reponse avec erreur 400 avec erreur 400", async function () {
                let responseErreur = {
                    response: {
                        status: 400,
                        data: {
                            UM_WS_JSON_SEC_DONN_NOMIN: {
                                erreurs: [
                                    {
                                        code: 400,
                                        message: "Erreur"
                                    }
                                ],
                                uuid: "bbe1fc8a-3756-5a87-8d0d-191f646006ef"
                            }
                        }
                    }
                };
                const axiosStub = sinon.stub().throws(responseErreur);

                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });

                const testFunction = async () => {
                    // Act
                    await peoplesoft.getSISData(
                        matricule,
                        uuidReq,
                        config.peoplesoft.donneesNominatives.serviceName
                    );
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(error.message, /^Error getSISData/);
                    assert.match(
                        error.message,
                        /httpErrStatus recu de PS: 400/
                    );
                    return true;
                });
            });
        });

        describe("Études antérieures ", function () {
            before(function () {
                matricule = config.test.matriculeExistant;
                uuidReq = chance.guid();
            });
            afterEach(function () {
                sinon.restore();
            });

            it("Études antérieures trouvées", async function () {
                // Arrange
                let eaExpected = [
                    {
                        yearFrom: "1981",
                        UMMonthFrom: "09",
                        yearTo: "1989",
                        UMMonthTo: "08",
                        ACADSubPlanModalite: "",
                        ACADSubPlanInstrument: "",
                        ACADSubPlanBaccap: "",
                        ACADSubPlanOption: "",
                        ACADSubPlanHonor: "",
                        extOrgId: "5000671",
                        UMOthrInstFlag: "N",
                        Descr: "Doctorat",
                        Descr100: "3-220-1-0 Psychologie",
                        ProgStatus: "CM",
                        country: "CAN",
                        state: "QC",
                        UMOthrInstDescr: "",
                        UMOthrInstFlng: "",
                        codeOfDegreeID: "",
                        degree: "PHD",
                        typeDiploma: "",
                        specialisationPremiere1: "",
                        specialisationPremiere2: "",
                        specialisationPremiere3: "",
                        specialisationTerminale1: "",
                        specialisationTerminale2: "",
                        optionMath: "",
                        BICoursesCompleted: "",
                        degreeStatus: "C",
                        UMAnnee: "",
                        UMMois: "",
                        idMsg: "b098913b-eec8-52b0-8574-76763fb1ac05"
                    },
                    {
                        yearFrom: "1979",
                        UMMonthFrom: "09",
                        yearTo: "1980",
                        UMMonthTo: "08",
                        ACADSubPlanModalite: "",
                        ACADSubPlanInstrument: "",
                        ACADSubPlanBaccap: "",
                        ACADSubPlanOption: "",
                        ACADSubPlanHonor: "",
                        extOrgId: "5000671",
                        UMOthrInstFlag: "N",
                        Descr: "Maîtrise",
                        Descr100: "2-220-1-3 Psychologie",
                        ProgStatus: "CM",
                        country: "CAN",
                        state: "QC",
                        UMOthrInstDescr: "",
                        UMOthrInstFlng: "",
                        codeOfDegreeID: "",
                        degree: "MSC",
                        typeDiploma: "",
                        specialisationPremiere1: "",
                        specialisationPremiere2: "",
                        specialisationPremiere3: "",
                        specialisationTerminale1: "",
                        specialisationTerminale2: "",
                        optionMath: "",
                        BICoursesCompleted: "",
                        degreeStatus: "C",
                        UMAnnee: "",
                        UMMois: "",
                        idMsg: "b098913b-eec8-52b0-8574-76763fb1ac05"
                    },
                    {
                        yearFrom: "1976",
                        UMMonthFrom: "09",
                        yearTo: "1979",
                        UMMonthTo: "04",
                        ACADSubPlanModalite: "",
                        ACADSubPlanInstrument: "",
                        ACADSubPlanBaccap: "",
                        ACADSubPlanOption: "",
                        ACADSubPlanHonor: "",
                        extOrgId: "5000671",
                        UMOthrInstFlag: "N",
                        Descr: "Baccalauréat",
                        Descr100: "1-220-1-0 Psychologie",
                        ProgStatus: "CM",
                        country: "CAN",
                        state: "QC",
                        UMOthrInstDescr: "",
                        UMOthrInstFlng: "",
                        codeOfDegreeID: "",
                        degree: "BSC",
                        typeDiploma: "",
                        specialisationPremiere1: "",
                        specialisationPremiere2: "",
                        specialisationPremiere3: "",
                        specialisationTerminale1: "",
                        specialisationTerminale2: "",
                        optionMath: "",
                        BICoursesCompleted: "",
                        degreeStatus: "C",
                        UMAnnee: "",
                        UMMois: "",
                        idMsg: "b098913b-eec8-52b0-8574-76763fb1ac05"
                    }
                ];
                const axiosStub = sinon.stub().resolves({ data: eaExpected });
                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });
                // Act
                const sISEtudesAnterieures = await peoplesoft.getSISData(
                    matricule,
                    uuidReq,
                    config.peoplesoft.etudesAnterieures.serviceName
                );

                //Assert
                assert.deepEqual(
                    sISEtudesAnterieures,
                    eaExpected,
                    "Réponse incorrect de SIS"
                );
            });

            it("Error getting etudes Anterieures - pas de response", async function () {
                const axiosStub = sinon
                    .stub()
                    .throws({ code: "ECONRESET", message: "Error ECONRESET" });

                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });

                const testFunction = async () => {
                    // Act
                    await peoplesoft.getSISData(
                        matricule,
                        uuidReq,
                        config.peoplesoft.etudesAnterieures.serviceName
                    );
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(error.message, /^Error getSISData /);
                    return true;
                });
            });

            it("Error getting etudesAnterieures - reponse avec erreur 404", async function () {
                let responseErreur = {
                    response: {
                        status: 404,
                        data: {
                            UM_WS_JSON_SEC_ETUD_ANT: {
                                erreurs: [
                                    {
                                        code: 404,
                                        message:
                                            "Le matricule  0040 n'existe pas dans Synchro"
                                    }
                                ],
                                uuid: "bbe1fc8a-3756-5a87-8d0d-191f646006ef"
                            }
                        }
                    }
                };
                const axiosStub = sinon.stub().throws(responseErreur);

                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });

                const testFunction = async () => {
                    // Act
                    await peoplesoft.getSISData(
                        matricule,
                        uuidReq,
                        config.peoplesoft.etudesAnterieures.serviceName
                    );
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(error.message, /Error getSISData/);
                    assert.match(error.message, /non trouvé/);
                    return true;
                });
            });

            it("Error getting etudesAnterieures - reponse avec erreur 400 avec erreur 400", async function () {
                let responseErreur = {
                    response: {
                        status: 400,
                        data: {
                            UM_WS_JSON_SEC_ETUD_ANT: {
                                erreurs: [
                                    {
                                        code: 400,
                                        message: "Erreur"
                                    }
                                ],
                                uuid: "bbe1fc8a-3756-5a87-8d0d-191f646006ef"
                            }
                        }
                    }
                };
                const axiosStub = sinon.stub().throws(responseErreur);

                const peoplesoft = proxyquire("../../lib/peoplesoft", {
                    axios: axiosStub
                });

                const testFunction = async () => {
                    // Act
                    await peoplesoft.getSISData(
                        matricule,
                        uuidReq,
                        config.peoplesoft.etudesAnterieures.serviceName
                    );
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(error.message, /^Error getSISData/);
                    assert.match(
                        error.message,
                        /httpErrStatus recu de PS: 400/
                    );
                    return true;
                });
            });
        });
    });
});
