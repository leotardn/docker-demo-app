"use strict";

const Chance = require("chance");

const { assert } = require("chai");
const assertNode = require("assert");
const proxyquire = require("proxyquire");
const sinon = require("sinon");

const chance = new Chance();
describe("Tests unitaires de ciam ", function () {
    describe("fonction getMatricule ", function () {
        afterEach(function () {
            sinon.restore();
        });

        it("Matricule trouvé", async function () {
            // Arrange
            let matriculeExpected = "00405";
            const axiosStub = sinon
                .stub()
                .resolves({ data: { udem_matricule: "00405" } });
            const ciam = proxyquire("../../lib/ciam", {
                axios: axiosStub
            });
            let idCIAM = chance.email();
            // Act
            const matricule = await ciam.getMatricule(idCIAM);

            //Assert
            assert.deepEqual(
                matricule,
                matriculeExpected,
                "Réponse incorrect de ciamapi"
            );
        });

        it("Matricule vide", async function () {
            // Arrange
            let matriculeExpected = undefined;
            const axiosStub = sinon
                .stub()
                .resolves({ data: { udem_matricule: "" } });
            const ciam = proxyquire("../../lib/ciam", {
                axios: axiosStub
            });
            let idCIAM = chance.email();
            // Act
            const matricule = await ciam.getMatricule(idCIAM);

            //Assert
            assert.deepEqual(
                matricule,
                matriculeExpected,
                "Réponse incorrect de ciamapi"
            );
        });

        it("Matricule non trouvé", async function () {
            // Arrange
            let matriculeExpected = undefined;
            const axiosStub = sinon
                .stub()

                .throws({ response: { status: 404 } });

            const ciam = proxyquire("../../lib/ciam", {
                axios: axiosStub
            });
            let idCIAM = chance.email();
            // Act
            const matricule = await ciam.getMatricule(idCIAM);

            //Assert
            assert.deepEqual(
                matricule,
                matriculeExpected,
                "Réponse incorrect de ciamapi"
            );
        });

        it("erreur ciamapi", async function () {
            // Arrange
            const axiosStub = sinon
                .stub()
                .throws({ response: { status: 500, message: "Server error" } });

            const ciam = proxyquire("../../lib/ciam", {
                axios: axiosStub
            });
            let idCIAM = chance.email();

            const testFunction = async () => {
                // Act
                await ciam.getMatricule(idCIAM);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getMatricule -/);
                return true;
            });
        });
    });
});
