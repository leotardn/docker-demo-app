"use strict";

const {
    donneesNominativesDev,
    etudesAnterieuresDev,
    fichierProprietes
} = require("../profilMockGenerator");

const Chance = require("chance");

const { assert } = require("chai");
const assertNode = require("assert");
const proxyquire = require("proxyquire");
const sinon = require("sinon");

const chance = new Chance();

describe("Tests de db", function () {
    afterEach(function () {
        sinon.restore();
    });

    describe("SAVE PERSONNE", function () {
        it("Save personne avec succès", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let personne = {
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            let createdAt = "22-02-07 14:40:17,896646000";
            let personneExpected = {
                idPersonne: idPersonne,
                createdAt: createdAt
            };
            repertoireObject.create = function () {
                return personne;
            };
            repertoireObject.save = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(personneExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const resul = await db.savePersonne(personne);

            // Assert
            assert.equal(repositoryStub.callCount, 1);
            assert.deepEqual(resul, personneExpected);
        });

        it("Error saving personne", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let personne = {
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            repertoireObject.create = function () {
                return personne;
            };
            repertoireObject.save = function () {
                throw new Error("Error saving personne");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.savePersonne(personne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error savePersonne/);
                return true;
            });
        });
    });

    describe("DELETE PERSONNE", function () {
        it("Delete personne avec succès", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let repertoireObject = {};
            let resultExpected = {
                affected: 1
            };
            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.deletePersonne(idPersonne);

            // Assert
            assert.deepEqual(result, resultExpected);
        });
        it("Delete personne non trouvée", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let repertoireObject = {};
            let resultExpected = {
                affected: 0
            };
            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.deletePersonne(idPersonne);

            // Assert
            assert.deepEqual(result, undefined);
        });
        it("Delete personne avec erreur db", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let repertoireObject = {};
            repertoireObject.delete = function () {
                throw new Error("Error deleting");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.deletePersonne(idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^deletePersonne/);
                return true;
            });
        });
    });

    describe("SAVE CIAM", function () {
        it("Save CIAM with succes", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let idCIAM = "test@test.com";
            let ciamExpected = [
                {
                    idCIAM: idCIAM,
                    idPersonne: idPersonne
                }
            ];
            let repertoireObject = {};
            repertoireObject.create = function () {
                return ciamExpected;
            };
            repertoireObject.save = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(ciamExpected);
                    }, 10);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.saveCIAM(idPersonne);

            // Assert
            assert.deepEqual(result, ciamExpected);
        });

        it("Erreur db", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let idCIAM = "test@test.com";
            let ciamExpected = {
                idCIAM: idCIAM,
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            repertoireObject.create = function () {
                return ciamExpected;
            };
            repertoireObject.save = function () {
                throw new Error("Erreur DB");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.saveCIAM(ciamExpected);
            };

            // Assert
            await assertNode.rejects(testFunction, {
                name: "Error",
                message: `Error saveCIAM idCIAM = ${JSON.stringify(
                    ciamExpected
                )} - Erreur DB`
            });

            assert.equal(repositoryStub.callCount, 1);
        });
    });

    describe("SAVE DONNEES NOMINATIVES", function () {
        it("Save données nominatives avec succès", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let donneesNominatives = {
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            let donneeNominativeExpected = {
                idPersonne: idPersonne
            };
            repertoireObject.create = function () {
                return donneesNominatives;
            };
            repertoireObject.save = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(donneeNominativeExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // // Act
            const result = await db.saveDonneesNominatives(donneesNominatives);

            // Assert
            assert.deepEqual(result, donneeNominativeExpected);
        });

        it("Error saving données nominatives", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let donneesNominatives = {
                idPersonne: idPersonne
            };
            let repertoireObject = {};

            repertoireObject.create = function () {
                return donneesNominatives;
            };

            repertoireObject.save = function () {
                throw new Error("Error saving ");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.saveDonneesNominatives(donneesNominatives);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error saveDonneesNominatives/);
                return true;
            });
        });
    });

    describe("SAVE LAST UPDATED FIELDS DN", function () {
        it("Save last updated fields DN avec succès", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let lastUpdatedFieldsDN = {
                idPersonne: idPersonne,
                fieldName: "AllowCommunication",
                fieldValue: "true",
                fieldOldValue: "false"
            };
            let repertoireObject = {};
            let lastUpdatedFieldsDNExpected = {
                dPersonne: idPersonne,
                fieldName: "AllowCommunication",
                fieldValue: "true",
                fieldOldValue: "false"
            };
            repertoireObject.create = function () {
                return lastUpdatedFieldsDN;
            };
            repertoireObject.save = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(lastUpdatedFieldsDNExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // // Act
            const result = await db.saveLastUpdatedFieldsDN(
                lastUpdatedFieldsDN
            );

            // Assert
            assert.deepEqual(result, lastUpdatedFieldsDNExpected);
        });

        it("Error saving données nominatives", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let lastUpdatedFieldsDN = {
                dPersonne: idPersonne,
                fieldName: "AllowCommunication",
                fieldValue: "true",
                fieldOldValue: "false"
            };
            let repertoireObject = {};

            repertoireObject.create = function () {
                return lastUpdatedFieldsDN;
            };

            repertoireObject.save = function () {
                throw new Error("Error saving ");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.saveLastUpdatedFieldsDN(lastUpdatedFieldsDN);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error saveLastUpdatedFieldsDN/);
                return true;
            });
        });
    });

    describe("SAVE ETUDES ANTERIEURES", function () {
        it("Save études antérieures avec succès", async function () {
            let id = chance.integer({ min: 1, max: 100 });
            let idPersonne = chance.guid({ version: 4 });
            let etudesAnterieures = {
                id: id,
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            let etudesAnterieuresExpected = {
                id: id,
                idPersonne: idPersonne
            };
            repertoireObject.create = function () {
                return etudesAnterieures;
            };
            repertoireObject.save = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(etudesAnterieuresExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // // Act
            const result = await db.saveEtudesAnterieures(etudesAnterieures);

            // Assert
            assert.deepEqual(result, etudesAnterieuresExpected);
        });

        it("Error saving études antérieures", async function () {
            let id = chance.integer({ min: 1, max: 100 });
            let idPersonne = chance.guid({ version: 4 });
            let etudesAnterieures = {
                id: id,
                idPersonne: idPersonne
            };
            let repertoireObject = {};

            repertoireObject.create = function () {
                return etudesAnterieures;
            };

            repertoireObject.save = function () {
                throw new Error("Error saving ");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.saveEtudesAnterieures(etudesAnterieures);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error saveEtudesAnterieures/);
                return true;
            });
        });
    });

    describe("DELETE ETUDES ANTERIEURES", function () {
        it("delete études antérieures avec succès", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let repertoireObject = {};
            let resultExpected = {
                affected: 1
            };
            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            let result = await db.deleteEtudesAnterieures(idPersonne);

            // Assert
            assert.equal(repositoryStub.callCount, 1);
            assert.deepEqual(result, resultExpected);
        });

        it("delete étud antérieur non trouvé", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let resultExpected = {
                affected: 0
            };
            let repertoireObject = {};

            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            let result = await db.deleteEtudesAnterieures(idPersonne);

            // Assert
            assert.equal(repositoryStub.callCount, 1);
            assert.deepEqual(result, undefined);
        });

        it("Erreur delete étude antérieure ", async function () {
            let idPersonne = chance.guid({ version: 4 });

            let repertoireObject = {};
            repertoireObject.delete = function () {
                throw new Error("Error deleting");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.deleteEtudesAnterieures(idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error deleteEtudesAnterieures/);
                return true;
            });
        });
    });

    describe("DELETE ETUDE ANTERIEURE", function () {
        it("delete études antérieures avec succès", async function () {
            let id = chance.natural();
            let repertoireObject = {};
            let resultExpected = {
                affected: 1
            };
            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            let result = await db.deleteEtudeAnterieure(id);

            // Assert
            assert.equal(repositoryStub.callCount, 1);
            assert.deepEqual(result, resultExpected);
        });

        it("delete étud antérieur non trouvé", async function () {
            let id = chance.natural();
            let resultExpected = {
                affected: 0
            };
            let repertoireObject = {};

            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            let result = await db.deleteEtudeAnterieure(id);

            // Assert
            assert.equal(repositoryStub.callCount, 1);
            assert.deepEqual(result, undefined);
        });

        it("Erreur delete étude antérieure ", async function () {
            let id = chance.natural();

            let repertoireObject = {};
            repertoireObject.delete = function () {
                throw new Error("Error deleting");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.deleteEtudeAnterieure(id);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error deleteEtudeAnterieure/);
                return true;
            });
        });
    });

    describe("SAVE FICHIERS PROPRIETES", function () {
        it("Save fichiers propriétes avec succès", async function () {
            let id = chance.integer({ min: 1, max: 100 });
            let idPersonne = chance.guid({ version: 4 });
            let fichiersProprietes = {
                id: id,
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            let fichiersProprietesExpected = {
                id: id,
                idPersonne: idPersonne
            };
            repertoireObject.create = function () {
                return fichiersProprietes;
            };
            repertoireObject.save = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(fichiersProprietesExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // // Act
            const result = await db.saveFichiersProprietes(fichiersProprietes);

            // Assert
            assert.deepEqual(result, fichiersProprietesExpected);
        });

        it("Error saving fichiers propriétes", async function () {
            let id = chance.integer({ min: 1, max: 100 });
            let idPersonne = chance.guid({ version: 4 });
            let fichiersProprietes = {
                id: id,
                idPersonne: idPersonne
            };
            let repertoireObject = {};

            repertoireObject.create = function () {
                return fichiersProprietes;
            };

            repertoireObject.save = function () {
                throw new Error("Error saving ");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.saveFichiersProprietes(fichiersProprietes);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error saveFichiersProprietes/);
                return true;
            });
        });
    });

    describe("DELETE FICHIERS PROPRIETES", function () {
        it("Delete fichiers propriétes avec succès", async function () {
            let id = chance.integer({ min: 1, max: 100 });
            let idPersonne = chance.guid({ version: 4 });

            let repertoireObject = {};
            let resultExpected = {
                affected: 1
            };

            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            let result = await db.deleteFichiersProprietes(id, idPersonne);

            // Assert
            assert.deepEqual(result, resultExpected);
        });

        it("delete fichier propriété non trouvé", async function () {
            let id = chance.natural();
            let idPersonne = chance.guid({ version: 4 });
            let resultExpected = {
                affected: 0
            };
            let repertoireObject = {};

            repertoireObject.delete = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            let result = await db.deleteFichiersProprietes(id, idPersonne);

            // Assert
            assert.equal(repositoryStub.callCount, 1);
            assert.deepEqual(result, undefined);
        });

        it("Erreur delete  ", async function () {
            let id = chance.integer({ min: 1, max: 100 });
            let idPersonne = chance.guid({ version: 4 });

            let repertoireObject = {};
            repertoireObject.delete = function () {
                throw new Error("Error deleting");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.deleteFichiersProprietes(id, idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error deleteFichiersProprietes/);
                return true;
            });
        });
    });

    describe("GET PERSONNE", function () {
        it("Get existing personne with succes - tous les données vides", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let tableauPersonneExpected = [
                {
                    idPersonne: idPersonne,
                    etudesAnterieures: [],
                    fichiersProprietes: []
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(tableauPersonneExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getPersonne(idPersonne);

            //Assert
            assert.deepEqual(result, tableauPersonneExpected[0]);
        });

        it("Get existing personne with succes - données nominatives, études antérieures, fichiers de propriétés", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let dn = donneesNominativesDev.get("MatFictif");
            let ea = etudesAnterieuresDev.get("MatFictif");
            ea.id = chance.natural();
            let fp = fichierProprietes;
            fp.id = chance.natural();
            let resultDB = [
                {
                    idPersonne: idPersonne,
                    donneesNominatives: dn,
                    etudesAnterieures: [ea],
                    fichiersProprietes: [fp]
                }
            ];
            let tableauPersonneExpected = [
                {
                    idPersonne: idPersonne,
                    etudesAnterieures: [ea.id],
                    fichiersProprietes: [fp.id]
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultDB);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getPersonne(idPersonne);

            //Assert
            assert.deepEqual(result, tableauPersonneExpected[0]);
        });

        it("Personne non trouvée dans la bd", async function () {
            let idPersonne = chance.guid({ version: 4 });

            let resultDB = [];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(resultDB);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getPersonne(idPersonne);

            //Assert
            assert.deepEqual(result, undefined);
        });

        it("Error getting personne", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let repertoireObject = {};
            repertoireObject.find = function () {
                throw new Error("Error getting ...");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getPersonne(idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getPersonne/);
                return true;
            });
        });
    });

    describe("GET CIAM", function () {
        it("Get existing CIAM with succes", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let idCIAM = "test@test.com";
            let ciamExpected = [
                {
                    idCIAM: idCIAM,
                    idPersonne: idPersonne
                }
            ];
            let repertoireObject = {};
            repertoireObject.findOne = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(ciamExpected);
                    }, 10);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getCIAM(idPersonne);

            // Assert
            assert.deepEqual(result, ciamExpected);
            assert.equal(repositoryStub.callCount, 1);
        });

        it("Erreur db", async function () {
            let idCIAM = "test@test.com";

            let repertoireObject = {};
            repertoireObject.findOne = function () {
                throw new Error("Erreur DB");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getCIAM(idCIAM);
            };

            // Assert
            await assertNode.rejects(testFunction, {
                name: "Error",
                message: `Error getCIAM idCIAM = ${idCIAM} - Erreur DB`
            });

            assert.equal(repositoryStub.callCount, 1);
        });
    });

    describe("GET DONNÉES NOMINATIVES ", function () {
        it("Get données nominatives for personne existing with succes", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let tableauDonneesNominExpected = {
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            repertoireObject.findOne = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(tableauDonneesNominExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getDonneesNominatives(idPersonne);

            //Assert
            assert.deepEqual(result, tableauDonneesNominExpected);
            assert.equal(repositoryStub.callCount, 1);
        });

        it("Error getting donneesnominatives", async function () {
            let idPersonne = chance.guid({ version: 4 });

            let repertoireObject = {};
            repertoireObject.findOne = function () {
                throw new Error("Error getting donneesnominatives");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getDonneesNominatives(idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getDonneesNominatives/);
                return true;
            });
        });

        it("Get admin données nominatives for matricule existing with succes", async function () {
            let matricule = "123456789";
            let tableauDonneesNominExpected = {
                matricule: matricule
            };
            let repertoireObject = {};
            repertoireObject.findOne = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(tableauDonneesNominExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getDonneesNominatives(matricule);

            //Assert
            assert.deepEqual(result, tableauDonneesNominExpected);
            assert.equal(repositoryStub.callCount, 1);
        });

        it("Error getting admin donneesnominatives for matricule", async function () {
            let matricule = "123456789";

            let repertoireObject = {};
            repertoireObject.findOne = function () {
                throw new Error("Error getting admin donneesnominatives");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getAdminDonneesNominatives(matricule);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(
                    error.message,
                    /^Error getAdminDonneesNominatives/
                );
                return true;
            });
        });

        it("Get last updated Données nominatives, without searchParams", async function () {
            let today = new Date().toISOString();
            let idPersonne = chance.guid({ version: 4 });
            let lastDonneeNominExpected = [
                {
                    idPersonne: idPersonne,
                    lastUpdatedFieldsDN: [
                        {
                            idPersonne: idPersonne,
                            fieldName: "AllowCommunication",
                            fieldValue: "true",
                            fieldOldValue: "false"
                        }
                    ]
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(lastDonneeNominExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getLastUpdatedDonneesNominatives(today);

            //Assert
            assert.deepEqual(result, lastDonneeNominExpected);
        });

        it("Get last updated Données nominatives, Nom&Prenom searchParams ", async function () {
            let today = new Date().toISOString();
            let idPersonne = chance.guid({ version: 4 });
            let lastDonneeNominExpected = [
                {
                    idPersonne: idPersonne
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(lastDonneeNominExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getLastUpdatedDonneesNominatives(today, {
                nom: "NOM",
                prenom: "PRENOM"
            });

            //Assert
            assert.deepEqual(result, lastDonneeNominExpected);
        });

        it("Get last updated Données nominatives, @courriel searchParams ", async function () {
            let today = new Date().toISOString();
            let idPersonne = chance.guid({ version: 4 });
            let lastDonneeNominExpected = [
                {
                    idPersonne: idPersonne
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(lastDonneeNominExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getLastUpdatedDonneesNominatives(today, {
                courriel: "nom.prenom@umontreal.ca"
            });

            //Assert
            assert.deepEqual(result, lastDonneeNominExpected);
        });

        it("Error getLastUpdatedDonneesNominatives", async function () {
            let today = new Date().toISOString();
            let repertoireObject = {};
            repertoireObject.find = function () {
                throw new Error("Error getLastUpdatedDonneesNominatives");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getLastUpdatedDonneesNominatives(today);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(
                    error.message,
                    /^Error getLastUpdatedDonneesNominatives/
                );
                return true;
            });
        });
    });

    describe("GET ÉTUDES ANTÉRIEURES ", function () {
        it("Get études antérieures for personne existing with succes", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let tableauEtudesAnterExpected = [
                {
                    idPersonne: idPersonne
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(tableauEtudesAnterExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getEtudesAnterieures(idPersonne);

            //Assert
            assert.deepEqual(result, tableauEtudesAnterExpected);
        });

        it("Error getting etudes anterieures", async function () {
            let idPersonne = chance.guid({ version: 4 });

            let repertoireObject = {};

            repertoireObject.find = function () {
                throw new Error(`Error getting etudes anterieures`);
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getEtudesAnterieures(idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getEtudesAnterieures/);
                return true;
            });
        });

        it("Get last updated Etudes antérieures", async function () {
            let today = new Date().toISOString();
            let idPersonne = chance.guid({ version: 4 });
            let lastEtudesAnterExpected = [
                {
                    idPersonne: idPersonne
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(lastEtudesAnterExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getLastUpdatedEtudesAnterieures(today);

            //Assert
            assert.deepEqual(result, lastEtudesAnterExpected);
        });

        it("Error getLastUpdatedEtudesAnterieures", async function () {
            let today = new Date().toISOString();

            let repertoireObject = {};
            repertoireObject.find = function () {
                throw new Error("Error getLastUpdatedEtudesAnterieures");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getLastUpdatedEtudesAnterieures(today);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(
                    error.message,
                    /^Error getLastUpdatedEtudesAnterieures/
                );
                return true;
            });
        });
    });

    describe("GET ÉTUDE ANTÉRIEURe ", function () {
        it("Get étude antérieure for personne existing with succes", async function () {
            let id = chance.natural();
            let etudeAnterExpected = etudesAnterieuresDev.get("MatFictif");
            let repertoireObject = {};
            repertoireObject.findOne = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(etudeAnterExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getEtudeAnterieure(id);

            //Assert
            assert.deepEqual(result, etudeAnterExpected);
        });

        it("Error getting etude anterieure", async function () {
            let id = chance.natural();

            let repertoireObject = {};

            repertoireObject.findOne = function () {
                throw new Error(`Error getting etude anterieure`);
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getEtudeAnterieure(id);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getEtudeAnterieure/);
                return true;
            });
        });
    });

    describe("GET FICHIERS PROPRIÉTES ", function () {
        it("Get fichiers propriétés for personne existing with succes", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let tableauFichierProprExpected = [
                {
                    idPersonne: idPersonne
                }
            ];

            let repertoireObject = {};
            repertoireObject.find = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(tableauFichierProprExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getFichiersProprietes(idPersonne);

            //Assert
            assert.deepEqual(result, tableauFichierProprExpected);
        });

        it("Error fichiers propriétés", async function () {
            let idPersonne = chance.guid({ version: 4 });
            let repertoireObject = {};
            repertoireObject.find = function () {
                throw new Error("Error getting ...");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getFichiersProprietes(idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getFichiersProprietes/);
                return true;
            });
        });
    });

    describe("GET FICHIER PROPRIÉTES ", function () {
        it("Get fichier propriétés for personne existing with succes", async function () {
            let id = chance.string({ length: 3 });
            let idPersonne = chance.guid({ version: 4 });
            let fichierProprExpected = {
                idPersonne: idPersonne
            };
            let repertoireObject = {};
            repertoireObject.findOne = function () {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        resolve(fichierProprExpected);
                    }, 30);
                });
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            // Act
            const result = await db.getFichierProprietes(id, idPersonne);

            //Assert
            assert.deepEqual(result, fichierProprExpected);
        });

        it("Error fichier propriétés", async function () {
            let id = chance.string({ length: 3 });
            let idPersonne = chance.guid({ version: 4 });
            let repertoireObject = {};
            repertoireObject.findOne = function () {
                throw new Error("Error getting ...");
            };

            let repositoryStub = sinon.stub().returns(repertoireObject);
            const typeormStub = {
                getRepository: repositoryStub
            };
            const db = proxyquire("../../lib/db", {
                typeorm: typeormStub
            });

            const testFunction = async () => {
                // Act
                await db.getFichierProprietes(id, idPersonne);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(repositoryStub.callCount, 1);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getFichierProprietes/);
                return true;
            });
        });
    });
});
