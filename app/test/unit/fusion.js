"use strict";

const Chance = require("chance");

const { assert } = require("chai");
const assertNode = require("assert");
const config = require("config");
const db = require("../../lib/db");
const peoplesoft = require("../../lib/peoplesoft.js");
const proxyquire = require("proxyquire");
const sinon = require("sinon");

const chance = new Chance();

describe("Tests de fusion", function () {
    describe("DONNÉES NOMINATIVES ", function () {
        let matricule;
        let idPersAGarder;
        let idPersADisparaitre;
        let idPersAGarderDN;
        let idPersADisparaitreDN;
        before(function () {
            matricule = config.test.matriculeExistant;
            idPersAGarder = chance.guid({ version: 4 });
            idPersADisparaitre = chance.guid({ version: 4 });
            let today = new Date();
            let hier = new Date();
            hier.setDate(today.getDate() - 1);
            idPersAGarderDN = {
                idPersonne: idPersAGarder,
                createdAt: hier,
                updatedAt: hier
            };
            idPersADisparaitreDN = {
                idPersonne: idPersADisparaitre,
                createdAt: today,
                updatedAt: today
            };
        });
        afterEach(function () {
            db.getDonneesNominatives.restore();
        });
        it("Fusion données Nominative d'idPersonne different (le plus recent est assigné idPersAGarder)", async function () {
            let dbStub = sinon.stub(db, "getDonneesNominatives");
            dbStub.onCall(0).resolves(idPersAGarderDN);
            dbStub.onCall(1).resolves(idPersADisparaitreDN);

            let dbStub2 = sinon.stub(db, "saveDonneesNominatives");
            dbStub2.resolves();

            let SISStub = sinon.stub(peoplesoft, "getSISData");
            SISStub.resolves(idPersADisparaitreDN);

            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            // Act
            const result = await fusion.donneesNominatives(
                idPersAGarder,
                idPersADisparaitre,
                matricule
            );

            //Assert
            assert.deepEqual(result, idPersADisparaitreDN);
            db.saveDonneesNominatives.restore();
            peoplesoft.getSISData.restore();
        });

        it("Fusion données Nominative - personne à garder non trouvée", async function () {
            let dbStub = sinon.stub(db, "getDonneesNominatives");
            dbStub.onCall(0).resolves(undefined);
            dbStub.onCall(1).resolves(idPersADisparaitreDN);

            let dbStub2 = sinon.stub(db, "saveDonneesNominatives");
            dbStub2.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            const testFunction = async () => {
                // Act
                await fusion.donneesNominatives(
                    idPersAGarder,
                    idPersADisparaitre,
                    matricule
                );
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(dbStub.callCount, 2);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error fusion_donneesNominatives/);
                assert.match(error.message, /non trouvé/);
                assert.match(error.message, /idPersonne à garder/);
                return true;
            });

            db.saveDonneesNominatives.restore();
        });

        it("Fusion données Nominative - personne à disparaître non trouvée", async function () {
            let dbStub = sinon.stub(db, "getDonneesNominatives");
            dbStub.onCall(0).resolves(idPersAGarder);
            dbStub.onCall(1).resolves(undefined);

            let dbStub2 = sinon.stub(db, "saveDonneesNominatives");
            dbStub2.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            const testFunction = async () => {
                // Act
                await fusion.donneesNominatives(
                    idPersAGarder,
                    idPersADisparaitre,
                    matricule
                );
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(dbStub.callCount, 2);
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error fusion_donneesNominatives/);
                assert.match(error.message, /non trouvé/);
                assert.match(error.message, /idPersonne à disparaître/);
                return true;
            });

            db.saveDonneesNominatives.restore();
        });

        it("Error getting donneesnominatives from database", async function () {
            sinon
                .stub(db, "getDonneesNominatives")
                .throws({ code: "ECONRESET", message: "Error ECONRESET" });

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            const testFunction = async () => {
                // Act
                await fusion.donneesNominatives(
                    idPersAGarder,
                    idPersADisparaitre,
                    matricule
                );
            };

            // Assert
            await assertNode.rejects(testFunction, {
                name: "Error",
                message: "Error fusion_donneesNominatives - Error ECONRESET"
            });
        });
    });

    describe("ETUDES ANTERIEURES", function () {
        let idPersAGarder;
        let idPersADisparaitre;
        let idPersAGarderEtAnt;
        let idPersADisparaitreEtAnt;

        before(function () {
            idPersAGarder = chance.guid({ version: 4 });
            idPersADisparaitre = chance.guid({ version: 4 });
        });
        afterEach(function () {
            db.getEtudesAnterieures.restore();
            db.deleteEtudesAnterieures.restore();
            db.saveEtudesAnterieures.restore();
        });
        it("Fusion études antérieures UMOthrInstFlag:'O'", async function () {
            idPersAGarderEtAnt = [
                {
                    yearFrom: "1990",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Different",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1990",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Leo",
                    idPersonne: idPersAGarder
                }
            ];
            idPersADisparaitreEtAnt = [
                {
                    yearFrom: "1990",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Leo",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1990",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Different",
                    idPersonne: idPersAGarder
                }
            ];

            let dbStub = sinon.stub(db, "getEtudesAnterieures");
            dbStub.onCall(0).resolves(idPersAGarderEtAnt);
            dbStub.onCall(1).resolves(idPersADisparaitreEtAnt);

            let dbStub2 = sinon.stub(db, "saveEtudesAnterieures");
            dbStub2.resolves();

            let dbStub3 = sinon.stub(db, "deleteEtudesAnterieures");
            dbStub3.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            // Act
            const result = await fusion.etudesAnterieures(
                idPersAGarder,
                idPersADisparaitre
            );

            //Assert
            assert.strictEqual(result.length, 2);
            assert.notEqual(
                result[0].UMOthrInstDescr,
                result[1].UMOthrInstDescr
            );
        });

        it("Fusion études antérieures yearFrom NOT EQUAL, UMOthrInstFlag:'O'", async function () {
            idPersAGarderEtAnt = [
                {
                    yearFrom: "1990",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Different",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1999",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Leo",
                    idPersonne: idPersAGarder
                }
            ];
            idPersADisparaitreEtAnt = [
                {
                    yearFrom: "1990",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Leo",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1990",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "",
                    UMOthrInstFlag: "O",
                    UMOthrInstDescr: "Different",
                    idPersonne: idPersAGarder
                }
            ];

            let dbStub = sinon.stub(db, "getEtudesAnterieures");
            dbStub.onCall(0).resolves(idPersAGarderEtAnt);
            dbStub.onCall(1).resolves(idPersADisparaitreEtAnt);

            let dbStub2 = sinon.stub(db, "saveEtudesAnterieures");
            dbStub2.resolves();

            let dbStub3 = sinon.stub(db, "deleteEtudesAnterieures");
            dbStub3.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            // Act
            const result = await fusion.etudesAnterieures(
                idPersAGarder,
                idPersADisparaitre
            );

            //Assert only 3 etudes anterieures have been saved
            assert.strictEqual(result.length, 3);
        });

        it("Fusion études antérieures extOrgId: 15600 same year and month", async function () {
            idPersAGarderEtAnt = [
                {
                    yearFrom: "1999",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1991",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];
            idPersADisparaitreEtAnt = [
                {
                    yearFrom: "1999",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1991",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];

            let dbStub = sinon.stub(db, "getEtudesAnterieures");
            dbStub.onCall(0).resolves(idPersAGarderEtAnt);
            dbStub.onCall(1).resolves(idPersADisparaitreEtAnt);

            let dbStub2 = sinon.stub(db, "saveEtudesAnterieures");
            dbStub2.resolves();

            let dbStub3 = sinon.stub(db, "deleteEtudesAnterieures");
            dbStub3.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            // Act
            const result = await fusion.etudesAnterieures(
                idPersAGarder,
                idPersADisparaitre
            );

            //Assert only 2 etudes anterieures have been saved
            assert.strictEqual(result.length, 2);
        });

        it("Fusion études antérieures same degree same yearFrom", async function () {
            idPersAGarderEtAnt = [
                {
                    yearFrom: "1999",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    degree: "C",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1991",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    degree: "C",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];
            idPersADisparaitreEtAnt = [
                {
                    yearFrom: "1999",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    degree: "C",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1991",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    degree: "C",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];

            let dbStub = sinon.stub(db, "getEtudesAnterieures");
            dbStub.onCall(0).resolves(idPersAGarderEtAnt);
            dbStub.onCall(1).resolves(idPersADisparaitreEtAnt);

            let dbStub2 = sinon.stub(db, "saveEtudesAnterieures");
            dbStub2.resolves();

            let dbStub3 = sinon.stub(db, "deleteEtudesAnterieures");
            dbStub3.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            // Act
            const result = await fusion.etudesAnterieures(
                idPersAGarder,
                idPersADisparaitre
            );

            //Assert only 2 etudes anterieures have been saved
            assert.strictEqual(result.length, 2);
        });

        it("Fusion études antérieures incomplete degree same yearFrom", async function () {
            idPersAGarderEtAnt = [
                {
                    yearFrom: "2020",
                    UMMonthFrom: "09",
                    UMAnnee: "2022",
                    UMMois: "08",
                    degreeStatus: config.constantes.degreeStatusEnCours,
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1991",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    degree: "C",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];
            idPersADisparaitreEtAnt = [
                {
                    yearFrom: "2020",
                    UMMonthFrom: "09",
                    UMAnnee: "2022",
                    UMMois: "08",
                    degreeStatus: config.constantes.degreeStatusEnCours,
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "2000",
                    UMMonthFrom: "09",
                    yearTo: "2002",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    degree: "C",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];

            let dbStub = sinon.stub(db, "getEtudesAnterieures");
            dbStub.onCall(0).resolves(idPersAGarderEtAnt);
            dbStub.onCall(1).resolves(idPersADisparaitreEtAnt);

            let dbStub2 = sinon.stub(db, "saveEtudesAnterieures");
            dbStub2.resolves();

            let dbStub3 = sinon.stub(db, "deleteEtudesAnterieures");
            dbStub3.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            // Act
            const result = await fusion.etudesAnterieures(
                idPersAGarder,
                idPersADisparaitre
            );

            //Assert only 3 etudes anterieures have been saved
            assert.strictEqual(result.length, 3);
        });

        it("Fusion études antérieures  same yearFrom UMMonthFrom yearTo UMMonthTo", async function () {
            idPersAGarderEtAnt = [
                {
                    yearFrom: "1999",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1991",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];
            idPersADisparaitreEtAnt = [
                {
                    yearFrom: "1999",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                },
                {
                    yearFrom: "1991",
                    UMMonthFrom: "09",
                    yearTo: "1980",
                    UMMonthTo: "08",
                    extOrgId: "15600",
                    UMOthrInstFlag: "",
                    UMOthrInstDescr: "",
                    idPersonne: idPersAGarder
                }
            ];

            let dbStub = sinon.stub(db, "getEtudesAnterieures");
            dbStub.onCall(0).resolves(idPersAGarderEtAnt);
            dbStub.onCall(1).resolves(idPersADisparaitreEtAnt);

            let dbStub2 = sinon.stub(db, "saveEtudesAnterieures");
            dbStub2.resolves();

            let dbStub3 = sinon.stub(db, "deleteEtudesAnterieures");
            dbStub3.resolves();

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            // Act
            const result = await fusion.etudesAnterieures(
                idPersAGarder,
                idPersADisparaitre
            );

            //Assert only 2 etudes anterieures have been saved
            assert.strictEqual(result.length, 2);
        });

        it("Error DB getting etudesAnterieures from database", async function () {
            let dbStub2 = sinon.stub(db, "saveEtudesAnterieures");
            dbStub2.resolves();

            let dbStub3 = sinon.stub(db, "deleteEtudesAnterieures");
            dbStub3.resolves();

            sinon
                .stub(db, "getEtudesAnterieures")
                .throws({ code: "ECONRESET", message: "Error ECONRESET" });

            const fusion = proxyquire("../../lib/fusion", {
                db
            });

            const testFunction = async () => {
                // Act
                await fusion.etudesAnterieures(
                    idPersAGarder,
                    idPersADisparaitre
                );
            };

            // Assert
            await assertNode.rejects(testFunction, {
                name: "Error",
                message: "Error fusion_etudesAnterieures - Error ECONRESET"
            });
        });
    });

    describe("Fonction updateDNFromSIS ", function () {
        let matricule;
        let idPers;
        let dn;
        let uuid;
        before(function () {
            matricule = config.test.matriculeExistant;
            idPers = chance.guid({ version: 4 });
            dn = {
                idPersonne: idPers,
                createdAt: "22-05-10 17:58:39",
                updatedAt: "22-07-10 17:58:39"
            };
            uuid = chance.guid();
        });
        it("Mise à jour de données nominatives à partir de données SIS", async function () {
            let dbStub2 = sinon.stub(db, "saveDonneesNominatives");
            dbStub2.resolves();

            let SISStub = sinon.stub(peoplesoft, "getSISData");
            SISStub.resolves(dn);

            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            // Act
            const result = await fusion.updateDNFromSIS(
                idPers,
                matricule,
                uuid
            );

            //Assert
            assert.deepEqual(result, dn);
            db.saveDonneesNominatives.restore();
            peoplesoft.getSISData.restore();
        });

        it("idPersonne vide", async function () {
            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            const testFunction = async () => {
                // Act
                await fusion.updateDNFromSIS(undefined, matricule, uuid);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(error.name, "Error");
                assert.match(
                    error.message,
                    /Error updateDNFromSIS - idPersonne absent/
                );

                return true;
            });
        });
        it("matricule vide", async function () {
            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            const testFunction = async () => {
                // Act
                await fusion.updateDNFromSIS(idPers, undefined, uuid);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(error.name, "Error");
                assert.match(
                    error.message,
                    /Error updateDNFromSIS - matricule absent/
                );

                return true;
            });
        });
        it("uuid vide", async function () {
            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            const testFunction = async () => {
                // Act
                await fusion.updateDNFromSIS(idPers, matricule, undefined);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(error.name, "Error");
                assert.match(
                    error.message,
                    /Error updateDNFromSIS - uuidReq absent/
                );

                return true;
            });
        });
        it("erreur dans l'obtention SIS", async function () {
            let SISStub = sinon.stub(peoplesoft, "getSISData");
            SISStub.throws(new Error("Error getSISData"));
            const fusion = proxyquire("../../lib/fusion", {
                db,
                peoplesoft
            });

            const testFunction = async () => {
                // Act
                await fusion.updateDNFromSIS(idPers, matricule, uuid);
            };

            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(error.name, "Error");
                assert.match(error.message, /Error getSISData/);
                return true;
            });
        });
    });
});
