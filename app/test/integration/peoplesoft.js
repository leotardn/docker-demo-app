"use strict";

const Chance = require("chance");

const { assert } = require("chai");
const assertNode = require("assert");
const config = require("config");
const peoplesoft = require("../../lib/peoplesoft");

const chance = new Chance();

describe("Tests intégration - peoplesoft", function () {
    describe("données nominatives", function () {
        it("Requête réussie à peoplesoft ", async function () {
            let matricule = config.test.matriculeExistant;
            let uuidReq = chance.guid();
            const sISDonneesNominatives = await peoplesoft.getSISData(
                matricule,
                uuidReq,
                config.peoplesoft.donneesNominatives.serviceName
            );
            assert.equal(
                sISDonneesNominatives.emplId,
                matricule,
                "Réponse incorrect de SIS"
            );
        });

        it("Requête à peoplesoft ne réussie pas", async function () {
            let matricule = config.test.matriculeNonExistant;
            let uuidReq = chance.guid();
            const testFunction = async () => {
                // Act
                await peoplesoft.getSISData(
                    matricule,
                    uuidReq,
                    config.peoplesoft.donneesNominatives.serviceName
                );
            };
            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getSISData/);
                assert.match(error.message, /non trouvé/);
                return true;
            });
        });
    });

    describe("études antérieures", function () {
        it("Requête réussie à peoplesoft ", async function () {
            let matricule = config.test.matriculeExistant;
            let uuidReq = chance.guid();
            const sISEtudesAnterieures = await peoplesoft.getSISData(
                matricule,
                uuidReq,
                config.peoplesoft.etudesAnterieures.serviceName
            );
            assert.isAbove(
                sISEtudesAnterieures.length,
                0,
                "Réponse doit contenir au moins une étude antérieure"
            );
        });

        it("Requête à peoplesoft ne réussie pas", async function () {
            let matricule = config.test.matriculeNonExistant;
            let uuidReq = chance.guid();
            const testFunction = async () => {
                // Act
                await peoplesoft.getSISData(
                    matricule,
                    uuidReq,
                    config.peoplesoft.etudesAnterieures.serviceName
                );
            };
            // Assert
            await assertNode.rejects(testFunction, (error) => {
                assert.equal(error.name, "Error");
                assert.match(error.message, /^Error getSISData/);
                assert.match(error.message, /non trouvé/);
                return true;
            });
        });
    });
});
