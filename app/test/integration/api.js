/* eslint-disable no-console */
/* eslint-disable security-node/detect-crlf */
"use strict";

/*
 * Attention: l'ordre d'exécution des tests est important:
 * des données sont créées et ensuite effacées de la base de données.
 * Besoins pour les tests d'intégration:
 *   une connexion avec la base de données
 *   l'api ciam doit être exécutée
 *   l'api admission doit être exécutée (pour la fusion)
 */

const {
    donneesNominativesDev,
    etudesAnterieuresDev,
    fichierProprietes,
    idBdNonExistant
} = require("../profilMockGenerator");

const Chance = require("chance");

const { assert } = require("chai");
const assertNode = require("assert");
const axios = require("axios");
const config = require("config");

const chance = new Chance();

const headers = {
    "Content-Type": "application/json"
};

const testingEnvironment = process.env.npm_config_testenv;
const testServerProfilAPI = config.get(
    `test.servers.${testingEnvironment}.profil`
);
const testServerCIAMAPI = config.get(`test.servers.${testingEnvironment}.ciam`);

console.log(`\nTesting environment: ${testingEnvironment}\n`);

describe("API tests", function () {
    describe(" TESTS CRUD  ", function () {
        let idCIAM;
        before(function () {
            /*
             * Arrange - tous les tests CRUD seront faits avec un nouvel
             * idCIAM
             */
            idCIAM = chance.email();
        });

        describe("Personne TESTS  ", function () {
            describe("GET TESTS", function () {
                it("Get personne - si la personne n'existe pas, elle sera créé", async function () {
                    const endpointURL = `${testServerProfilAPI}/personne?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );

                    assert.exists(
                        response.data.idPersonne,
                        "Response ne contient pas le idPersonne "
                    );

                    assert.exists(
                        response.data.etudesAnterieures,
                        "La réponse ne contient pas  la propriété etudesAnterieures"
                    );

                    assert.exists(
                        response.data.fichiersProprietes,
                        "La réponse ne contient pas  la propriété fichiersProprietes"
                    );

                    assert.notExists(
                        response.data.donneesNominatives,
                        "La réponse ne contient pas  la propriété donneesNominatives"
                    );
                });

                it("Error getting personne without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/personne`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };
                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });
        });

        describe("Données nominatives TESTS", function () {
            describe("TESTS POST", function () {
                it("Saves  donneesnominatives", async function () {
                    const endpointURL = `${testServerProfilAPI}/donneesnominatives/?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: donneesNominativesDev.get("MatFictif")
                    };

                    // Act
                    const response = await axios(axiosConfig);
                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200 response"
                    );
                    assert.exists(
                        response.data.idPersonne,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.notExists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        donneesNominativesDev.get("MatFictif"),
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Error saving new donneesnominatives without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/donneesnominatives`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });

            describe("GET TESTS", function () {
                let today;
                before(async function () {
                    // La date sans l'heure
                    today = new Date().toISOString().split("T")[0];
                });
                it("Gets donneesnominatives with idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/donneesnominatives?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200 response"
                    );
                    assert.exists(
                        response.data.idPersonne,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        donneesNominativesDev.get("MatFictif"),
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Gets admin donneesnominatives mode=inter with matricule", async function () {
                    const { emplId: matricule } = donneesNominativesDev.get(
                        "MatFictif"
                    );
                    const endpointURL = `${testServerProfilAPI}/admin/donneesNominatives?mode=inter&matricule=${matricule}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200 response"
                    );
                    assert.exists(
                        response.data.idPersonne,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        donneesNominativesDev.get("MatFictif"),
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Gets admin donneesnominatives mode=inter with @courriel", async function () {
                    const { emailAddr: courriel } = donneesNominativesDev.get(
                        "MatFictif"
                    );
                    const endpointURL = `${testServerProfilAPI}/admin/donneesNominatives?mode=inter&courriel=${courriel}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    let expecteddonneesNominativesDev = donneesNominativesDev.get(
                        "MatFictif"
                    );
                    expecteddonneesNominativesDev.admApplNbr = [];

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200 response"
                    );
                    assert.containsAllDeepKeys(
                        expecteddonneesNominativesDev,
                        response.data[0],
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Gets admin donneesnominatives mode=inter with @courriel inexistant", async function () {
                    const endpointURL = `${testServerProfilAPI}/admin/donneesNominatives?mode=inter&courriel=klasjd@okasd.com`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        204,
                        "Http response status DOES NOT match the expected 200 response"
                    );
                });

                it("Gets admin donneesnominatives mode=inter with Nom&Prenom", async function () {
                    const { UMLastName: nom } = donneesNominativesDev.get(
                        "MatFictif"
                    );
                    const { UMFirstName: prenom } = donneesNominativesDev.get(
                        "MatFictif"
                    );
                    const endpointURL = `${testServerProfilAPI}/admin/donneesNominatives?mode=inter&nom=${nom}&prenom=${prenom}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    let expecteddonneesNominativesDev = donneesNominativesDev.get(
                        "MatFictif"
                    );
                    expecteddonneesNominativesDev.admApplNbr = [];

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200 response"
                    );
                    assert.containsAllDeepKeys(
                        expecteddonneesNominativesDev,
                        response.data[0],
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Gets admin donneesnominatives mode=inter with dateDebutAnalyse", async function () {
                    const endpointURL = `${testServerProfilAPI}/admin/donneesNominatives?mode=inter&dateDebutAnalyse=${today}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    let expecteddonneesNominativesDev = donneesNominativesDev.get(
                        "MatFictif"
                    );
                    expecteddonneesNominativesDev.idPersonne = "";
                    expecteddonneesNominativesDev.lastUpdatedFieldsDN = [];

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200 response"
                    );
                    assert.exists(
                        response.data[0].lastUpdatedFieldsDN,
                        "Response ne contient pas lastUpdatedFieldsDN"
                    );
                    assert.containsAllDeepKeys(
                        expecteddonneesNominativesDev,
                        response.data[0],
                        "Response  DOES NOT match the provided one"
                    );
                });

                it(`Error get admin donneesnominatives without mode IN ("inter","sis")`, async function () {
                    const endpointURL = `${testServerProfilAPI}/admin/donneesNominatives?mode=notExisting`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 400"
                    });
                });

                it(`Error get admin donneesnominatives with invalid courriel`, async function () {
                    const endpointURL = `${testServerProfilAPI}/admin/donneesNominatives?mode=inter&courriel=notExisting.com`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 400"
                    });
                });

                it("Error get donneesnominatives without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/donneesnominatives/${idBdNonExistant}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });
        });

        describe("Fichiers propriétés  TESTS", function () {
            describe("POST TESTS", function () {
                it("Saves new fichier proprieté", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${fichierProprietes.id}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: fichierProprietes
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        fichierProprietes,
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Save existing fichier proprieté", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${fichierProprietes.id}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: fichierProprietes
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        fichierProprietes,
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Error saving fichier proprieté without id", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichierProprietes`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });

                it("Error saving fichier proprieté without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichierProprietes/${fichierProprietes.id}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });

            describe("GET TESTS", function () {
                it("Gets existing fichierProprietes using its Id", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${fichierProprietes.id}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        fichierProprietes,
                        "Response ID DOES NOT match the provided one"
                    );
                });

                it("Erreur get le fichier de propriété avec un id non exitant  -", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${idBdNonExistant}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 404"
                    });
                });

                it("get all fichiers de propriétés", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                });

                it("ERROR GET fichier proprieté missing idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${fichierProprietes.id}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });
        });

        describe("Études antérieures  TESTS", function () {
            describe("POST TESTS", function () {
                it("Saves new études antérieures", async function () {
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: etudesAnterieuresDev.get("MatFictif")
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        etudesAnterieuresDev.get("MatFictif"),
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Saves new études antérieures avec ACADPlan", async function () {
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures?idCIAM=${idCIAM}`;
                    let ea = Object.assign(
                        {},
                        etudesAnterieuresDev.get("MatFictif")
                    );
                    ea.ACADPlan = "12345";
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: ea
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        ea,
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("No Error saving études antérieures without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieurs`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers
                    };

                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };
                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });

                it("Save existing études antérieures", async function () {
                    // Obtention de l'id d'un étude antérieur ajouté plus tôt dans les tests.
                    const endPointUrlGetPersonne = `${testServerProfilAPI}/personne?idCIAM=${idCIAM}`;
                    const axiosConfigGet = {
                        method: "get",
                        url: endPointUrlGetPersonne,
                        headers: headers
                    };
                    const responseGet = await axios(axiosConfigGet);
                    const idEtudesAnterieures =
                        responseGet.data.etudesAnterieures[0];

                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures/${idEtudesAnterieures}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: etudesAnterieuresDev.get("MatFictif")
                    };
                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        etudesAnterieuresDev.get("MatFictif"),
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("No error post not existing études antérieures", async function () {
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures/${idBdNonExistant}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: etudesAnterieuresDev.get("MatFictif")
                    };

                    // Act
                    const response = await axios(axiosConfig);

                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        etudesAnterieuresDev.get("MatFictif"),
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Save existing études antérieures without id does not throw Error", async function () {
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures/?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: etudesAnterieuresDev.get("MatFictif")
                    };

                    // Act
                    const response = await axios(axiosConfig);
                    // Assert
                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.exists(
                        response.data.createdAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.exists(
                        response.data.updatedAt,
                        "Response ne contient pas la propriété attendue"
                    );
                    assert.deepOwnInclude(
                        response.data,
                        etudesAnterieuresDev.get("MatFictif"),
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Error saving études antérieures without idCIAM", async function () {
                    // Obtention de l'id d'un étude antérieur ajouté plus tôt dans les tests.
                    const endPointUrlGetPersonne = `${testServerProfilAPI}/personne?idCIAM=${idCIAM}`;
                    const axiosConfigGet = {
                        method: "get",
                        url: endPointUrlGetPersonne,
                        headers: headers
                    };
                    const responseGet = await axios(axiosConfigGet);
                    const idEtudesAnterieures =
                        responseGet.data.etudesAnterieures[0];

                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures/${idEtudesAnterieures}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: etudesAnterieuresDev.get("MatFictif")
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });

                it("Error saving existing études antérieures without idCIAM", async function () {
                    // Obtention de l'id d'un étude antérieur ajouté plus tôt dans les tests.
                    const endPointUrlGetPersonne = `${testServerProfilAPI}/personne?idCIAM=${idCIAM}`;
                    const axiosConfigGet = {
                        method: "get",
                        url: endPointUrlGetPersonne,
                        headers: headers
                    };
                    const responseGet = await axios(axiosConfigGet);
                    const idEtudesAnterieures =
                        responseGet.data.etudesAnterieures[0];

                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures/${idEtudesAnterieures}`;
                    const axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: etudesAnterieuresDev.get("MatFictif")
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });

            describe("GET TESTS", function () {
                it("Gets all études antérieures using idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    // Act
                    const response = await axios(axiosConfig);

                    // Assert

                    assert.strictEqual(
                        parseInt(response.status),
                        200,
                        "Http response status DOES NOT match the expected 200"
                    );
                    assert.isAbove(
                        response.data.length,
                        0,
                        "Response does not contain etudesAnterieures"
                    );
                });

                it("Error getting études antérieures without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures`;
                    const axiosConfig = {
                        method: "get",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });
        });

        describe("Delete TESTS", function () {
            describe("Études antérieures", function () {
                it("Delete existing etudeAnterieure", async function () {
                    // Obtention de l'id d'un étude antérieur ajouté plus tôt dans les tests.
                    const endPointUrlGetPersonne = `${testServerProfilAPI}/personne?idCIAM=${idCIAM}`;
                    const axiosConfigGet = {
                        method: "get",
                        url: endPointUrlGetPersonne,
                        headers: headers
                    };
                    let responseGet = await axios(axiosConfigGet);
                    const idEtudesAnterieures =
                        responseGet.data.etudesAnterieures[0];
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures/${idEtudesAnterieures}?idCIAM=${idCIAM}`;
                    const axiosConfigDelete = {
                        method: "delete",
                        url: endpointURL,
                        headers: headers
                    };

                    // Act
                    const responseDelete = await axios(axiosConfigDelete);
                    // Assert
                    assert.strictEqual(
                        parseInt(responseDelete.status),
                        204,
                        "Http response status DOES NOT match the expected 204"
                    );

                    // Personne ne contient plus le etude antérieure supprimé
                    responseGet = await axios(axiosConfigGet);
                    assert.notDeepNestedInclude(
                        responseGet.data,
                        { id: idEtudesAnterieures },
                        "Response  DOES NOT match the provided one"
                    );
                });

                it("Error deleting etudesAnterieures with not existing idEtudesAnterieures", async function () {
                    let idNonExistant = chance.natural();
                    const endpointURL = `${testServerProfilAPI}/etudesAnterieures/${idNonExistant}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "delete",
                        url: endpointURL,
                        headers: headers
                    };

                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 404"
                    });
                });

                it("Error delete etudesAnterieures without idCIAM", async function () {
                    // Obtention de l'id d'un étude antérieure ajouté plus tôt dans les tests.
                    const endPointUrlGetPersonne = `${testServerProfilAPI}/personne?idCIAM=${idCIAM}`;
                    const axiosConfigGet = {
                        method: "get",
                        url: endPointUrlGetPersonne,
                        headers: headers
                    };
                    const responseGet = await axios(axiosConfigGet);
                    const idEtudesAnterieures =
                        responseGet.data.etudesAnterieures[0];
                    const endpointURLDelete = `${testServerProfilAPI}/etudesAnterieures/${idEtudesAnterieures}}`;
                    const axiosConfig = {
                        method: "delete",
                        url: endpointURLDelete,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });

            describe("Fichiers de propriétes", function () {
                it("Delete existing fichierProprietes", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${fichierProprietes.id}?idCIAM=${idCIAM}`;
                    const axiosConfigDelete = {
                        method: "delete",
                        url: endpointURL,
                        headers: headers
                    };
                    const axiosConfigGet = {
                        method: "get",
                        url: endpointURL,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    };

                    // Act
                    const responseDelete = await axios(axiosConfigDelete);

                    // Assert
                    assert.strictEqual(
                        parseInt(responseDelete.status),
                        204,
                        "Http response status DOES NOT match the expected 204"
                    );
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfigGet);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 404"
                    });
                });

                it("Error - DELETE id does not exist", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${idBdNonExistant}?idCIAM=${idCIAM}`;
                    const axiosConfig = {
                        method: "delete",
                        url: endpointURL,
                        headers: headers
                    };

                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 404"
                    });
                });

                it("Error delete  without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/fichiersProprietes/${fichierProprietes.propFile}`;
                    const axiosConfig = {
                        method: "delete",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });

            describe("Personne", function () {
                it("Delete existing personne", async function () {
                    const endpointURL = `${testServerProfilAPI}/personne?idCIAM=${idCIAM}`;
                    const axiosConfigDelete = {
                        method: "delete",
                        url: endpointURL,
                        headers: headers
                    };
                    const axiosConfigGet = {
                        method: "get",
                        url: endpointURL,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    };

                    // Act
                    const responseDelete = await axios(axiosConfigDelete);

                    // Assert
                    assert.strictEqual(
                        parseInt(responseDelete.status),
                        204,
                        "Http response status DOES NOT match the expected 204"
                    );

                    // Act
                    const responseGet = await axios(axiosConfigGet);

                    // Assert
                    assert.strictEqual(
                        parseInt(responseGet.status),
                        200,
                        "Http response status DOES NOT match the expected 204"
                    );
                });

                it("Error delete profile without idCIAM", async function () {
                    const endpointURL = `${testServerProfilAPI}/personne`;
                    const axiosConfig = {
                        method: "delete",
                        url: endpointURL,
                        headers: headers
                    };
                    const testFunction = async () => {
                        // Act
                        await axios(axiosConfig);
                    };

                    // Assert
                    await assertNode.rejects(testFunction, {
                        name: "Error",
                        message: "Request failed with status code 401"
                    });
                });
            });
        });
    });

    describe("SIS", function () {
        let idCIAM;

        before(async function () {
            idCIAM = config.test.courrielUsagerDansOkta;
        });

        describe("ERROR GET TESTS Gets NOT EXISTING matricule", function () {
            before(async function () {
                try {
                    let endpointURL = `${testServerCIAMAPI}/compte/${idCIAM}`;
                    let axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: {
                            udem_matricule: config.test.matriculeNonExistant
                        }
                    };
                    await axios(axiosConfig);
                } catch (error) {
                    throw Error(
                        `Erreur dans before SIS: Verifier que le compte CIAM existe ${error}`
                    );
                }
            });
            it("Error getting donneesNominatives not existing matricule", async function () {
                const endpointURL = `${testServerProfilAPI}/SIS/donneesNominatives?idCIAM=${idCIAM}`;
                const axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };

                const testFunction = async () => {
                    // Act
                    await axios(axiosConfig);
                };

                // Assert
                await assertNode.rejects(testFunction, {
                    name: "Error",
                    message: "Request failed with status code 404"
                });
            });

            it("Error getting etudesAnterieures not existing matricule", async function () {
                const endpointURL = `${testServerProfilAPI}/SIS/etudesAnterieures?idCIAM=${idCIAM}`;
                const axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };

                const testFunction = async () => {
                    // Act
                    await axios(axiosConfig);
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(
                        error.message,
                        /^Request failed with status code 404/
                    );
                    return true;
                });
            });
        });

        describe("ERROR GET TESTS Gets NULL matricule", function () {
            before(async function () {
                try {
                    let endpointURL = `${testServerCIAMAPI}/compte/${idCIAM}`;
                    let axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: {
                            udem_matricule: " "
                        }
                    };
                    await axios(axiosConfig);
                } catch (error) {
                    throw Error(
                        `Erreur dans before SIS: Verifier que le compte CIAM existe ${error}`
                    );
                }
            });
            it("Error getting donneesNominatives NULL matricule", async function () {
                const endpointURL = `${testServerProfilAPI}/SIS/donneesNominatives?idCIAM=${idCIAM}`;
                const axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };

                const testFunction = async () => {
                    // Act
                    await axios(axiosConfig);
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(
                        error.message,
                        /^Request failed with status code 403/
                    );
                    return true;
                });
            });

            it("Error getting etudesAnterieures NULL matricule", async function () {
                const endpointURL = `${testServerProfilAPI}/SIS/etudesAnterieures?idCIAM=${idCIAM}`;
                const axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };

                const testFunction = async () => {
                    // Act
                    await axios(axiosConfig);
                };

                // Assert
                await assertNode.rejects(testFunction, (error) => {
                    assert.equal(error.name, "Error");
                    assert.match(
                        error.message,
                        /^Request failed with status code 403/
                    );
                    return true;
                });
            });
        });

        describe("GET TESTS Gets EXISTING matricule", function () {
            before(async function () {
                try {
                    let endpointURL = `${testServerCIAMAPI}/compte/${idCIAM}`;
                    let axiosConfig = {
                        method: "post",
                        url: endpointURL,
                        headers: headers,
                        data: { udem_matricule: config.test.matriculeExistant }
                    };
                    await axios(axiosConfig);
                } catch (error) {
                    throw Error(
                        `Erreur dans before SIS: Verifier que le compte CIAM existe ${error}`
                    );
                }
            });
            it("Gets donneesNominatives existing matricule", async function () {
                const endpointURL = `${testServerProfilAPI}/SIS/donneesNominatives?idCIAM=${idCIAM}`;
                const axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };

                // Act
                const response = await axios(axiosConfig);

                // Assert
                assert.strictEqual(
                    parseInt(response.status),
                    200,
                    "Http response status DOES NOT match the expected 200"
                );

                assert.strictEqual(
                    response.data.emplId,
                    config.test.matriculeExistant,
                    "Response ID DOES NOTT match the provided one"
                );

                assert.exists(
                    response.data.emplId,
                    "La réponse ne contient pas la propriété emplId"
                );
            });

            it("Gets etudesAnterieures existing matricule", async function () {
                const endpointURL = `${testServerProfilAPI}/SIS/etudesAnterieures?idCIAM=${idCIAM}`;
                const axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };

                // Act
                const response = await axios(axiosConfig);

                // Assert
                assert.strictEqual(
                    parseInt(response.status),
                    200,
                    "Http response status DOES NOT match the expected 200"
                );

                assert.isArray(response.data, "Response doit être tableau");
                assert.isAbove(
                    response.data.length,
                    1,
                    "La réponse doit être un tableau de taille supérieur ou égale à 1"
                );
            });
        });
    });

    describe("Fusion personnes  TESTS - ce test doit etre executé après tous les autres, parce qu'il efface tous les données précédentes  ", function () {
        let idPersonneAGarder;
        let idPersonneADisparaitre;
        let idCIAMAGarder;
        let idCIAMADisparaitre;
        let response;
        before(async function () {
            try {
                // Arrange

                /*
                 * Création de la personne à garder dans la bd via son courriel
                 * Le idOkta existe déja pour ce courriel.
                 */
                idCIAMAGarder = config.test.courrielUsagerDansOkta;
                let endpointURL = `${testServerProfilAPI}/personne?idCIAM=${idCIAMAGarder}`;
                let axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };
                response = await axios(axiosConfig);

                //Création de la personne à disparaître
                idPersonneAGarder = response.data.idPersonne;
                if (!idPersonneAGarder) {
                    throw new Error(
                        `idPersonne avec idCIAM à garder ${idCIAMAGarder} n'a pas été trouvé. Verifier que la personne existe dans la bd`
                    );
                }
                //Deuxième personne pour la fusion
                idCIAMADisparaitre = config.get(
                    "test.courrielUsagerDansOkta2Fusion"
                );
                endpointURL = `${testServerProfilAPI}/personne?idCIAM=${idCIAMADisparaitre}`;
                axiosConfig = {
                    method: "get",
                    url: endpointURL,
                    headers: headers
                };
                response = await axios(axiosConfig);
                idPersonneADisparaitre = response.data.idPersonne;
                if (!idPersonneADisparaitre) {
                    throw new Error(
                        `idPersonne avec idCIAM à disparaitre ${idCIAMADisparaitre} n'a pas été trouvé. Verifier que la personne existe dans la bd`
                    );
                }
                // Les données nominatives utilisées seront celles obtenues de SIS

                // On ajoute des études antérieures pour les deux personnes
                endpointURL = `${testServerProfilAPI}/etudesAnterieures?idCIAM=${idCIAMAGarder}`;
                axiosConfig = {
                    method: "post",
                    url: endpointURL,
                    headers: headers,
                    data: etudesAnterieuresDev.get("eaSynchroSansErreur")
                };
                await axios(axiosConfig);

                endpointURL = `${testServerProfilAPI}/etudesAnterieures?idCIAM=${idCIAMADisparaitre}`;
                axiosConfig = {
                    method: "post",
                    url: endpointURL,
                    headers: headers,
                    data: etudesAnterieuresDev.get("eAMatFictif")
                };
                await axios(axiosConfig);
            } catch (error) {
                console.log("Error before fusion", error);
            }
        });
        after(async function () {
            //Clean up database
            let endpointURL = `${testServerProfilAPI}/personne?idCIAM=${idCIAMADisparaitre}`;
            let axiosConfig = {
                method: "delete",
                url: endpointURL,
                headers: headers
            };
            try {
                await axios(axiosConfig);
            } catch (err) {
                console.log("Fusion After Err", err);
            }
        });
        it("Fusion deux personne dans la bd intermediaire - des fois ça me marche pas (essayer de vider la table personne)", async function () {
            let endpointURL = `${testServerProfilAPI}/fusion?idCIAMAGarder=${idCIAMAGarder}&idCIAMADisparaitre=${idCIAMADisparaitre}&matriculeAGarder=${config.test.matriculeExistant}`;
            let axiosConfig = {
                method: "post",
                url: endpointURL,
                headers: headers
            };

            // Act
            await axios(axiosConfig);

            /*
             *  Assert
             * Verifie que la personne à  garder est encore dans la bd avec le même idPersonne
             */
            endpointURL = `${testServerProfilAPI}/personne?idCIAM=${idCIAMAGarder}`;
            axiosConfig = {
                method: "get",
                url: endpointURL,
                headers: headers
            };
            response = await axios(axiosConfig);
            assert.equal(
                response.data.idPersonne,
                idPersonneAGarder,
                "idPersonne non retrouvé, Personne à garder n'est pas présente dans la bd."
            );

            /*
             *  Assert
             * Verifie que la personne à disparaître a disparue dans la bd
             */
            endpointURL = `${testServerProfilAPI}/personne?idCIAM=${idCIAMADisparaitre}`;
            axiosConfig = {
                method: "get",
                url: endpointURL,
                headers: headers
            };
            response = await axios(axiosConfig);
            assert.notEqual(
                response.data.idPersonne,
                idPersonneADisparaitre,
                "idPersonne retrouvé, Personne à disparaitre est toujours presente dans la bd."
            );
        });

        it("Error fusion comptes avec un idPersonne à disparaitre inexistant", async function () {
            const idCIAMSansPersonne = chance.email();
            const endpointURL = `${testServerProfilAPI}/fusion?idCIAMAGarder=${idCIAMAGarder}&idCIAMADisparaitre=${idCIAMSansPersonne}&matriculeAGarder=${config.test.matriculeExistant}`;
            const axiosConfig = {
                method: "post",
                url: endpointURL,
                headers: headers
            };
            const testFunction = async () => {
                // Act
                await axios(axiosConfig);
            };

            // Assert
            await assertNode.rejects(testFunction, {
                name: "Error",
                message: "Request failed with status code 404"
            });
        });
    });
});
